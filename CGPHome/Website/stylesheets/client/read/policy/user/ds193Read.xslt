<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas" xmlns:ds="http://tempuri.org/dsCustomer.xsd">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmPHDETAIL.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmSPECCONT.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmSPECPE.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPARKDETA.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmCLAIMSUM.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmXSANDNCD.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmProposer.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmGeneral.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmPHDETAIL.tst" match="ds:USER_CGPHOME_PHDETAIL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_PHDETAIL[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPHDETAIL.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_PHDETAIL_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboWinLocks</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:WINLOCKS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:WINLOCKS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboAlarmType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ALARMTYPE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:ALARMTYPE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboDoorLock</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DOORLOCK_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:DOORLOCK_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtPESpecCount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:PESPECCOUNT" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtContSpecCount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CONTSPECCOUNT" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdTotalSI</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:TOTALSI)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:TOTALSI" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboCycleCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CYCLECOVER_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CYCLECOVER_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboPECover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:PECOVER_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:PECOVER_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:ADCOVER='1' or ds:ADCOVER='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdContentSI</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:CONTENTSI)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:CONTENTSI" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdPHSI</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:PHSI)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:PHSI" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkGSORepair</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:GSOREPAIR='1' or ds:GSOREPAIR='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkSmoke</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:SMOKE='1' or ds:SMOKE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkFinanced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:FINANCED='1' or ds:FINANCED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboFlatorFelt</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:FLATORFELT_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:FLATORFELT_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboYOManufacture</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:YOMANUFACTURE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:YOMANUFACTURE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TYPE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboModel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:MODEL_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:MODEL_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMake</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:MAKE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:MAKE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtIntParty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:INTPARTY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECCONT.tst" match="ds:USER_CGPHOME_SPECCONT">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_SPECCONT[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECCONT.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_SPECCONT_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtdummy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DUMMY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmITEMSPEC.tst">
             <xsl:with-param name="pParentID" select="ds:CGPHOME_SPECCONT_ID" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECPE.tst" match="ds:USER_CGPHOME_SPECPE">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_SPECPE[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECPE.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_SPECPE_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDummy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DUMMY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmSPECITEM.tst">
             <xsl:with-param name="pParentID" select="ds:CGPHOME_SPECPE_ID" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPARKDETA.tst" match="ds:USER_CGPHOME_PARKDETA">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_PARKDETA[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPARKDETA.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_PARKDETA_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboUnoccupied</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:UNOCCUPIED_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:UNOCCUPIED_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRegSite</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REGSITE='1' or ds:REGSITE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboBusinessUse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:BUSINESSUSE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:BUSINESSUSE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNumOnSite</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:NUMONSITE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmCLAIMSUM.tst" match="ds:USER_CGPHOME_CLAIMSUM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_CLAIMSUM[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmCLAIMSUM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_CLAIMSUM_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
         </controls>

         <xsl:call-template name="frmCLAIMDET.tst">
             <xsl:with-param name="pParentID" select="ds:CGPHOME_CLAIMSUM_ID" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmCLAIMDET.tst" match="ds:USER_CGPHOME_CLAIMDET">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_CLAIMDET[ds:CGPHOME_CLAIMSUM_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmCLAIMDET.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_CLAIMDET_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:CGPHOME_CLAIMSUM_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkAtFault</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:ATFAULT='1' or ds:ATFAULT='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkIncOnThisPolicy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:INCONTHISPOLICY='1' or ds:INCONTHISPOLICY='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkNCDPredjudiced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:NCDPREDJUDICED='1' or ds:NCDPREDJUDICED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLegalRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:LEGALREF" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdSettledAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:SETTLEDAMOUNT)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:SETTLEDAMOUNT" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskSettledDate</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:SETTLEDDATE)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:SETTLEDDATE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdAmountClaimed</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:AMOUNTCLAIMED)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:AMOUNTCLAIMED" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtDescription</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DESCRIPTION" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMSTATUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMSTATUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMDETAILS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMDETAILS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMTYPE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMTYPE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidTime</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:INCIDTIME)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:INCIDTIME" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidDate</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:INCIDDATE)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:INCIDDATE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMREF" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNotesView</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:NOTESVIEW" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNewNotes</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:NEWNOTES" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmITEMSPEC.tst" match="ds:USER_CGPHOME_ITEMSPEC">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_ITEMSPEC[ds:CGPHOME_SPECCONT_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmITEMSPEC.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_ITEMSPEC_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:CGPHOME_SPECCONT_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtItemSerial</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMSERIAL" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemModel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMMODEL" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemMake</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMMAKE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdItemValue</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:ITEMVALUE)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:ITEMVALUE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemDesc</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMDESC" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECITEM.tst" match="ds:USER_CGPHOME_SPECITEM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_SPECITEM[ds:CGPHOME_SPECPE_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECITEM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_SPECITEM_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:CGPHOME_SPECPE_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtITEMSERIAL</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMSERIAL" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMMODEL</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMMODEL" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMMAKE</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMMAKE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdITEMVALUE</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:ITEMVALUE)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:ITEMVALUE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMDESC</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ITEMDESC" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmXSANDNCD.tst" match="ds:USER_CGPHOME_XSANDNCD">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_XSANDNCD[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmXSANDNCD.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_XSANDNCD_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboNCD</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:NCD_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:NCD_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboExcess</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EXCESS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EXCESS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmProposer.tst" match="ds:USER_CGPHOME_PROPOSER">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_PROPOSER[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmProposer.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_PROPOSER_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboRelationship</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:RELATIONSHIP_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:RELATIONSHIP_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMaritalStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:MARITALSTATUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:MARITALSTATUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtAge</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:AGE)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:AGE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:DOB)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:DOB" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:SURNAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitials</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:INITIALS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:FORENAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optCompany</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:COMPANY='1' or ds:COMPANY='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optNA</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:NA='1' or ds:NA='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optFemale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:FEMALE='1' or ds:FEMALE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optMale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:MALE='1' or ds:MALE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TITLE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:TITLE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmOccupat.tst">
             <xsl:with-param name="pParentID" select="ds:CGPHOME_PROPOSER_ID" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmOccupat.tst" match="ds:USER_CGPHOME_OCCUPAT">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_OCCUPAT[ds:CGPHOME_PROPOSER_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmOccupat.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_OCCUPAT_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:CGPHOME_PROPOSER_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkPartTime</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:PARTTIME='1' or ds:PARTTIME='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TYPE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:TYPE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpsBusiness</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EMPSBUSINESS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EMPSBUSINESS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboDescription</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DESCRIPTION_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:DESCRIPTION_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmGeneral.tst" match="ds:USER_CGPHOME_GENERAL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_CGPHOME_GENERAL[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGeneral.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:CGPHOME_GENERAL_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DETAILS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRequested</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REQUESTED='1' or ds:REQUESTED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkConvicted</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:CONVICTED='1' or ds:CONVICTED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefused</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REFUSED='1' or ds:REFUSED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
