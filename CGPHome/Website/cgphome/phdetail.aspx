<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Park Home Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Park Home Details</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_PHDETAIL__MAKE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__MAKE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__MAKE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__MAKE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__MAKE_ID__0">Make</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__MAKE_ID__0" runat="server" ListViewName="parkhome_make_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__MAKE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__MAKE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Make" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__MODEL_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__MODEL_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__MODEL_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__MODEL_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__MODEL_ID__0">Model</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__MODEL_ID__0" runat="server" ListViewName="parkhome_model_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__MODEL_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__MODEL_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Model" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__TYPE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__TYPE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__TYPE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__TYPE__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__TYPE__0">Type</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__TYPE__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__TYPE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__TYPE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Type" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0">Year Of Manufacture</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" runat="server" ListViewName="year_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__YOMANUFACTURE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Year Of Manufacture" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__FLATORFELT_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__FLATORFELT_ID__0">Flat or Felt Roof</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" runat="server" ListViewName="parkhome_rooftype_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__FLATORFELT_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Flat or Felt Roof" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__FINANCED__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__FINANCED__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__FINANCED__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__FINANCED__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__FINANCED__0">Financed</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_PHDETAIL__FINANCED__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__SMOKE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__SMOKE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__SMOKE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__SMOKE__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__SMOKE__0">Smoke Detectors?</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_PHDETAIL__SMOKE__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__GSOREPAIR__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__GSOREPAIR__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__GSOREPAIR__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__GSOREPAIR__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__GSOREPAIR__0">Good State of Repair</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_PHDETAIL__GSOREPAIR__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__PHSI__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__PHSI__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__PHSI__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__PHSI__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__PHSI__0">Park Home SI</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__PHSI__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__PHSI__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__PHSI__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Park Home SI" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__CONTENTSI__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__CONTENTSI__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__CONTENTSI__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__CONTENTSI__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__CONTENTSI__0">Contents SI</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__CONTENTSI__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__CONTENTSI__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__CONTENTSI__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Contents SI" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0">Count Of Contents Specified Items</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" runat="server" MaxLength="140" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__CONTSPECCOUNT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Count Of Contents Specified Items" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__ADCOVER__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__ADCOVER__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__ADCOVER__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__ADCOVER__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__ADCOVER__0">Accidental Damage Cover</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_PHDETAIL__ADCOVER__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__PECOVER_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__PECOVER_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__PECOVER_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__PECOVER_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__PECOVER_ID__0">Personal Effects and Money</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__PECOVER_ID__0" runat="server" ListViewName="parkhome_pecover_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__PECOVER_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__PECOVER_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Personal Effects and Money" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__PESPECCOUNT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__PESPECCOUNT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__PESPECCOUNT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__PESPECCOUNT__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__PESPECCOUNT__0">Count of Personal Effects Specified Items</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__PESPECCOUNT__0" runat="server" MaxLength="100" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__PESPECCOUNT__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__PESPECCOUNT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Count of Personal Effects Specified Items" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0">Pedal Cycle Cover</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" runat="server" ListViewName="parkhome_pedalcover_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__CYCLECOVER_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Pedal Cycle Cover" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__TOTALSI__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__TOTALSI__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__TOTALSI__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__TOTALSI__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__TOTALSI__0">TotalSI</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__TOTALSI__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__TOTALSI__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__TOTALSI__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="TotalSI" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__DOORLOCK_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__DOORLOCK_ID__0">Door Locks</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" runat="server" ListViewName="parkhome_door_locks_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__DOORLOCK_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Door Locks" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0">Alarm Type</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" runat="server" ListViewName="parkhome_alarm_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__ALARMTYPE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Alarm Type" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__WINLOCKS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__WINLOCKS_ID__0">Window Locks</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" runat="server" ListViewName="parkhome_window_locks_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__WINLOCKS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Window Locks" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PHDETAIL__INTPARTY__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PHDETAIL__INTPARTY__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PHDETAIL__INTPARTY__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PHDETAIL__INTPARTY__0" runat="server" AssociatedControlID="USER_CGPHOME_PHDETAIL__INTPARTY__0">Interested Party Details</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PHDETAIL__INTPARTY__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="8000" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PHDETAIL__INTPARTY__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PHDETAIL__INTPARTY__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Interested Party Details" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
