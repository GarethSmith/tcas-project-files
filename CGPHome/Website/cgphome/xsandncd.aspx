<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Excess And NCD</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Excess And NCD</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_XSANDNCD__EXCESS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_XSANDNCD__EXCESS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_XSANDNCD__EXCESS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_XSANDNCD__EXCESS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_XSANDNCD__EXCESS_ID__0">Excess</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_XSANDNCD__EXCESS_ID__0" runat="server" ListViewName="parkhome_excess_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_XSANDNCD__EXCESS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_XSANDNCD__EXCESS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Excess" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_XSANDNCD__NCD_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_XSANDNCD__NCD_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_XSANDNCD__NCD_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_XSANDNCD__NCD_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_XSANDNCD__NCD_ID__0">No Claims Discount</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_XSANDNCD__NCD_ID__0" runat="server" ListViewName="parkhome_ncd_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_XSANDNCD__NCD_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_XSANDNCD__NCD_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="No Claims Discount" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
