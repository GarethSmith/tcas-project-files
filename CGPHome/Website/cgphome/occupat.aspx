<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Occupation</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LOBParent" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend></legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" runat="server" style="display:none;"></li>
             <li>
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0"></tes_webcontrols:TGSLLabel>
                 <span class="control"><asp:DropDownList id="USER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" runat="server"></asp:DropDownList>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_OCCUPAT__CGPHOME_PROPOSER_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Occupation</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0">Description</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" runat="server" ListViewName="occupation_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_OCCUPAT__DESCRIPTION_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Description" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0">Employers Business</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" runat="server" ListViewName="employers_bus_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_OCCUPAT__EMPSBUSINESS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Employers Business" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_OCCUPAT__TYPE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_OCCUPAT__TYPE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_OCCUPAT__TYPE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_OCCUPAT__TYPE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_OCCUPAT__TYPE_ID__0">Type</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_OCCUPAT__TYPE_ID__0" runat="server" ListViewName="employment_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_OCCUPAT__TYPE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_OCCUPAT__TYPE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Type" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_OCCUPAT__PARTTIME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_OCCUPAT__PARTTIME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_OCCUPAT__PARTTIME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_OCCUPAT__PARTTIME__0" runat="server" AssociatedControlID="USER_CGPHOME_OCCUPAT__PARTTIME__0">Part Time</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_OCCUPAT__PARTTIME__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
