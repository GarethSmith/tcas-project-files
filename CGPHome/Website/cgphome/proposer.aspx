<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server"></h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend></legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_PROPOSER__TITLE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__TITLE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__TITLE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__TITLE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__TITLE_ID__0">Title</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PROPOSER__TITLE_ID__0" runat="server" ListViewName="title_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__TITLE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__TITLE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Title" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__SEX__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__SEX__0"">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__SEX__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__SEX__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__SEX__0">Male</tes_webcontrols:TGSLLabel>
                 <span class="control"><span class="radiocontrol" ID="USER_CGPHOME_PROPOSER__SEX__0" runat="server"><span><asp:RadioButton ID="USER_CGPHOME_PROPOSER__NA__0" runat="server" Text="N/A" GroupName="grpSex" /></span><span><asp:RadioButton ID="USER_CGPHOME_PROPOSER__MALE__0" runat="server" Text="Male" GroupName="grpSex" /></span><span><asp:RadioButton ID="USER_CGPHOME_PROPOSER__FEMALE__0" runat="server" Text="Female" GroupName="grpSex" /></span><span><asp:RadioButton ID="USER_CGPHOME_PROPOSER__COMPANY__0" runat="server" Text="Company" GroupName="grpSex" /></span></span></span></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__FORENAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__FORENAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__FORENAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__FORENAME__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__FORENAME__0">Forename</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PROPOSER__FORENAME__0" runat="server" MaxLength="100" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__FORENAME__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__FORENAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Forename" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__INITIALS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__INITIALS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__INITIALS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__INITIALS__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__INITIALS__0">Other Initials</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PROPOSER__INITIALS__0" runat="server" MaxLength="100" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__INITIALS__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__INITIALS__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Other Initials" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__SURNAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__SURNAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__SURNAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__SURNAME__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__SURNAME__0">Surname</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PROPOSER__SURNAME__0" runat="server" MaxLength="100" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__SURNAME__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__SURNAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Surname" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__DOB__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__DOB__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__DOB__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__DOB__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__DOB__0">DOB</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_CGPHOME_PROPOSER__DOB__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__DOB__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__DOB__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="DOB" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__AGE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__AGE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__AGE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__AGE__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__AGE__0">Age</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PROPOSER__AGE__0" runat="server" MaxLength="10"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__AGE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__AGE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Age" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0">Marital Status</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" runat="server" ListViewName="marital_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__MARITALSTATUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Marital Status" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0">Relationship</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" runat="server" ListViewName="relationship_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PROPOSER__RELATIONSHIP_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Relationship" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
