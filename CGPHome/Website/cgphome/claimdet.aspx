<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Claim Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Claim Details</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_CLAIMDET__CLAIMREF__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__CLAIMREF__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__CLAIMREF__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__CLAIMREF__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__CLAIMREF__0">Claim Reference</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__CLAIMREF__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__CLAIMREF__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__CLAIMREF__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Claim Reference" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__INCIDDATE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__INCIDDATE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__INCIDDATE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__INCIDDATE__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__INCIDDATE__0">Incident Date</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_CGPHOME_CLAIMDET__INCIDDATE__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__INCIDDATE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__INCIDDATE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Incident Date" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__INCIDTIME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__INCIDTIME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__INCIDTIME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__INCIDTIME__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__INCIDTIME__0">Incident Time</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTimeSelect id="USER_CGPHOME_CLAIMDET__INCIDTIME__0" runat="server"></tes_webcontrols:TGSLTimeSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__INCIDTIME__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__INCIDTIME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Incident Time" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0">Claim Type</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" runat="server" ListViewName="parkhome_claim_type_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__CLAIMTYPE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Claim Type" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0">Claim Details</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" runat="server" ListViewName="parkhome_claim_details_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__CLAIMDETAILS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Claim Details" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0">Claim Status</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" runat="server" ListViewName="parkhome_claim_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__CLAIMSTATUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Claim Status" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__DESCRIPTION__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__DESCRIPTION__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__DESCRIPTION__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__DESCRIPTION__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__DESCRIPTION__0">Claim Description</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__DESCRIPTION__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="8000" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__DESCRIPTION__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__DESCRIPTION__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Claim Description" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0">Amount Claimed</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__AMOUNTCLAIMED__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Amount Claimed" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__SETTLEDDATE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__SETTLEDDATE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__SETTLEDDATE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__SETTLEDDATE__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__SETTLEDDATE__0">Settlement Date</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_CGPHOME_CLAIMDET__SETTLEDDATE__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__SETTLEDDATE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__SETTLEDDATE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Settlement Date" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0">Settled Amount</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__SETTLEDAMOUNT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Settled Amount" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__LEGALREF__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__LEGALREF__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__LEGALREF__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__LEGALREF__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__LEGALREF__0">Legal Reference Number</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__LEGALREF__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__LEGALREF__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__LEGALREF__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Legal Reference Number" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0">NCD Predjudiced</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_CLAIMDET__NCDPREDJUDICED__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0">Incurred On This Policy</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_CLAIMDET__INCONTHISPOLICY__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__ATFAULT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__ATFAULT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__ATFAULT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__ATFAULT__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__ATFAULT__0">At Fault</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_CLAIMDET__ATFAULT__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__NOTESVIEW__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__NOTESVIEW__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__NOTESVIEW__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__NOTESVIEW__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__NOTESVIEW__0">Notes View</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__NOTESVIEW__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="8000" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__NOTESVIEW__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__NOTESVIEW__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Notes View" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_CLAIMDET__NEWNOTES__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_CLAIMDET__NEWNOTES__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_CLAIMDET__NEWNOTES__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_CLAIMDET__NEWNOTES__0" runat="server" AssociatedControlID="USER_CGPHOME_CLAIMDET__NEWNOTES__0">New Notes</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_CLAIMDET__NEWNOTES__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="8000" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_CLAIMDET__NEWNOTES__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_CLAIMDET__NEWNOTES__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="New Notes" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
