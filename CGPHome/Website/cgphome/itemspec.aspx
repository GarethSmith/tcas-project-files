<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Specified Items</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Specified Items</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_ITEMSPEC__ITEMDESC__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_ITEMSPEC__ITEMDESC__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_ITEMSPEC__ITEMDESC__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_ITEMSPEC__ITEMDESC__0" runat="server" AssociatedControlID="USER_CGPHOME_ITEMSPEC__ITEMDESC__0">Item Description</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_ITEMSPEC__ITEMDESC__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_ITEMSPEC__ITEMDESC__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_ITEMSPEC__ITEMDESC__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Item Description" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_ITEMSPEC__ITEMVALUE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_ITEMSPEC__ITEMVALUE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_ITEMSPEC__ITEMVALUE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_ITEMSPEC__ITEMVALUE__0" runat="server" AssociatedControlID="USER_CGPHOME_ITEMSPEC__ITEMVALUE__0">Item Value</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_ITEMSPEC__ITEMVALUE__0" runat="server" isDecimalValue="True" Numeric="True" Currency="True" decimalPlaces="2"></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_ITEMSPEC__ITEMVALUE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_ITEMSPEC__ITEMVALUE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Item Value" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_ITEMSPEC__ITEMMAKE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_ITEMSPEC__ITEMMAKE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_ITEMSPEC__ITEMMAKE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_ITEMSPEC__ITEMMAKE__0" runat="server" AssociatedControlID="USER_CGPHOME_ITEMSPEC__ITEMMAKE__0">Item Make</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_ITEMSPEC__ITEMMAKE__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_ITEMSPEC__ITEMMAKE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_ITEMSPEC__ITEMMAKE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Item Make" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_ITEMSPEC__ITEMMODEL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_ITEMSPEC__ITEMMODEL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_ITEMSPEC__ITEMMODEL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_ITEMSPEC__ITEMMODEL__0" runat="server" AssociatedControlID="USER_CGPHOME_ITEMSPEC__ITEMMODEL__0">Item Model</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_ITEMSPEC__ITEMMODEL__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_ITEMSPEC__ITEMMODEL__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_ITEMSPEC__ITEMMODEL__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Item Model" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_ITEMSPEC__ITEMSERIAL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" runat="server" AssociatedControlID="USER_CGPHOME_ITEMSPEC__ITEMSERIAL__0">Serial Number</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_ITEMSPEC__ITEMSERIAL__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Serial Number" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
