<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Park Home Estate Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Park Home Estate Details</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_PARKDETA__NUMONSITE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PARKDETA__NUMONSITE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PARKDETA__NUMONSITE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PARKDETA__NUMONSITE__0" runat="server" AssociatedControlID="USER_CGPHOME_PARKDETA__NUMONSITE__0">Number of Park Homes on Site</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_PARKDETA__NUMONSITE__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PARKDETA__NUMONSITE__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PARKDETA__NUMONSITE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Number of Park Homes on Site" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0">Used For Business?</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" runat="server" ListViewName="parkhome_business_use_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PARKDETA__BUSINESSUSE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Used For Business?" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_CGPHOME_PARKDETA__REGSITE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PARKDETA__REGSITE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PARKDETA__REGSITE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PARKDETA__REGSITE__0" runat="server" AssociatedControlID="USER_CGPHOME_PARKDETA__REGSITE__0">Registered Site</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_CGPHOME_PARKDETA__REGSITE__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" runat="server" AssociatedControlID="USER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0">Unoccupancy Duration</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" runat="server" ListViewName="parkhome_occupancy_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_PARKDETA__UNOCCUPIED_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Unoccupancy Duration" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
