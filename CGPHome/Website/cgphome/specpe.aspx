<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Personal Effects Specified Items</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Personal Effects Specified Items</legend>
         <ol class="formlayout">
             <li id="subUSER_CGPHOME_SPECPE__DUMMY__0" runat="server" style="display:none;"></li>
             <li id="pUSER_CGPHOME_SPECPE__DUMMY__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_CGPHOME_SPECPE__DUMMY__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_CGPHOME_SPECPE__DUMMY__0" runat="server" AssociatedControlID="USER_CGPHOME_SPECPE__DUMMY__0">Dummy</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_CGPHOME_SPECPE__DUMMY__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_CGPHOME_SPECPE__DUMMY__0" runat="server" Branded="False" ControlToValidate="USER_CGPHOME_SPECPE__DUMMY__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Dummy" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
