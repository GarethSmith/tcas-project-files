<?xml version="1.0" encoding="utf-8"?>
<screen name="frmCLAIMNOT.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Claim Notes</caption>
    <description>Claim Notes</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>465</height>
    <parentID />
    <text>Claim Notes</text>
    <width>606</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module frmCLAIMNOT

Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lxmlUser As New Xml.XmlDocument
                Try
                    lxmlUser.LoadXml(sender.objRisk.sUserXML)
                Catch ex As Exception
                    Exit Function
                End Try

                Dim lobjNew As TextBox = GetControl("txtNewNotes", owner.Controls)
                Dim lobjExisting As TextBox = GetControl("txtNewNotes", owner.Controls)
                Dim lobjTest As String = TableLookup("user_cgphome_claimnot", "", "claimnotes")

                MessageBox.Show("Test", "CAS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)



            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
End Function

End Module
End Namespace


</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="cmdAddButt" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cmdAddButt

Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lxmlUser As New Xml.XmlDocument
                Try
                    lxmlUser.LoadXml(sender.objRisk.sUserXML)
                Catch ex As Exception
                    Exit Function
                End Try

                Dim lobjNew As TextBox = GetControl("txtNewNotes", owner.Controls)
                Dim lobjExisting As TextBox = GetControl("txtNewNotes", owner.Controls)
                Dim lobjTest As String = TableLookup("user_cgphome_claimnot", "", "claimnotes")

                GetControl("txtNewNotes", owner.Controls).Text = lobjTest

                MessageBox.Show("Test", "CAS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
End Function

End Module
End Namespace

</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Add Button</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>494</left>
            <listView />
            <top>196</top>
            <tabIndex>1</tabIndex>
            <type>0</type>
            <text>Add</text>
            <width>88</width>
        </control>
        <control name="txtNewNotes" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>NEWNOTES</columnName>
            <description>New Notes</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>173</height>
            <left>6</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>225</top>
            <width>576</width>
            <showData />
            <controls />
        </control>
        <control name="txtClaimNotes" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>CLAIMNOTES</columnName>
            <description>Claim Notes</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>173</height>
            <left>6</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>17</top>
            <width>576</width>
            <showData />
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>