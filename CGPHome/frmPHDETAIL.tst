<?xml version="1.0" encoding="utf-8"?>
<screen name="frmPHDETAIL.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Park Home Details</caption>
    <description>Park Home Details</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>421</height>
    <parentID />
    <text>Park Home Details</text>
    <width>671</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module frmPHDETAIL

        Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)

            Try

                GetControl("txtContSpecCount", owner.Controls).Enabled = False
                GetControl("txtPESpecCount", owner.Controls).Enabled = False

                If Not GetUserControlValue("frmSPECCONT.tst", "txtdummy", owner) Is Nothing Then
                    GetControl("txtContSpecCount", owner.Controls).Text = GetUserControlValue("frmSPECCONT.tst", "txtdummy", owner)
                End If

                If Not GetUserControlValue("frmSPECPE.tst", "txtDummy", owner) Is Nothing Then
                    GetControl("txtPESpecCount", owner.Controls).Text = GetUserControlValue("frmSPECPE.tst", "txtDummy", owner)
                End If

            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace














</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="txtIntParty" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>True</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>INTPARTY</columnName>
            <description>Interested Party Details</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>72</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>18</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>176</top>
            <width>312</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <startYear />
            <endYear />
            <controls />
        </control>
        <control name="grpSecurity" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>75</height>
            <left>6</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>17</tabIndex>
            <text>Security</text>
            <top>281</top>
            <width>647</width>
            <controls>
                <control name="cboWinLocks" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <boundScreen />
                    <columnName>PARKHOME_WINDOW_LOCKS_ID</columnName>
                    <description>Window Locks</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>139</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_PARKHOME_WINDOW_LOCKS</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <userFilled>False</userFilled>
                    <filteredDropdown />
                    <text />
                    <top>44</top>
                    <width>177</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel21" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text>Window Locks</text>
                    <textAlign>1</textAlign>
                    <top>47</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cboAlarmType" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <boundScreen />
                    <columnName>PARKHOME_ALARM_ID</columnName>
                    <description>Alarm Type</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>470</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_PARKHOME_ALARM</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <userFilled>False</userFilled>
                    <filteredDropdown />
                    <text />
                    <top>17</top>
                    <width>168</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel20" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>328</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <text>Alarm Type</text>
                    <textAlign>1</textAlign>
                    <top>20</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cboDoorLock" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <boundScreen />
                    <columnName>PARKHOME_DOOR_LOCKS_ID</columnName>
                    <description>Door Locks</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>139</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_PARKHOME_DOOR_LOCKS</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <userFilled>False</userFilled>
                    <filteredDropdown />
                    <text />
                    <top>17</top>
                    <width>177</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel19" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text>Door Locks</text>
                    <textAlign>16</textAlign>
                    <top>17</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
            </controls>
        </control>
        <control name="grpSumInsured" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>249</height>
            <left>328</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>16</tabIndex>
            <text>Sum Insured</text>
            <top>10</top>
            <width>325</width>
            <controls>
                <control name="txtPESpecCount" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <boundScreen />
                    <characterCasing>0</characterCasing>
                    <columnName>PESPECCOUNT</columnName>
                    <description>Count of Personal Effects Specified Items</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>148</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <maxLength>100</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>6</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>159</top>
                    <width>28</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <startYear />
                    <endYear />
                    <controls />
                </control>
                <control name="txtContSpecCount" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <boundScreen />
                    <characterCasing>0</characterCasing>
                    <columnName>CONTSPECCOUNT</columnName>
                    <description>Count Of Contents Specified Items</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>148</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <maxLength>140</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>73</top>
                    <width>29</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <startYear />
                    <endYear />
                    <controls />
                </control>
                <control name="cfdTotalSI" type="tgsCurrencyField">
                    <enableControls />
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <columnName>TOTALSI</columnName>
                    <codeEnabled>True</codeEnabled>
                    <description>TotalSI</description>
                    <decimalPlaces>2</decimalPlaces>
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <editable>True</editable>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>124</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <symbolEnabled>True</symbolEnabled>
                    <tabIndex>8</tabIndex>
                    <text />
                    <top>216</top>
                    <width>192</width>
                    <sumInsured>False</sumInsured>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel18" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>16</tabIndex>
                    <text>Total Sum Insured</text>
                    <textAlign>16</textAlign>
                    <top>211</top>
                    <width>122</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cboCycleCover" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <boundScreen />
                    <columnName>PARKHOME_PEDALCOVER_ID</columnName>
                    <description>Pedal Cycle Cover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>148</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_PARKHOME_PEDALCOVER</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>7</tabIndex>
                    <userFilled>False</userFilled>
                    <filteredDropdown />
                    <text />
                    <top>189</top>
                    <width>121</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel17" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>14</tabIndex>
                    <text>Pedal Cycle Cover</text>
                    <textAlign>1</textAlign>
                    <top>192</top>
                    <width>123</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="WisLabel15" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>33</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>11</tabIndex>
                    <text>Personal Effects Specified Items</text>
                    <textAlign>1</textAlign>
                    <top>159</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cboPECover" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <boundScreen />
                    <columnName>PARKHOME_PECOVER_ID</columnName>
                    <description>Personal Effects and Money</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>148</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_PARKHOME_PECOVER</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <userFilled>False</userFilled>
                    <filteredDropdown />
                    <text />
                    <top>129</top>
                    <width>119</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel14" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>30</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>9</tabIndex>
                    <text>Personal Effects and Money (UK Cover)</text>
                    <textAlign>1</textAlign>
                    <top>129</top>
                    <width>123</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="chkADCover" type="WisCheckBox">
                    <GotFocusCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module chkADCover

Public Function GotFocus(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lstrValue1 As String = CType((GetControl("cfdPHSI", owner.Controls).Text), String)
                Dim lstrValue2 As String = CType((GetControl("cfdContentSI", owner.Controls).Text), String)
                Dim lintTotal As Integer

                lintTotal = Val(lstrValue1) + Val(lstrValue2)

                GetControl("cfdTotalSI", owner.Controls).Text = "150"

            Catch ex As Exception
                Throw New Exception("GotFocus: " + ex.Message)
            End Try
End Function

End Module
End Namespace








</GotFocusCode>
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <boundScreen />
                    <columnName>ADCOVER</columnName>
                    <description>Accidental Damage Cover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>148</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text />
                    <textAlign>16</textAlign>
                    <top>100</top>
                    <width>29</width>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="WisLabel13" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>7</tabIndex>
                    <text>Accidental Damage?</text>
                    <textAlign>1</textAlign>
                    <top>106</top>
                    <width>127</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="WisLabel11" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>27</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text>Contents Specified Items</text>
                    <textAlign>1</textAlign>
                    <top>69</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cfdContentSI" type="tgsCurrencyField">
                    <ValueChangedCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cfdContentSI

Public Function ValueChanged(ByRef owner As Form, ByRef sender As Object)
            Try

                GetControl("cfdTotalSI", owner.Controls).Amount() = GetControl("cfdPHSI", owner.Controls).Amount + GetControl("cfdContentSI", owner.Controls).Amount

            Catch ex As Exception
                Throw New Exception("ValueChanged: " + ex.Message)
            End Try
End Function

End Module
End Namespace
</ValueChangedCode>
                    <enableControls />
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <columnName>CONTENTSI</columnName>
                    <codeEnabled>True</codeEnabled>
                    <description>Contents SI</description>
                    <decimalPlaces>2</decimalPlaces>
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <editable>True</editable>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>124</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <symbolEnabled>True</symbolEnabled>
                    <tabIndex>2</tabIndex>
                    <text />
                    <top>44</top>
                    <width>192</width>
                    <sumInsured>False</sumInsured>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel10" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <text>Contents SI</text>
                    <textAlign>16</textAlign>
                    <top>44</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
                <control name="cfdPHSI" type="tgsCurrencyField">
                    <ValueChangedCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cfdPHSI

Public Function ValueChanged(ByRef owner As Form, ByRef sender As Object)
            Try

                GetControl("cfdTotalSI", owner.Controls).Amount() = GetControl("cfdPHSI", owner.Controls).Amount + GetControl("cfdContentSI", owner.Controls).Amount

            Catch ex As Exception
                Throw New Exception("ValueChanged: " + ex.Message)
            End Try
End Function

End Module
End Namespace
</ValueChangedCode>
                    <enableControls />
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <columnName>PHSI</columnName>
                    <codeEnabled>True</codeEnabled>
                    <description>Park Home SI</description>
                    <decimalPlaces>2</decimalPlaces>
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <editable>True</editable>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>124</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <symbolEnabled>True</symbolEnabled>
                    <tabIndex>1</tabIndex>
                    <text />
                    <top>17</top>
                    <width>192</width>
                    <sumInsured>False</sumInsured>
                    <showData />
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel9" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <boundScreen />
                    <boundTransform />
                    <description />
                    <maximumTotal />
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text>Park Home SI</text>
                    <textAlign>16</textAlign>
                    <top>17</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <maskedEditBox />
                    <maskedEditBox2 />
                    <showData />
                    <controls />
                </control>
            </controls>
        </control>
        <control name="chkGSORepair" type="WisCheckBox">
            <enableControls />
            <displayPages />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>GSOREPAIR</columnName>
            <description>Good State of Repair</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>24</height>
            <left>300</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>15</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>254</top>
            <width>22</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <displayAsYesNo>False</displayAsYesNo>
            <controls />
        </control>
        <control name="WisLabel8" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>165</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <text>Good State Of Repair?</text>
            <textAlign>16</textAlign>
            <top>256</top>
            <width>138</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkSmoke" type="WisCheckBox">
            <enableControls />
            <displayPages />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>SMOKE</columnName>
            <description>Smoke Detectors?</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>24</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>13</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>254</top>
            <width>17</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <displayAsYesNo>False</displayAsYesNo>
            <controls />
        </control>
        <control name="WisLabel7" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>12</tabIndex>
            <text>Smoke Detectors?</text>
            <textAlign>16</textAlign>
            <top>256</top>
            <width>117</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkFinanced" type="WisCheckBox">
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module chkFinanced

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                If GetControl("chkFinanced", owner.Controls).Checked = False Then
                    GetControl("txtIntParty", owner.Controls).Text = Nothing
                    GetControl("txtIntParty", owner.Controls).Enabled = False
                Else
                    GetControl("txtIntParty", owner.Controls).Enabled = True
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace

</ClickCode>
            <enableControls />
            <displayPages />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>FINANCED</columnName>
            <description>Financed</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>11</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>152</top>
            <width>17</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <displayAsYesNo>False</displayAsYesNo>
            <controls />
        </control>
        <control name="WisLabel6" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>Financed?</text>
            <textAlign>16</textAlign>
            <top>152</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboFlatorFelt" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_ROOFTYPE_ID</columnName>
            <description>Flat or Felt Roof</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_ROOFTYPE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <userFilled>False</userFilled>
            <filteredDropdown />
            <text />
            <top>125</top>
            <width>177</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="WisLabel5" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text>Flat or Felt Roof?</text>
            <textAlign>16</textAlign>
            <top>125</top>
            <width>120</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboYOManufacture" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>YEAR_ID</columnName>
            <description>Year Of Manufacture</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_YEAR</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>7</tabIndex>
            <userFilled>False</userFilled>
            <filteredDropdown />
            <text />
            <top>98</top>
            <width>177</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="WisLabel4" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>6</tabIndex>
            <text>Year Of Manufacture</text>
            <textAlign>16</textAlign>
            <top>98</top>
            <width>126</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtType" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>TYPE</columnName>
            <description>Type</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>5</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>71</top>
            <width>177</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <startYear />
            <endYear />
            <controls />
        </control>
        <control name="WisLabel3" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text>Type</text>
            <textAlign>16</textAlign>
            <top>71</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboMake" type="WisComboBox">
            <SelectedIndexChangedCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cboMake

Public Function SelectedIndexChanged(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjModel As Object = GetControl("cboModel", owner.Controls)
                If Not lobjModel Is Nothing Then
                    Dim lintPortfoliokey As Integer = 112 'CType(owner, Object).objRisk.objParent.Portfoliokey
                    Dim lstrModelID As String = lobjModel.SelectedID
                    Dim lstrX As String = TableLookupFiltered("ParkHome_Model_View", lintPortfoliokey, 0, 0, "", CType(sender, Object).selectedID, "ParkHome_Make_ID")
                    lobjModel.FillFromXml(lstrX)
                    lobjModel.SelectedID = lstrModelID
                End If

            Catch ex As Exception
                Throw New Exception("SelectedIndexChanged: " + ex.Message)
            End Try
End Function

End Module
End Namespace






</SelectedIndexChangedCode>
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_MAKE_ID</columnName>
            <description>Make</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_MAKE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <userFilled>False</userFilled>
            <filteredDropdown />
            <text />
            <top>17</top>
            <width>177</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="WisLabel1" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Make</text>
            <textAlign>16</textAlign>
            <top>17</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
		        <control name="cboModel" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_MODEL_ID</columnName>
            <description>Model</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>145</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_MODEL</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <userFilled>False</userFilled>
            <filteredDropdown />
            <text />
            <top>44</top>
            <width>177</width>
            <showData />
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="WisLabel2" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <maximumTotal />
            <minimumTotal />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Model</text>
            <textAlign>16</textAlign>
            <top>44</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
    <formValidation />
    <hideClaim />
</screen>