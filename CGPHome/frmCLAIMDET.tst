<?xml version="1.0" encoding="utf-8"?>
<screen name="frmCLAIMDET.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Claim Details</caption>
    <description>Claim Details</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>737</height>
    <parentID />
    <text>Claim Details</text>
    <width>417</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module frmCLAIMDET

        Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjStatus As ComboBox = GetControl("cboClaimStatus", owner.Controls)

                If lobjStatus.Text = "Settled" Then
                    GetControl("cfdSettledAmount", owner.Controls).Enabled = True
                    GetControl("mskSettledDate", owner.Controls).Enabled = True
                Else
                    GetControl("cfdSettledAmount", owner.Controls).Enabled = False
                    GetControl("mskSettledDate", owner.Controls).Enabled = False
                End If

                Dim lobjNotes As TextBox = GetControl("txtNotesView", owner.Controls)

                lobjNotes.ReadOnly = True
                lobjNotes.ScrollBars = ScrollBars.Vertical


            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace





</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="txtNewNotes" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>NEWNOTES</columnName>
            <description>New Notes</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>80</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>34</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>574</top>
            <width>385</width>
            <showData />
            <controls />
        </control>
        <control name="cmdAddNotes" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module cmdAddNotes

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjView As TextBox = GetControl("txtNotesView", owner.Controls)
                Dim lobjNew As TextBox = GetControl("txtNewNotes", owner.Controls)

                Dim ldteNow As Date = Now()

                If lobjView.Text = "" Then
                    lobjView.Text = ldteNow.ToString + " - " + lobjNew.Text
                Else
                    lobjView.Text += vbNewLine + ldteNow.ToString + " - " + lobjNew.Text
                End If

                'Clear the new notes sections
                lobjNew.Text = ""

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Add Notes</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>320</left>
            <listView />
            <top>545</top>
            <tabIndex>33</tabIndex>
            <type>0</type>
            <text>Add Notes</text>
            <width>75</width>
        </control>
        <control name="txtNotesView" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>True</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>NOTESVIEW</columnName>
            <description>Notes View</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>80</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>32</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>459</top>
            <width>385</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel4" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>31</tabIndex>
            <text>Notes:</text>
            <textAlign>1</textAlign>
            <top>442</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="WisLabel9" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>16</tabIndex>
            <text>Amount Claimed</text>
            <textAlign>1</textAlign>
            <top>258</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkAtFault" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>ATFAULT</columnName>
            <description>At Fault</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>317</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>29</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>367</top>
            <width>78</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel15" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>232</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>28</tabIndex>
            <text>At Fault?</text>
            <textAlign>1</textAlign>
            <top>372</top>
            <width>63</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkIncOnThisPolicy" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>INCONTHISPOLICY</columnName>
            <description>Incurred On This Policy</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>24</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>27</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>396</top>
            <width>39</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel14" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>31</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>26</tabIndex>
            <text>Incurred On This Policy?</text>
            <textAlign>1</textAlign>
            <top>402</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkNCDPredjudiced" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls />
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>NCDPREDJUDICED</columnName>
            <description>NCD Predjudiced</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>24</height>
            <left>127</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>25</tabIndex>
            <checkAlign>16</checkAlign>
            <text />
            <textAlign>16</textAlign>
            <top>366</top>
            <width>39</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel13" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>24</tabIndex>
            <text>NCD Prejudiced?</text>
            <textAlign>1</textAlign>
            <top>372</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtLegalRef" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>LEGALREF</columnName>
            <description>Legal Reference Number</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>23</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>339</top>
            <width>269</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel12" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>22</tabIndex>
            <text>Legal Ref. Number</text>
            <textAlign>1</textAlign>
            <top>341</top>
            <width>113</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cfdSettledAmount" type="tgsCurrencyField">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <ValueChangedCode />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <columnName>SETTLEDAMOUNT</columnName>
            <codeEnabled>True</codeEnabled>
            <description>Settled Amount</description>
            <enabled>True</enabled>
            <editable>True</editable>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>102</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <symbolEnabled>True</symbolEnabled>
            <tabIndex>21</tabIndex>
            <text />
            <top>312</top>
            <width>172</width>
            <sumInsured>False</sumInsured>
            <showData />
            <controls />
        </control>
        <control name="WisLabel11" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>20</tabIndex>
            <text>Settled Amount</text>
            <textAlign>1</textAlign>
            <top>312</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="mskSettledDate" type="WisMaskedBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>SETTLEDDATE</columnName>
            <description>Settlement Date</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <mask>##/##/####</mask>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>19</tabIndex>
            <text>__/__/____</text>
            <textAlign>0</textAlign>
            <top>285</top>
            <width>100</width>
            <boundObject>0</boundObject>
            <showData />
            <controls />
        </control>
        <control name="WisLabel10" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>18</tabIndex>
            <text>Settlement Date</text>
            <textAlign>1</textAlign>
            <top>287</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cfdAmountClaimed" type="tgsCurrencyField">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <ValueChangedCode />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <columnName>AMOUNTCLAIMED</columnName>
            <codeEnabled>True</codeEnabled>
            <description>Amount Claimed</description>
            <enabled>True</enabled>
            <editable>True</editable>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>102</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <symbolEnabled>True</symbolEnabled>
            <tabIndex>17</tabIndex>
            <text />
            <top>258</top>
            <width>172</width>
            <sumInsured>False</sumInsured>
            <showData />
            <controls />
        </control>
        <control name="txtDescription" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>DESCRIPTION</columnName>
            <description>Claim Description</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>95</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>15</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>157</top>
            <width>269</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel8" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <text>Description</text>
            <textAlign>1</textAlign>
            <top>159</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboClaimStatus" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cboClaimStatus

Public Function TextChanged(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjStatus As ComboBox = GetControl("cboClaimStatus", owner.Controls)


                If lobjStatus.Text = "Settled" Then
                    GetControl("cfdSettledAmount", owner.Controls).Enabled = True
                    GetControl("mskSettledDate", owner.Controls).Enabled = True
                Else
                    GetControl("cfdSettledAmount", owner.Controls).Enabled = False
                    GetControl("mskSettledDate", owner.Controls).Enabled = False
                End If

            Catch ex As Exception
                Throw New Exception("TextChanged: " + ex.Message)
            End Try
End Function

End Module
End Namespace



</TextChangedCode>
            <SelectedIndexChangedCode />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_CLAIM_STATUS_ID</columnName>
            <description>Claim Status</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_CLAIM_STATUS</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>13</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>130</top>
            <width>269</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel7" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>12</tabIndex>
            <text>Claim Status</text>
            <textAlign>1</textAlign>
            <top>133</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboClaimDetails" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <SelectedIndexChangedCode />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_CLAIM_DETAILS_ID</columnName>
            <description>Claim Details</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_CLAIM_DETAILS</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>11</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>103</top>
            <width>269</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel6" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>Details</text>
            <textAlign>1</textAlign>
            <top>106</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboClaimType" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <SelectedIndexChangedCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cboClaimType

Public Function SelectedIndexChanged(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjModel As Object = GetControl("cboClaimDetails", owner.Controls)
                If Not lobjModel Is Nothing Then
                    Dim lintPortfoliokey As Integer = 112 'CType(owner, Object).objRisk.objParent.Portfoliokey
                    Dim lstrModelID As String = lobjModel.SelectedID
                    Dim lstrX As String = TableLookupFiltered("ParkHome_Claim_Details_View", lintPortfoliokey, 0, 0, "", CType(sender, Object).selectedID, "ParkHome_Claim_Type_ID")
                    lobjModel.FillFromXml(lstrX)
                    lobjModel.SelectedID = lstrModelID
                End If

            Catch ex As Exception
                Throw New Exception("SelectedIndexChanged: " + ex.Message)
            End Try
End Function

End Module
End Namespace

</SelectedIndexChangedCode>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>PARKHOME_CLAIM_TYPE_ID</columnName>
            <description>Claim Type</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_PARKHOME_CLAIM_TYPE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>76</top>
            <width>269</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel5" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text>Claim Type</text>
            <textAlign>1</textAlign>
            <top>79</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="mskIncidTime" type="WisMaskedBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>INCIDTIME</columnName>
            <description>Incident Time</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>295</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <mask>##:##</mask>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>5</tabIndex>
            <text>__:__</text>
            <textAlign>0</textAlign>
            <top>42</top>
            <width>100</width>
            <boundObject>0</boundObject>
            <showData />
            <controls />
        </control>
        <control name="WisLabel3" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>232</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text>Time</text>
            <textAlign>1</textAlign>
            <top>44</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="mskIncidDate" type="WisMaskedBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>INCIDDATE</columnName>
            <description>Incident Date</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <mask>##/##/####</mask>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <text>__/__/____</text>
            <textAlign>0</textAlign>
            <top>42</top>
            <width>100</width>
            <boundObject>0</boundObject>
            <showData />
            <controls />
        </control>
        <control name="WisLabel2" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Incident Date</text>
            <textAlign>1</textAlign>
            <top>44</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtClaimRef" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>CLAIMREF</columnName>
            <description>Claim Reference</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>15</top>
            <width>100</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel1" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>17</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Claim Reference</text>
            <textAlign>1</textAlign>
            <top>17</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>