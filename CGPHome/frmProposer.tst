<?xml version="1.0" encoding="utf-8"?>
<screen name="frmProposer.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption />
    <description />
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>405</height>
    <parentID />
    <text>PolicyHolder Details</text>
    <width>393</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module frmProposer

        Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjControl As Control

                If Not CType(owner, Object).objRisk Is Nothing Then
                    If CType(owner, Object).Index = 1 Then
                        If Not CType(owner, Object).objRisk.objClient Is Nothing Then
                            'Populate the client name through to the name fields on this screen
                            With CType(owner, Object).objRisk.objClient
                                If Trim(GetControl("cboTitle", owner.Controls).text) = "" Then GetControl("cboTitle", owner.Controls).text = .objTitle.sDesc
                                If Trim(GetControl("txtForename", owner.Controls).text) = "" Then GetControl("txtForename", owner.Controls).text = .sForename
                                If Trim(GetControl("txtInitials", owner.Controls).text) = "" Then GetControl("txtInitials", owner.Controls).text = .sInitials
                                If Trim(GetControl("txtSurname", owner.Controls).text) = "" Then GetControl("txtSurname", owner.Controls).text = .sSurname
                                If (Trim(GetControl("mskDOB", owner.Controls).text) = "__/__/____" And .dtDOB &lt;&gt; "00:00:00") Then GetControl("mskDOB", owner.Controls).text = .dtDOB
                            End With
                        End If
                    End If
                End If

                If Not GetControl("mskDOB", owner.Controls).text = "__/__/____" Then
                    Dim lstrDate1 As Date = Date.Now.Date
                    Dim lstrDate2 As Date = CType(GetControl("mskDOB", owner.Controls).Text, DateTime)
                    Dim lstrAge As TextBox = GetControl("txtAge", owner.Controls)
                    lstrAge.Text = Math.Round(-DateDiff("y", Today, CDate(lstrDate2.Date)) / 365.25, 0)
                End If

                Dim lsrtAge As TextBox = GetControl("txtAge", owner.Controls)
                lsrtAge.Enabled = False

            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace

</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="grpOccupation" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>125</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>17</tabIndex>
            <text>Occupation</text>
            <top>208</top>
            <width>361</width>
            <controls>
                <control name="cmdOccRemove" type="WisButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>Occupation Remove</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>280</left>
                    <listView>lvwOccList</listView>
                    <top>91</top>
                    <tabIndex>3</tabIndex>
                    <type>5</type>
                    <text>    &amp;Remove</text>
                    <width>75</width>
                </control>
                <control name="cmdOccView" type="WisButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>Occupation View</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>199</left>
                    <listView>lvwOccList</listView>
                    <top>91</top>
                    <tabIndex>2</tabIndex>
                    <type>4</type>
                    <text>  &amp;Edit</text>
                    <width>75</width>
                </control>
                <control name="cmdOccAdd" type="WisButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>Occupation Add</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>118</left>
                    <listView>lvwOccList</listView>
                    <top>91</top>
                    <tabIndex>1</tabIndex>
                    <type>3</type>
                    <text>  &amp;Add</text>
                    <width>75</width>
                </control>
                <control name="lvwOccList" type="WisListView">
                    <columns>
                        <column>
                            <text>Description</text>
                            <Width>132</Width>
                            <bindControl>cboDescription</bindControl>
                        </column>
                        <column>
                            <text>Employers Business</text>
                            <Width>130</Width>
                            <bindControl>cboEmpsBusiness</bindControl>
                        </column>
                        <column>
                            <text>Status</text>
                            <Width>85</Width>
                            <bindControl>cboType</bindControl>
                        </column>
                    </columns>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <boundScreen>CGPHome\frmOccupat.tst</boundScreen>
                    <description>Occupation List</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>65</height>
                    <left>6</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text />
                    <top>20</top>
                    <width>349</width>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="cboRelationship" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <SelectedIndexChangedCode />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>RELATIONSHIP_ID</columnName>
            <description>Relationship</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_RELATIONSHIP</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>16</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>179</top>
            <width>273</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel8" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>15</tabIndex>
            <text>Relationship</text>
            <textAlign>1</textAlign>
            <top>182</top>
            <width>84</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="cboMaritalStatus" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <SelectedIndexChangedCode />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>MARITAL_STATUS_ID</columnName>
            <description>Marital Status</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_MARITAL_STATUS</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>152</top>
            <width>273</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel7" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>13</tabIndex>
            <text>Marital Status</text>
            <textAlign>1</textAlign>
            <top>155</top>
            <width>84</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtAge" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>AGE</columnName>
            <description>Age</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>232</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>10</maxLength>
            <multiLine>False</multiLine>
            <numeric>True</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>12</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>124</top>
            <width>49</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel6" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>192</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>11</tabIndex>
            <text>Age</text>
            <textAlign>1</textAlign>
            <top>126</top>
            <width>34</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="mskDOB" type="WisMaskedBox">
            <LostFocusCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module mskDOB

        Public Function LostFocus(ByRef owner As Form, ByRef sender As Object)
            Try

                If Not GetControl("mskDOB", owner.Controls).text = "__/__/____" Then
                    Dim lstrDate1 As Date = Date.Now.Date
                    Dim lstrDate2 As Date = CType(GetControl("mskDOB", owner.Controls).Text, DateTime)
                    Dim lstrAge As TextBox = GetControl("txtAge", owner.Controls)
                    lstrAge.Text = Math.Round(-DateDiff("y", Today, CDate(lstrDate2.Date)) / 365.25, 0)
                End If

            Catch ex As Exception
                Throw New Exception("LostFocus: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace</LostFocusCode>
            <GotFocusCode />
            <ClickCode />
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>DOB</columnName>
            <description>DOB</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <mask>##/##/####</mask>
            <maxLength>32767</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>__/__/____</text>
            <textAlign>0</textAlign>
            <top>125</top>
            <width>64</width>
            <boundObject>0</boundObject>
            <showData />
            <controls />
        </control>
        <control name="WisLabel5" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <text>DOB</text>
            <textAlign>1</textAlign>
            <top>126</top>
            <width>32</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtSurname" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>SURNAME</columnName>
            <description>Surname</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>100</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>97</top>
            <width>183</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel4" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>7</tabIndex>
            <text>Surname</text>
            <textAlign>1</textAlign>
            <top>99</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtInitials" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>INITIALS</columnName>
            <description>Other Initials</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>100</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>6</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>70</top>
            <width>183</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel3" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>5</tabIndex>
            <text>Other Initials</text>
            <textAlign>1</textAlign>
            <top>72</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="txtForename" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>FORENAME</columnName>
            <description>Forename</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>100</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>43</top>
            <width>183</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel2" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <text>Forename</text>
            <textAlign>1</textAlign>
            <top>45</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="grpSex" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>136</height>
            <left>287</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Sex</text>
            <top>10</top>
            <width>84</width>
            <controls>
                <control name="optCompany" type="WisRadioButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <boundScreen />
                    <columnName>COMPANY</columnName>
                    <description>Company</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>Company</text>
                    <textAlign>16</textAlign>
                    <top>106</top>
                    <width>76</width>
                    <showData />
                    <controls />
                </control>
                <control name="optNA" type="WisRadioButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <boundScreen />
                    <columnName>NA</columnName>
                    <description>N/A</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>N/A</text>
                    <textAlign>16</textAlign>
                    <top>76</top>
                    <width>66</width>
                    <showData />
                    <controls />
                </control>
                <control name="optFemale" type="WisRadioButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <boundScreen />
                    <columnName>FEMALE</columnName>
                    <description>Female</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>Female</text>
                    <textAlign>16</textAlign>
                    <top>46</top>
                    <width>66</width>
                    <showData />
                    <controls />
                </control>
                <control name="optMale" type="WisRadioButton">
                    <LostFocusCode />
                    <GotFocusCode />
                    <ClickCode />
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <boundScreen />
                    <columnName>MALE</columnName>
                    <description>Male</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>Male</text>
                    <textAlign>16</textAlign>
                    <top>16</top>
                    <width>56</width>
                    <showData />
                    <controls />
                </control>
            </controls>
        </control>
        <control name="cboTitle" type="WisComboBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <SelectedIndexChangedCode />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <boundScreen />
            <columnName>TITLE_ID</columnName>
            <description>Title</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>98</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <listTable>LIST_TITLE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>16</top>
            <width>183</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel1" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Title</text>
            <textAlign>1</textAlign>
            <top>19</top>
            <width>68</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns>
            <column>
                <text>Forename</text>
                <bindControl>txtForename</bindControl>
            </column>
            <column>
                <text>Surname</text>
                <bindControl>txtSurname</bindControl>
            </column>
            <column>
                <text>DOB</text>
                <bindControl>mskDOB</bindControl>
            </column>
            <column>
                <text>Marital Status</text>
                <bindControl>cboMaritalStatus</bindControl>
            </column>
            <column>
                <text>Relationship</text>
                <bindControl>cboRelationship</bindControl>
            </column>
        </columns>
    </generalSummary>
</screen>