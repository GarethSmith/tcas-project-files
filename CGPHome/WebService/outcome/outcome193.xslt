<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:outcome="http://www.wisl.co.uk/schemas" xmlns:func="http://www.wisl.co.uk/core/functions">

	<xsl:import href="outcomeClient.xslt" />
	<xsl:import href="outcomePolicy.xslt" />
	<xsl:import href="outcomeAddon.xslt" />
	<xsl:import href="outcomeclaims.xslt" />
	<xsl:import href="../core/format.xslt" />

	<xsl:template name="outcome193" match="/outcome:client">

		<xsl:param name="QuoteStage"/>
		<xsl:param name="AgentName"/>
		<xsl:variable name="vPolicyDetailsID" select="outcome:policies/outcome:policy/@iD" />

		<System xmlns="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.wisl.co.uk/schemas http://www.wisl-test.co.uk/schemas/core/outcome.xsd">
			<Client>
				<xsl:call-template name="outcomeClient">
					<xsl:with-param name="AgentName" select="$AgentName" />
				</xsl:call-template>
				<Policy_Details>
					<xsl:call-template name="outcomePolicy">
						<xsl:with-param name="QuoteStage" select="$QuoteStage" />
					</xsl:call-template>
				    <xsl:call-template name="outcomeAddon" />
				    <xsl:call-template name="outcomeClaims" />
                     <xsl:call-template name="frmPHDETAIL.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmSPECCONT.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmSPECPE.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmPARKDETA.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmCLAIMSUM.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmXSANDNCD.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmProposer.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmGeneral.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
				</Policy_Details>
			</Client>
		</System>

	</xsl:template>

     <xsl:template name="frmPHDETAIL.tst" match="frmPHDETAIL.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmPHDETAIL.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_PHDETAIL xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_CGPHOME_PHDETAIL_WinLocks><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboWinLocks']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_WinLocks>
<UQ_CGPHOME_PHDETAIL_AlarmType><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboAlarmType']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_AlarmType>
<UQ_CGPHOME_PHDETAIL_DoorLock><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboDoorLock']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_DoorLock>
                 <UQ_CGPHOME_PHDETAIL_PESpecCount><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtPESpecCount']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_PESpecCount>
                 <UQ_CGPHOME_PHDETAIL_ContSpecCount><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtContSpecCount']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_ContSpecCount>
                 <UQ_CGPHOME_PHDETAIL_TotalSI><xsl:value-of select="outcome:controls/outcome:control[@name='cfdTotalSI']/outcome:value" /></UQ_CGPHOME_PHDETAIL_TotalSI>
<UQ_CGPHOME_PHDETAIL_CycleCover><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboCycleCover']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_CycleCover>
<UQ_CGPHOME_PHDETAIL_PECover><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboPECover']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_PECover>
                 <UQ_CGPHOME_PHDETAIL_ADCover><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkADCover']/outcome:value='1' or outcome:controls/outcome:control[@name='chkADCover']/outcome:value='true')" /></UQ_CGPHOME_PHDETAIL_ADCover>
                 <UQ_CGPHOME_PHDETAIL_ContentSI><xsl:value-of select="outcome:controls/outcome:control[@name='cfdContentSI']/outcome:value" /></UQ_CGPHOME_PHDETAIL_ContentSI>
                 <UQ_CGPHOME_PHDETAIL_PHSI><xsl:value-of select="outcome:controls/outcome:control[@name='cfdPHSI']/outcome:value" /></UQ_CGPHOME_PHDETAIL_PHSI>
                 <UQ_CGPHOME_PHDETAIL_GSORepair><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkGSORepair']/outcome:value='1' or outcome:controls/outcome:control[@name='chkGSORepair']/outcome:value='true')" /></UQ_CGPHOME_PHDETAIL_GSORepair>
                 <UQ_CGPHOME_PHDETAIL_Smoke><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkSmoke']/outcome:value='1' or outcome:controls/outcome:control[@name='chkSmoke']/outcome:value='true')" /></UQ_CGPHOME_PHDETAIL_Smoke>
                 <UQ_CGPHOME_PHDETAIL_Financed><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkFinanced']/outcome:value='1' or outcome:controls/outcome:control[@name='chkFinanced']/outcome:value='true')" /></UQ_CGPHOME_PHDETAIL_Financed>
<UQ_CGPHOME_PHDETAIL_FlatorFelt><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboFlatorFelt']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_FlatorFelt>
<UQ_CGPHOME_PHDETAIL_YOManufacture><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboYOManufacture']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_YOManufacture>
                 <UQ_CGPHOME_PHDETAIL_Type><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtType']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_Type>
<UQ_CGPHOME_PHDETAIL_Model><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboModel']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_Model>
<UQ_CGPHOME_PHDETAIL_Make><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboMake']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_Make>
                 <UQ_CGPHOME_PHDETAIL_IntParty><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtIntParty']/outcome:value)" /></UQ_CGPHOME_PHDETAIL_IntParty>
             </UO_CGPHOME_PHDETAIL>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmSPECCONT.tst" match="frmSPECCONT.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmSPECCONT.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_SPECCONT xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_SPECCONT_dummy><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtdummy']/outcome:value)" /></UQ_CGPHOME_SPECCONT_dummy>

                 <xsl:call-template name="frmITEMSPEC.tst">
                     <xsl:with-param name="pParentID" select="@iD" />
                 </xsl:call-template>
             </UO_CGPHOME_SPECCONT>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmSPECPE.tst" match="frmSPECPE.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmSPECPE.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_SPECPE xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_SPECPE_Dummy><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDummy']/outcome:value)" /></UQ_CGPHOME_SPECPE_Dummy>

                 <xsl:call-template name="frmSPECITEM.tst">
                     <xsl:with-param name="pParentID" select="@iD" />
                 </xsl:call-template>
             </UO_CGPHOME_SPECPE>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmPARKDETA.tst" match="frmPARKDETA.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmPARKDETA.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_PARKDETA xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_CGPHOME_PARKDETA_Unoccupied><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboUnoccupied']/outcome:value)" /></UQ_CGPHOME_PARKDETA_Unoccupied>
                 <UQ_CGPHOME_PARKDETA_RegSite><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRegSite']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRegSite']/outcome:value='true')" /></UQ_CGPHOME_PARKDETA_RegSite>
<UQ_CGPHOME_PARKDETA_BusinessUse><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboBusinessUse']/outcome:value)" /></UQ_CGPHOME_PARKDETA_BusinessUse>
                 <UQ_CGPHOME_PARKDETA_NumOnSite><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtNumOnSite']/outcome:value)" /></UQ_CGPHOME_PARKDETA_NumOnSite>
             </UO_CGPHOME_PARKDETA>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmCLAIMSUM.tst" match="frmCLAIMSUM.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmCLAIMSUM.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_CLAIMSUM xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>

                 <xsl:call-template name="frmCLAIMDET.tst">
                     <xsl:with-param name="pParentID" select="@iD" />
                 </xsl:call-template>
             </UO_CGPHOME_CLAIMSUM>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmCLAIMDET.tst" match="frmCLAIMDET.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmCLAIMDET.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_CLAIMDET xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_CLAIMDET_AtFault><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkAtFault']/outcome:value='1' or outcome:controls/outcome:control[@name='chkAtFault']/outcome:value='true')" /></UQ_CGPHOME_CLAIMDET_AtFault>
                 <UQ_CGPHOME_CLAIMDET_IncOnThisPolicy><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkIncOnThisPolicy']/outcome:value='1' or outcome:controls/outcome:control[@name='chkIncOnThisPolicy']/outcome:value='true')" /></UQ_CGPHOME_CLAIMDET_IncOnThisPolicy>
                 <UQ_CGPHOME_CLAIMDET_NCDPredjudiced><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkNCDPredjudiced']/outcome:value='1' or outcome:controls/outcome:control[@name='chkNCDPredjudiced']/outcome:value='true')" /></UQ_CGPHOME_CLAIMDET_NCDPredjudiced>
                 <UQ_CGPHOME_CLAIMDET_LegalRef><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtLegalRef']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_LegalRef>
                 <UQ_CGPHOME_CLAIMDET_SettledAmount><xsl:value-of select="outcome:controls/outcome:control[@name='cfdSettledAmount']/outcome:value" /></UQ_CGPHOME_CLAIMDET_SettledAmount>
                 <UQ_CGPHOME_CLAIMDET_SettledDate><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskSettledDate']/outcome:value,2)" /></UQ_CGPHOME_CLAIMDET_SettledDate>
                 <UQ_CGPHOME_CLAIMDET_AmountClaimed><xsl:value-of select="outcome:controls/outcome:control[@name='cfdAmountClaimed']/outcome:value" /></UQ_CGPHOME_CLAIMDET_AmountClaimed>
                 <UQ_CGPHOME_CLAIMDET_Description><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDescription']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_Description>
<UQ_CGPHOME_CLAIMDET_ClaimStatus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimStatus']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_ClaimStatus>
<UQ_CGPHOME_CLAIMDET_ClaimDetails><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimDetails']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_ClaimDetails>
<UQ_CGPHOME_CLAIMDET_ClaimType><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimType']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_ClaimType>
                 <UQ_CGPHOME_CLAIMDET_IncidTime><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskIncidTime']/outcome:value,2)" /></UQ_CGPHOME_CLAIMDET_IncidTime>
                 <UQ_CGPHOME_CLAIMDET_IncidDate><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskIncidDate']/outcome:value,2)" /></UQ_CGPHOME_CLAIMDET_IncidDate>
                 <UQ_CGPHOME_CLAIMDET_ClaimRef><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtClaimRef']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_ClaimRef>
                 <UQ_CGPHOME_CLAIMDET_NotesView><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtNotesView']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_NotesView>
                 <UQ_CGPHOME_CLAIMDET_NewNotes><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtNewNotes']/outcome:value)" /></UQ_CGPHOME_CLAIMDET_NewNotes>
             </UO_CGPHOME_CLAIMDET>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmITEMSPEC.tst" match="frmITEMSPEC.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmITEMSPEC.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_ITEMSPEC xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_ITEMSPEC_ItemSerial><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtItemSerial']/outcome:value)" /></UQ_CGPHOME_ITEMSPEC_ItemSerial>
                 <UQ_CGPHOME_ITEMSPEC_ItemModel><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtItemModel']/outcome:value)" /></UQ_CGPHOME_ITEMSPEC_ItemModel>
                 <UQ_CGPHOME_ITEMSPEC_ItemMake><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtItemMake']/outcome:value)" /></UQ_CGPHOME_ITEMSPEC_ItemMake>
                 <UQ_CGPHOME_ITEMSPEC_ItemValue><xsl:value-of select="outcome:controls/outcome:control[@name='cfdItemValue']/outcome:value" /></UQ_CGPHOME_ITEMSPEC_ItemValue>
                 <UQ_CGPHOME_ITEMSPEC_ItemDesc><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtItemDesc']/outcome:value)" /></UQ_CGPHOME_ITEMSPEC_ItemDesc>
             </UO_CGPHOME_ITEMSPEC>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmSPECITEM.tst" match="frmSPECITEM.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmSPECITEM.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_SPECITEM xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_SPECITEM_ITEMSERIAL><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtITEMSERIAL']/outcome:value)" /></UQ_CGPHOME_SPECITEM_ITEMSERIAL>
                 <UQ_CGPHOME_SPECITEM_ITEMMODEL><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtITEMMODEL']/outcome:value)" /></UQ_CGPHOME_SPECITEM_ITEMMODEL>
                 <UQ_CGPHOME_SPECITEM_ITEMMAKE><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtITEMMAKE']/outcome:value)" /></UQ_CGPHOME_SPECITEM_ITEMMAKE>
                 <UQ_CGPHOME_SPECITEM_ITEMVALUE><xsl:value-of select="outcome:controls/outcome:control[@name='cfdITEMVALUE']/outcome:value" /></UQ_CGPHOME_SPECITEM_ITEMVALUE>
                 <UQ_CGPHOME_SPECITEM_ITEMDESC><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtITEMDESC']/outcome:value)" /></UQ_CGPHOME_SPECITEM_ITEMDESC>
             </UO_CGPHOME_SPECITEM>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmXSANDNCD.tst" match="frmXSANDNCD.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmXSANDNCD.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_XSANDNCD xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_CGPHOME_XSANDNCD_NCD><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboNCD']/outcome:value)" /></UQ_CGPHOME_XSANDNCD_NCD>
<UQ_CGPHOME_XSANDNCD_Excess><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboExcess']/outcome:value)" /></UQ_CGPHOME_XSANDNCD_Excess>
             </UO_CGPHOME_XSANDNCD>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmProposer.tst" match="frmProposer.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmProposer.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_Proposer xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_CGPHOME_Proposer_Relationship><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboRelationship']/outcome:value)" /></UQ_CGPHOME_Proposer_Relationship>
<UQ_CGPHOME_Proposer_MaritalStatus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboMaritalStatus']/outcome:value)" /></UQ_CGPHOME_Proposer_MaritalStatus>
                 <UQ_CGPHOME_Proposer_Age><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtAge']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtAge']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_CGPHOME_Proposer_Age>
                 <UQ_CGPHOME_Proposer_DOB><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskDOB']/outcome:value,2)" /></UQ_CGPHOME_Proposer_DOB>
                 <UQ_CGPHOME_Proposer_Surname><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSurname']/outcome:value)" /></UQ_CGPHOME_Proposer_Surname>
                 <UQ_CGPHOME_Proposer_Initials><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtInitials']/outcome:value)" /></UQ_CGPHOME_Proposer_Initials>
                 <UQ_CGPHOME_Proposer_Forename><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtForename']/outcome:value)" /></UQ_CGPHOME_Proposer_Forename>
                 <UQ_CGPHOME_Proposer_Company><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optCompany']/outcome:value='1' or outcome:controls/outcome:control[@name='optCompany']/outcome:value='true')" /></UQ_CGPHOME_Proposer_Company>
                 <UQ_CGPHOME_Proposer_NA><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optNA']/outcome:value='1' or outcome:controls/outcome:control[@name='optNA']/outcome:value='true')" /></UQ_CGPHOME_Proposer_NA>
                 <UQ_CGPHOME_Proposer_Female><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optFemale']/outcome:value='1' or outcome:controls/outcome:control[@name='optFemale']/outcome:value='true')" /></UQ_CGPHOME_Proposer_Female>
                 <UQ_CGPHOME_Proposer_Male><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optMale']/outcome:value='1' or outcome:controls/outcome:control[@name='optMale']/outcome:value='true')" /></UQ_CGPHOME_Proposer_Male>
<UQ_CGPHOME_Proposer_Title><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTitle']/outcome:value)" /></UQ_CGPHOME_Proposer_Title>

                 <xsl:call-template name="frmOccupat.tst">
                     <xsl:with-param name="pParentID" select="@iD" />
                 </xsl:call-template>
             </UO_CGPHOME_Proposer>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmOccupat.tst" match="frmOccupat.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmOccupat.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_Occupat xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_Occupat_PartTime><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkPartTime']/outcome:value='1' or outcome:controls/outcome:control[@name='chkPartTime']/outcome:value='true')" /></UQ_CGPHOME_Occupat_PartTime>
<UQ_CGPHOME_Occupat_Type><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboType']/outcome:value)" /></UQ_CGPHOME_Occupat_Type>
<UQ_CGPHOME_Occupat_EmpsBusiness><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpsBusiness']/outcome:value)" /></UQ_CGPHOME_Occupat_EmpsBusiness>
<UQ_CGPHOME_Occupat_Description><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboDescription']/outcome:value)" /></UQ_CGPHOME_Occupat_Description>
             </UO_CGPHOME_Occupat>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmGeneral.tst" match="frmGeneral.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmGeneral.tst' and @parentID=$pParentID]">
             <UO_CGPHOME_General xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGPHOME_General_Details><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDetails']/outcome:value)" /></UQ_CGPHOME_General_Details>
                 <UQ_CGPHOME_General_Requested><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRequested']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRequested']/outcome:value='true')" /></UQ_CGPHOME_General_Requested>
                 <UQ_CGPHOME_General_Convicted><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='1' or outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='true')" /></UQ_CGPHOME_General_Convicted>
                 <UQ_CGPHOME_General_Refused><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRefused']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRefused']/outcome:value='true')" /></UQ_CGPHOME_General_Refused>
             </UO_CGPHOME_General>
         </xsl:for-each>
     </xsl:template>

</xsl:stylesheet>
