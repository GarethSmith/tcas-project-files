<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmPHDETAIL.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmSPECCONT.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmSPECPE.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPARKDETA.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmCLAIMSUM.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmXSANDNCD.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmProposer.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmGeneral.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmPHDETAIL.tst" match="frmPHDETAIL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmPHDETAIL[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPHDETAIL.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_phdetail_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboWinLocks</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="winlocks_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="winlocks_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboAlarmType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="alarmtype_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="alarmtype_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboDoorLock</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="doorlock_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="doorlock_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtPESpecCount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="pespeccount" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtContSpecCount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="contspeccount" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdTotalSI</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="totalsi" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboCycleCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="cyclecover_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="cyclecover_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboPECover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="pecover_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="pecover_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="adcover" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdContentSI</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="contentsi" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdPHSI</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="phsi" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkGSORepair</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="gsorepair" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkSmoke</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="smoke" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkFinanced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="financed" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboFlatorFelt</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="flatorfelt_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="flatorfelt_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboYOManufacture</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="yomanufacture_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="yomanufacture_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="type" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboModel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="model_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="model_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMake</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="make_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="make_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtIntParty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="intparty" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECCONT.tst" match="frmSPECCONT">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmSPECCONT[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECCONT.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_speccont_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtdummy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="dummy" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmITEMSPEC.tst">
             <xsl:with-param name="pParentID" select="cgphome_speccont_id" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECPE.tst" match="frmSPECPE">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmSPECPE[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECPE.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_specpe_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDummy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="dummy" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmSPECITEM.tst">
             <xsl:with-param name="pParentID" select="cgphome_specpe_id" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPARKDETA.tst" match="frmPARKDETA">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmPARKDETA[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPARKDETA.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_parkdeta_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboUnoccupied</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="unoccupied_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="unoccupied_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRegSite</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="regsite" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboBusinessUse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="businessuse_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="businessuse_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNumOnSite</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="numonsite" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmCLAIMSUM.tst" match="frmCLAIMSUM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmCLAIMSUM[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmCLAIMSUM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_claimsum_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
         </controls>

         <xsl:call-template name="frmCLAIMDET.tst">
             <xsl:with-param name="pParentID" select="cgphome_claimsum_id" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmCLAIMDET.tst" match="frmCLAIMDET">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmCLAIMDET[cgphome_claimsum_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmCLAIMDET.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_claimdet_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="cgphome_claimsum_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkAtFault</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="atfault" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkIncOnThisPolicy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="inconthispolicy" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkNCDPredjudiced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ncdpredjudiced" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLegalRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="legalref" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdSettledAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="settledamount" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskSettledDate</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="settleddate" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdAmountClaimed</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="amountclaimed" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtDescription</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="description" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimstatus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimstatus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimdetails_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimdetails_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimtype_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimtype_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidTime</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="incidtime" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidDate</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="inciddate" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimref" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNotesView</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="notesview" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNewNotes</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="newnotes" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmITEMSPEC.tst" match="frmITEMSPEC">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmITEMSPEC[cgphome_speccont_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmITEMSPEC.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_itemspec_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="cgphome_speccont_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtItemSerial</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemserial" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemModel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemmodel" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemMake</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemmake" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdItemValue</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemvalue" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemDesc</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemdesc" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECITEM.tst" match="frmSPECITEM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmSPECITEM[cgphome_specpe_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECITEM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_specitem_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="cgphome_specpe_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtITEMSERIAL</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemserial" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMMODEL</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemmodel" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMMAKE</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemmake" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cfdITEMVALUE</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemvalue" /></xsl:element>
                 <xsl:element name="dataType">c</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtITEMDESC</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemdesc" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmXSANDNCD.tst" match="frmXSANDNCD">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmXSANDNCD[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmXSANDNCD.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_xsandncd_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboNCD</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ncd_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ncd_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboExcess</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="excess_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="excess_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmProposer.tst" match="frmProposer">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmProposer[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmProposer.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_proposer_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboRelationship</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="relationship_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="relationship_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMaritalStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="maritalstatus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="maritalstatus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtAge</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="age" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="dob" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="surname" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitials</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="initials" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="forename" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optCompany</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="company" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optNA</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="na" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optFemale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="female" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optMale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="male" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="title_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="title_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmOccupat.tst">
             <xsl:with-param name="pParentID" select="cgphome_proposer_id" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmOccupat.tst" match="frmOccupat">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmOccupat[cgphome_proposer_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmOccupat.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_occupat_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="cgphome_proposer_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkPartTime</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="parttime" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="type_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="type_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpsBusiness</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="empsbusiness_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="empsbusiness_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboDescription</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="description_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="description_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmGeneral.tst" match="frmGeneral">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmGeneral[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGeneral.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="cgphome_general_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="details" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRequested</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="requested" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkConvicted</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="convicted" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefused</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="refused" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
