<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tes="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:func="http://www.wisl.co.uk/functions">

<xsl:import href="../../../../core/functions.xslt" />

 <xsl:param name="pPolicyDetailsID" select="''" />
 <xsl:param name="pHistoryID" select="''" />

 <xsl:template name="screens" match="/">

    <ArrayOfStcStoredProcedure>

        <stcStoredProcedure/>

         <stcStoredProcedure>
             <strName>SP_BO_SAVE_POLICY_LINK</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@cipPolicyLinkID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="func:getguid()"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:client/@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="tes:client/@hID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdPolicyDetailsID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pPolicyDetailsID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

        <xsl:call-template name="frmPHDETAIL.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmSPECCONT.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmSPECPE.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmPARKDETA.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmCLAIMSUM.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmXSANDNCD.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmProposer.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmGeneral.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

    </ArrayOfStcStoredProcedure>
</xsl:template>

 <xsl:template name="frmPHDETAIL.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmPHDETAIL.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_PHDETAIL</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_PHDETAIL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@WINLOCKS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboWinLocks']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ALARMTYPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboAlarmType']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DOORLOCK_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboDoorLock']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PESPECCOUNT</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtPESpecCount']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CONTSPECCOUNT</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtContSpecCount']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TOTALSI</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdTotalSI']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CYCLECOVER_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboCycleCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PECOVER_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboPECover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ADCOVER</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkADCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CONTENTSI</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdContentSI']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PHSI</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdPHSI']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@GSOREPAIR</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkGSORepair']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SMOKE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkSmoke']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FINANCED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkFinanced']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FLATORFELT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboFlatorFelt']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@YOMANUFACTURE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboYOManufacture']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TYPE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtType']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MODEL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboModel']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MAKE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboMake']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INTPARTY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtIntParty']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmSPECCONT.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmSPECCONT.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_SPECCONT</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_SPECCONT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DUMMY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtdummy']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmITEMSPEC.tst">
             <xsl:with-param name="pParentID" select="@iD" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmSPECPE.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmSPECPE.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_SPECPE</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_SPECPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DUMMY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtDummy']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmSPECITEM.tst">
             <xsl:with-param name="pParentID" select="@iD" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmPARKDETA.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmPARKDETA.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_PARKDETA</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_PARKDETA_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@UNOCCUPIED_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboUnoccupied']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REGSITE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRegSite']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@BUSINESSUSE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboBusinessUse']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NUMONSITE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtNumOnSite']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmCLAIMSUM.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmCLAIMSUM.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_CLAIMSUM</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_CLAIMSUM_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmCLAIMDET.tst">
             <xsl:with-param name="pParentID" select="@iD" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmCLAIMDET.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmCLAIMDET.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_CLAIMDET</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_CLAIMDET_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CGPHOME_CLAIMSUM_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ATFAULT</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkAtFault']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INCONTHISPOLICY</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkIncOnThisPolicy']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NCDPREDJUDICED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkNCDPredjudiced']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LEGALREF</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtLegalRef']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SETTLEDAMOUNT</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdSettledAmount']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SETTLEDDATE</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskSettledDate']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskSettledDate']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskSettledDate']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@AMOUNTCLAIMED</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdAmountClaimed']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DESCRIPTION</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtDescription']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMSTATUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimStatus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMDETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimDetails']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMTYPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimType']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INCIDTIME</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidTime']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskIncidTime']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidTime']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INCIDDATE</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidDate']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskIncidDate']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidDate']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMREF</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtClaimRef']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NOTESVIEW</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtNotesView']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NEWNOTES</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtNewNotes']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmITEMSPEC.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmITEMSPEC.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_ITEMSPEC</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_ITEMSPEC_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CGPHOME_SPECCONT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMSERIAL</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtItemSerial']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMMODEL</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtItemModel']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMMAKE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtItemMake']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMVALUE</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdItemValue']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMDESC</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtItemDesc']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmSPECITEM.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmSPECITEM.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_SPECITEM</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_SPECITEM_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CGPHOME_SPECPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMSERIAL</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtITEMSERIAL']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMMODEL</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtITEMMODEL']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMMAKE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtITEMMAKE']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMVALUE</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='cfdITEMVALUE']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ITEMDESC</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtITEMDESC']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmXSANDNCD.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmXSANDNCD.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_XSANDNCD</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_XSANDNCD_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NCD_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboNCD']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EXCESS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboExcess']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmProposer.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmProposer.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_PROPOSER</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_PROPOSER_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@RELATIONSHIP_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboRelationship']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MARITALSTATUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboMaritalStatus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@AGE</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtAge']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DOB</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskDOB']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SURNAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtSurname']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INITIALS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtInitials']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FORENAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtForename']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@COMPANY</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optCompany']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NA</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optNA']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FEMALE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optFemale']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MALE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optMale']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TITLE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboTitle']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmOccupat.tst">
             <xsl:with-param name="pParentID" select="@iD" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmOccupat.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmOccupat.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_OCCUPAT</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_OCCUPAT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CGPHOME_PROPOSER_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PARTTIME</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkPartTime']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TYPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboType']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EMPSBUSINESS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboEmpsBusiness']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DESCRIPTION_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboDescription']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmGeneral.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmGeneral.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGPHOME_GENERAL</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGPHOME_GENERAL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DETAILS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtDetails']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REQUESTED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRequested']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CONVICTED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkConvicted']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REFUSED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRefused']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

</xsl:stylesheet>
