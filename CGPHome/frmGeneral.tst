<?xml version="1.0" encoding="utf-8"?>
<screen name="frmGeneral.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>General Questions</caption>
    <description>General Questions</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>325</height>
    <parentID />
    <text>General Questions</text>
    <width>382</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="txtDetails" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>DETAILS</columnName>
            <description>Details</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>92</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>8000</maxLength>
            <multiLine>True</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>154</top>
            <width>350</width>
            <showData />
            <controls />
        </control>
        <control name="WisLabel1" type="WisLabel">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <boundScreen />
            <boundTransform />
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <text>Details</text>
            <textAlign>1</textAlign>
            <top>137</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <maskedEditBox />
            <maskedEditBox2 />
            <showData />
            <controls />
        </control>
        <control name="chkRequested" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls>
                <enableControl>
                    <control>txtDetails</control>
                    <enable>True</enable>
                </enableControl>
            </enableControls>
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>REQUESTED</columnName>
            <description>Requested</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>34</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <checkAlign>16</checkAlign>
            <text>Ever Been Requested To Take Extra Precautions By An Insurer Regarding A Specific Area Of Risk</text>
            <textAlign>16</textAlign>
            <top>100</top>
            <width>350</width>
            <showData />
            <controls />
        </control>
        <control name="chkConvicted" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls>
                <enableControl>
                    <control>txtDetails</control>
                    <enable>True</enable>
                </enableControl>
            </enableControls>
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>CONVICTED</columnName>
            <description>Convicted</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>34</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <checkAlign>16</checkAlign>
            <text>Ever Been Convicted Of Arson, Theft, Robbery Or Any Other Offence Involving Dishonesty</text>
            <textAlign>16</textAlign>
            <top>60</top>
            <width>350</width>
            <showData />
            <controls />
        </control>
        <control name="chkRefused" type="WisCheckBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <enableControls>
                <enableControl>
                    <control>txtDetails</control>
                    <enable>True</enable>
                </enableControl>
            </enableControls>
            <appearance>0</appearance>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <boundScreen />
            <columnName>REFUSED</columnName>
            <description>Refused</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>34</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <checkAlign>16</checkAlign>
            <text>Ever Been Refused Or Had Cancelled, Park Home Insurance Or Had Terms Applied?</text>
            <textAlign>16</textAlign>
            <top>20</top>
            <width>352</width>
            <showData />
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>