<?xml version="1.0" encoding="utf-8"?>
<screen name="frmCLAIMSUM.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Claims Summary</caption>
    <description>Claims Summary</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>364</height>
    <parentID />
    <text>Claims Summary</text>
    <width>620</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="cmdRemoveClaim" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Remove Claim</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>314</left>
            <listView>lvwClaimSummary</listView>
            <top>274</top>
            <tabIndex>6</tabIndex>
            <type>5</type>
            <text>    &amp;Remove</text>
            <width>85</width>
        </control>
        <control name="cmdEditClaim" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Edit Claim</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>223</left>
            <listView>lvwClaimSummary</listView>
            <top>274</top>
            <tabIndex>5</tabIndex>
            <type>4</type>
            <text>  &amp;Edit</text>
            <width>85</width>
        </control>
        <control name="cmdAddClaim" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Add Claim</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>132</left>
            <listView>lvwClaimSummary</listView>
            <top>274</top>
            <tabIndex>4</tabIndex>
            <type>3</type>
            <text>  &amp;Add</text>
            <width>85</width>
        </control>
        <control name="lvwClaimSummary" type="WisListView">
            <columns>
                <column>
                    <text>Incident Date</text>
                    <Width>122</Width>
                    <bindControl>mskIncidDate</bindControl>
                </column>
                <column>
                    <text>Type</text>
                    <Width>133</Width>
                    <bindControl>cboClaimType</bindControl>
                </column>
                <column>
                    <text>Status</text>
                    <Width>114</Width>
                    <bindControl>cboClaimStatus</bindControl>
                </column>
                <column>
                    <text>Amount Claimed</text>
                    <Width>111</Width>
                    <bindControl>cfdAmountClaimed</bindControl>
                </column>
                <column>
                    <text>Settled Amount</text>
                    <Width>107</Width>
                    <bindControl>cfdSettledAmount</bindControl>
                </column>
            </columns>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen>CGPHome\frmCLAIMDET.tst</boundScreen>
            <description>Claims Summary Table</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>248</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text />
            <top>20</top>
            <width>590</width>
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>