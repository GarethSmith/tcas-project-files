<?xml version="1.0" encoding="utf-8"?>
<screen name="frmSPECPE.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Personal Effects Specified Items</caption>
    <description>Personal Effects Specified Items</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>299</height>
    <parentID />
    <text>Personal Effects Specified Items</text>
    <width>722</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module frmSPECPE

Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjDummy As TextBox = GetControl("txtDummy", owner.Controls)

                lobjDummy.Visible = False

            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
End Function

End Module
End Namespace
</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="txtDummy" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode />
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>DUMMY</columnName>
            <description>Dummy</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>587</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>213</top>
            <width>100</width>
            <showData />
            <controls />
        </control>
        <control name="cmdREMSPECPE" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module cmdREMSPECPE

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtDummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECPELIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function
    End Module
End Namespace</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Remove Specified Personal Item</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>448</left>
            <listView>lvwSPECPELIST</listView>
            <top>210</top>
            <tabIndex>3</tabIndex>
            <type>5</type>
            <text>    &amp;Remove</text>
            <width>75</width>
        </control>
        <control name="cmdEditSpecPE" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module cmdEditSpecPE

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtDummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECPELIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Edit Specified Personal Item</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>273</left>
            <listView>lvwSPECPELIST</listView>
            <top>210</top>
            <tabIndex>2</tabIndex>
            <type>4</type>
            <text>  &amp;Edit</text>
            <width>75</width>
        </control>
        <control name="cmdADDSPECPE" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cmdADDSPECPE

Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtDummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECPELIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
End Function

End Module
End Namespace
</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Add A Specified Item PE</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>110</left>
            <listView>lvwSPECPELIST</listView>
            <top>210</top>
            <tabIndex>1</tabIndex>
            <type>3</type>
            <text>  &amp;Add</text>
            <width>75</width>
        </control>
        <control name="lvwSPECPELIST" type="WisListView">
            <columns>
                <column>
                    <text>Description</text>
                    <Width>602</Width>
                    <bindControl>txtITEMDESC</bindControl>
                </column>
                <column>
                    <text>Value</text>
                    <Width>94</Width>
                    <bindControl>cfdITEMVALUE</bindControl>
                </column>
            </columns>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen>CGPHome\frmSPECITEM.tst</boundScreen>
            <description>List of Specified Personal Effects</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>194</height>
            <left>6</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text />
            <top>10</top>
            <width>698</width>
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>