<?xml version="1.0" encoding="utf-8"?>
<screen name="frmSPECCONT.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Contents Specified Items</caption>
    <description>Contents Specified Items</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>323</height>
    <parentID />
    <text>Contents Specified Items</text>
    <width>675</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module frmSPECCONT

Public Function FormLoad(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjDummy As TextBox = GetControl("txtdummy", owner.Controls)

                lobjDummy.Visible = False

            Catch ex As Exception
                Throw New Exception("FormLoad: " + ex.Message)
            End Try
End Function

End Module
End Namespace

</formLoadCode>
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="txtdummy" type="WisTextBox">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module txtDummy2

Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
End Function

End Module
End Namespace

</ClickCode>
            <TextChangedCode />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen />
            <characterCasing>0</characterCasing>
            <columnName>DUMMY</columnName>
            <description>Dummy</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>10</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData />
            <maxLength>100</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>231</top>
            <width>100</width>
            <showData />
            <controls />
        </control>
        <control name="cmdREMSPECCONT" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module cmdREMSPECCONT

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtdummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECCONTLIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Remove Specified Contents</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>409</left>
            <listView>lvwSPECCONTLIST</listView>
            <top>231</top>
            <tabIndex>3</tabIndex>
            <type>5</type>
            <text>    &amp;Remove</text>
            <width>75</width>
        </control>
        <control name="cmdEditSpecCont" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
    Public Module cmdEditSpecCont

        Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtdummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECCONTLIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
        End Function

    End Module
End Namespace</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Edit Specified Contents</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>264</left>
            <listView>lvwSPECCONTLIST</listView>
            <top>231</top>
            <tabIndex>2</tabIndex>
            <type>4</type>
            <text>  &amp;Edit</text>
            <width>75</width>
        </control>
        <control name="cmdAddSpecCont" type="WisButton">
            <LostFocusCode />
            <GotFocusCode />
            <ClickCode>Option Strict Off
Imports System
Imports System.Windows.Forms
Imports TES_UserMethods
Namespace TGSDynamic
Public Module cmdAddSpecCont

Public Function Click(ByRef owner As Form, ByRef sender As Object)
            Try

                Dim lobjLabel As TextBox = GetControl("txtdummy", owner.Controls)
                Dim lobjList As ListView = GetControl("lvwSPECCONTLIST", owner.Controls)


                If lobjList Is Nothing Then
                    lobjLabel.Text = "0"
                Else
                    lobjLabel.Text = lobjList.Items.Count
                End If

            Catch ex As Exception
                Throw New Exception("Click: " + ex.Message)
            End Try
End Function

End Module
End Namespace
</ClickCode>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <description>Add Specified Contents</description>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>134</left>
            <listView>lvwSPECCONTLIST</listView>
            <top>231</top>
            <tabIndex>1</tabIndex>
            <type>3</type>
            <text>  &amp;Add</text>
            <width>75</width>
        </control>
        <control name="lvwSPECCONTLIST" type="WisListView">
            <columns>
                <column>
                    <text>Description</text>
                    <Width>563</Width>
                    <bindControl>txtItemDesc</bindControl>
                </column>
                <column>
                    <text>Value</text>
                    <Width>89</Width>
                    <bindControl>cfdItemValue</bindControl>
                </column>
            </columns>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <boundScreen>CGPHome\frmITEMSPEC.tst</boundScreen>
            <description>Specified Contents List</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>208</height>
            <left>6</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text />
            <top>17</top>
            <width>655</width>
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
</screen>