<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmGENQUEST.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmHHDETAIL.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmHPCLAIM.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmSPECITEM.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmXSANDNCD.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPropDet.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmGENQUEST.tst" match="frmGENQUEST">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmGENQUEST[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGENQUEST.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_genquest_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="details" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkExtraPre</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="extrapre" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkCriminalOffence</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="criminaloffence" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefOrCancel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="reforcancel" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmHHDETAIL.tst" match="frmHHDETAIL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmHHDETAIL[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmHHDETAIL.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_hhdetail_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtYearBuilt</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="yearbuilt" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtContentValue</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="contentvalue" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADPets</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="adpets" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="adcover" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtBuildingValue</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="buildingvalue" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboBusUse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="bususe_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="bususe_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboUnoccupancy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="unoccupancy_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="unoccupancy_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkGSRepair</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="gsrepair" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkWaterSwitch</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="waterswitch" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkAlarm</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="alarm" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkHiring</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="hiring" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkSCEntrance</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="scentrance" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkWindowLocks</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="windowlocks" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkDoorsLocked</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="doorslocked" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNumofBedrooms</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="numofbedrooms" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboWallConst</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="wallconst_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="wallconst_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboRoofConst</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="roofconst_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="roofconst_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboFlatOrFelt</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="flatorfelt_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="flatorfelt_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboListStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="liststatus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="liststatus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTypeOfProperty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="typeofproperty_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="typeofproperty_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCountry</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="country" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCounty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="county" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCity</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="city" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLocality</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="locality" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtStreet</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="street" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtHouse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="house" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtPostcode</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="postcode" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmHPCLAIM.tst" match="frmHPCLAIM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmHPCLAIM[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmHPCLAIM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_hpclaim_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkIncOnPol</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="inconpol" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkNCDPrejudiced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ncdprejudiced" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLegalRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="legalref" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSettledAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="settledamount" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskSettledDate</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="settleddate" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimamount" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimDesc</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimdesc" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimstatus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimstatus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimDetail</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimdetail_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimdetail_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimtype_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="claimtype_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidentDate</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="incidentdate" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="claimref" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmSPECITEM.tst" match="frmSPECITEM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmSPECITEM[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmSPECITEM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_specitem_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtValue</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="value" /></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSerialNumber</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="serialnumber" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtModel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="model" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtMake</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="make" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtItemDesc</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="itemdesc" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmXSANDNCD.tst" match="frmXSANDNCD">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmXSANDNCD[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmXSANDNCD.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_xsandncd_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboNCD</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ncd_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ncd_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboExcess</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="excess_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="excess_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPropDet.tst" match="frmPropDet">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmPropDet[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPropDet.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_propdet_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboRelationship</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="relationship_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="relationship_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMarital</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="marital_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="marital_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtAGe</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="age" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="dob" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="surname" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitials</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="initials" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="forename" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="title_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="title_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optFemale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="female" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optMale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="male" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmOCCUPAT.tst">
             <xsl:with-param name="pParentID" select="ldhp_propdet_id" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmOCCUPAT.tst" match="frmOCCUPAT">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmOCCUPAT[ldhp_propdet_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmOCCUPAT.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ldhp_occupat_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ldhp_propdet_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkPartTime</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="parttime" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="empstatus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="empstatus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmployersBus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="employersbus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="employersbus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboOccupation</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="occupation_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="occupation_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
