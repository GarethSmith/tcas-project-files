<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tes="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:func="http://www.wisl.co.uk/functions">

<xsl:import href="../../../../core/functions.xslt" />

 <xsl:param name="pPolicyDetailsID" select="''" />
 <xsl:param name="pHistoryID" select="''" />

 <xsl:template name="screens" match="/">

    <ArrayOfStcStoredProcedure>

        <stcStoredProcedure/>

         <stcStoredProcedure>
             <strName>SP_BO_SAVE_POLICY_LINK</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@cipPolicyLinkID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="func:getguid()"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:client/@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="tes:client/@hID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdPolicyDetailsID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pPolicyDetailsID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

        <xsl:call-template name="frmGENQUEST.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmHHDETAIL.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmHPCLAIM.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmXSANDNCD.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmPropDet.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmEXTCOV.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

    </ArrayOfStcStoredProcedure>
</xsl:template>

 <xsl:template name="frmGENQUEST.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmGENQUEST.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_GENQUEST</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_GENQUEST_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DETAILS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtDetails']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EXTRAPRE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkExtraPre']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CRIMINALOFFENCE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkCriminalOffence']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REFORCANCEL</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRefOrCancel']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmHHDETAIL.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmHHDETAIL.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_HHDETAIL</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_HHDETAIL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@YEARBUILT</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtYearBuilt']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CONTENTVALUE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtContentValue']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ADPETS</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkADPets']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ADCOVER</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkADCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@BUILDINGVALUE</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtBuildingValue']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@BUSUSE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboBusUse']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@UNOCCUPANCY_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboUnoccupancy']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@GSREPAIR</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkGSRepair']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@WATERSWITCH</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkWaterSwitch']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SCENTRANCE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkSCEntrance']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@WINDOWLOCKS</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkWindowLocks']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DOORSLOCKED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkDoorsLocked']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NUMOFBEDROOMS</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtNumofBedrooms']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@WALLCONST_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboWallConst']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ROOFCONST_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboRoofConst']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FLATORFELT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboFlatOrFelt']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LISTSTATUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboListStatus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TYPEOFPROPERTY_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboTypeOfProperty']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@COUNTRY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCountry']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@COUNTY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCounty']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CITY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCity']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LOCALITY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtLocality']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@STREET</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtStreet']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HOUSE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtHouse']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POSTCODE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtPostcode']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SLEEPS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtSleeps']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CNTCOVER</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkCNTCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@BLDCOVER</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkBLDCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@BLDAD</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkBldAD']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HIRING_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboHiring']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@ALARM_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboAlarm']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SMOKEDETECT</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkSmokeDetect']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmHPCLAIM.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmHPCLAIM.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_HPCLAIM</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_HPCLAIM_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INCONPOL</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkIncOnPol']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NCDPREJUDICED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkNCDPrejudiced']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LEGALREF</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtLegalRef']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SETTLEDAMOUNT</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtSettledAmount']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SETTLEDDATE</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskSettledDate']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskSettledDate']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskSettledDate']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMAMOUNT</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtClaimAmount']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMDESC</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtClaimDesc']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMSTATUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimStatus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMDETAIL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimDetail']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMTYPE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboClaimType']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INCIDENTDATE</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidentDate']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskIncidentDate']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskIncidentDate']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CLAIMREF</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtClaimRef']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@APPSECTION_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboAppSection']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmXSANDNCD.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmXSANDNCD.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_XSANDNCD</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_XSANDNCD_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@NCD_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboNCD']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EXCESS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboExcess']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmPropDet.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmPropDet.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_PROPDET</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_PROPDET_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@RELATIONSHIP_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboRelationship']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MARITAL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboMarital']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@AGE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtAGe']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DOB</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskDOB']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SURNAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtSurname']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INITIALS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtInitials']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FORENAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtForename']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TITLE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboTitle']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FEMALE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optFemale']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MALE</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='optMale']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmOCCUPAT.tst">
             <xsl:with-param name="pParentID" select="@iD" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmOCCUPAT.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmOCCUPAT.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_OCCUPAT</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_OCCUPAT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LDHP_PROPDET_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PARTTIME</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkPartTime']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EMPSTATUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboEmpStatus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EMPLOYERSBUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboEmployersBus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@OCCUPATION_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboOccupation']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmEXTCOV.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmEXTCOV.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_LDHP_EXTCOV</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@LDHP_EXTCOV_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@PROPONCOVER</strName>
                     <objValue xsi:type="xsd:decimal"><xsl:value-of select="tes:controls/tes:control[@name='txtPropOnCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@RSADISCOUN_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboRSADiscoun']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

</xsl:stylesheet>
