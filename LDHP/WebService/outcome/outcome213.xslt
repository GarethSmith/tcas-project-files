<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:outcome="http://www.wisl.co.uk/schemas" xmlns:func="http://www.wisl.co.uk/core/functions">

	<xsl:import href="outcomeClient.xslt" />
	<xsl:import href="outcomePolicy.xslt" />
	<xsl:import href="outcomeAddon.xslt" />
	<xsl:import href="outcomeclaims.xslt" />
	<xsl:import href="../core/format.xslt" />

	<xsl:template name="outcome213" match="/outcome:client">

		<xsl:param name="QuoteStage"/>
		<xsl:param name="AgentName"/>
		<xsl:variable name="vPolicyDetailsID" select="outcome:policies/outcome:policy/@iD" />

		<System xmlns="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.wisl.co.uk/schemas http://www.wisl-test.co.uk/schemas/core/outcome.xsd">
			<Client>
				<xsl:call-template name="outcomeClient">
					<xsl:with-param name="AgentName" select="$AgentName" />
				</xsl:call-template>
				<Policy_Details>
					<xsl:call-template name="outcomePolicy">
						<xsl:with-param name="QuoteStage" select="$QuoteStage" />
					</xsl:call-template>
				    <xsl:call-template name="outcomeAddon" />
				    <xsl:call-template name="outcomeClaims" />
                     <xsl:call-template name="frmGENQUEST.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmHHDETAIL.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmHPCLAIM.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmXSANDNCD.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmPropDet.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmEXTCOV.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
				</Policy_Details>
			</Client>
		</System>

	</xsl:template>

     <xsl:template name="frmGENQUEST.tst" match="frmGENQUEST.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmGENQUEST.tst' and @parentID=$pParentID]">
             <UO_LDHP_GENQUEST xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_LDHP_GENQUEST_Details><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDetails']/outcome:value)" /></UQ_LDHP_GENQUEST_Details>
                 <UQ_LDHP_GENQUEST_ExtraPre><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkExtraPre']/outcome:value='1' or outcome:controls/outcome:control[@name='chkExtraPre']/outcome:value='true')" /></UQ_LDHP_GENQUEST_ExtraPre>
                 <UQ_LDHP_GENQUEST_CriminalOffence><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkCriminalOffence']/outcome:value='1' or outcome:controls/outcome:control[@name='chkCriminalOffence']/outcome:value='true')" /></UQ_LDHP_GENQUEST_CriminalOffence>
                 <UQ_LDHP_GENQUEST_RefOrCancel><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRefOrCancel']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRefOrCancel']/outcome:value='true')" /></UQ_LDHP_GENQUEST_RefOrCancel>
             </UO_LDHP_GENQUEST>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmHHDETAIL.tst" match="frmHHDETAIL.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmHHDETAIL.tst' and @parentID=$pParentID]">
             <UO_LDHP_HHDETAIL xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_LDHP_HHDETAIL_YearBuilt><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtYearBuilt']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtYearBuilt']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_HHDETAIL_YearBuilt>
                 <UQ_LDHP_HHDETAIL_ContentValue><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtContentValue']/outcome:value)" /></UQ_LDHP_HHDETAIL_ContentValue>
                 <UQ_LDHP_HHDETAIL_ADPets><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkADPets']/outcome:value='1' or outcome:controls/outcome:control[@name='chkADPets']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_ADPets>
                 <UQ_LDHP_HHDETAIL_ADCover><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkADCover']/outcome:value='1' or outcome:controls/outcome:control[@name='chkADCover']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_ADCover>
                 <UQ_LDHP_HHDETAIL_BuildingValue><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtBuildingValue']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtBuildingValue']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_HHDETAIL_BuildingValue>
<UQ_LDHP_HHDETAIL_BusUse><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboBusUse']/outcome:value)" /></UQ_LDHP_HHDETAIL_BusUse>
<UQ_LDHP_HHDETAIL_Unoccupancy><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboUnoccupancy']/outcome:value)" /></UQ_LDHP_HHDETAIL_Unoccupancy>
                 <UQ_LDHP_HHDETAIL_GSRepair><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkGSRepair']/outcome:value='1' or outcome:controls/outcome:control[@name='chkGSRepair']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_GSRepair>
                 <UQ_LDHP_HHDETAIL_WaterSwitch><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkWaterSwitch']/outcome:value='1' or outcome:controls/outcome:control[@name='chkWaterSwitch']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_WaterSwitch>
                 <UQ_LDHP_HHDETAIL_SCEntrance><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkSCEntrance']/outcome:value='1' or outcome:controls/outcome:control[@name='chkSCEntrance']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_SCEntrance>
                 <UQ_LDHP_HHDETAIL_WindowLocks><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkWindowLocks']/outcome:value='1' or outcome:controls/outcome:control[@name='chkWindowLocks']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_WindowLocks>
                 <UQ_LDHP_HHDETAIL_DoorsLocked><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkDoorsLocked']/outcome:value='1' or outcome:controls/outcome:control[@name='chkDoorsLocked']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_DoorsLocked>
                 <UQ_LDHP_HHDETAIL_NumofBedrooms><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtNumofBedrooms']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtNumofBedrooms']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_HHDETAIL_NumofBedrooms>
<UQ_LDHP_HHDETAIL_WallConst><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboWallConst']/outcome:value)" /></UQ_LDHP_HHDETAIL_WallConst>
<UQ_LDHP_HHDETAIL_RoofConst><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboRoofConst']/outcome:value)" /></UQ_LDHP_HHDETAIL_RoofConst>
<UQ_LDHP_HHDETAIL_FlatOrFelt><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboFlatOrFelt']/outcome:value)" /></UQ_LDHP_HHDETAIL_FlatOrFelt>
<UQ_LDHP_HHDETAIL_ListStatus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboListStatus']/outcome:value)" /></UQ_LDHP_HHDETAIL_ListStatus>
<UQ_LDHP_HHDETAIL_TypeOfProperty><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTypeOfProperty']/outcome:value)" /></UQ_LDHP_HHDETAIL_TypeOfProperty>
                 <UQ_LDHP_HHDETAIL_Country><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCountry']/outcome:value)" /></UQ_LDHP_HHDETAIL_Country>
                 <UQ_LDHP_HHDETAIL_County><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCounty']/outcome:value)" /></UQ_LDHP_HHDETAIL_County>
                 <UQ_LDHP_HHDETAIL_City><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCity']/outcome:value)" /></UQ_LDHP_HHDETAIL_City>
                 <UQ_LDHP_HHDETAIL_Locality><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtLocality']/outcome:value)" /></UQ_LDHP_HHDETAIL_Locality>
                 <UQ_LDHP_HHDETAIL_Street><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtStreet']/outcome:value)" /></UQ_LDHP_HHDETAIL_Street>
                 <UQ_LDHP_HHDETAIL_House><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtHouse']/outcome:value)" /></UQ_LDHP_HHDETAIL_House>
                 <UQ_LDHP_HHDETAIL_Postcode_Town><xsl:value-of select="func:PostCodeTown(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_LDHP_HHDETAIL_Postcode_Town>
                 <UQ_LDHP_HHDETAIL_Postcode_Sector><xsl:value-of select="func:PostCodeSector(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_LDHP_HHDETAIL_Postcode_Sector>
                 <UQ_LDHP_HHDETAIL_Postcode_TownAndSector><xsl:value-of select="func:PostCodeTown(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /><xsl:value-of select="func:PostCodeSector(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_LDHP_HHDETAIL_Postcode_TownAndSector>
                 <UQ_LDHP_HHDETAIL_Postcode_TownOnly><xsl:value-of select="func:PostCodeTownOnly(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_LDHP_HHDETAIL_Postcode_TownOnly>
                 <UQ_LDHP_HHDETAIL_Postcode><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_LDHP_HHDETAIL_Postcode>
                 <UQ_LDHP_HHDETAIL_Sleeps><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSleeps']/outcome:value)" /></UQ_LDHP_HHDETAIL_Sleeps>
                 <UQ_LDHP_HHDETAIL_CNTCover><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkCNTCover']/outcome:value='1' or outcome:controls/outcome:control[@name='chkCNTCover']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_CNTCover>
                 <UQ_LDHP_HHDETAIL_BLDCover><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkBLDCover']/outcome:value='1' or outcome:controls/outcome:control[@name='chkBLDCover']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_BLDCover>
                 <UQ_LDHP_HHDETAIL_BldAD><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkBldAD']/outcome:value='1' or outcome:controls/outcome:control[@name='chkBldAD']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_BldAD>
<UQ_LDHP_HHDETAIL_Hiring><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboHiring']/outcome:value)" /></UQ_LDHP_HHDETAIL_Hiring>
<UQ_LDHP_HHDETAIL_Alarm><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboAlarm']/outcome:value)" /></UQ_LDHP_HHDETAIL_Alarm>
                 <UQ_LDHP_HHDETAIL_SmokeDetect><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkSmokeDetect']/outcome:value='1' or outcome:controls/outcome:control[@name='chkSmokeDetect']/outcome:value='true')" /></UQ_LDHP_HHDETAIL_SmokeDetect>
             </UO_LDHP_HHDETAIL>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmHPCLAIM.tst" match="frmHPCLAIM.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmHPCLAIM.tst' and @parentID=$pParentID]">
             <UO_LDHP_HPCLAIM xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_LDHP_HPCLAIM_IncOnPol><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkIncOnPol']/outcome:value='1' or outcome:controls/outcome:control[@name='chkIncOnPol']/outcome:value='true')" /></UQ_LDHP_HPCLAIM_IncOnPol>
                 <UQ_LDHP_HPCLAIM_NCDPrejudiced><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkNCDPrejudiced']/outcome:value='1' or outcome:controls/outcome:control[@name='chkNCDPrejudiced']/outcome:value='true')" /></UQ_LDHP_HPCLAIM_NCDPrejudiced>
                 <UQ_LDHP_HPCLAIM_LegalRef><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtLegalRef']/outcome:value)" /></UQ_LDHP_HPCLAIM_LegalRef>
                 <UQ_LDHP_HPCLAIM_SettledAmount><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtSettledAmount']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSettledAmount']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_HPCLAIM_SettledAmount>
                 <UQ_LDHP_HPCLAIM_SettledDate><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskSettledDate']/outcome:value,2)" /></UQ_LDHP_HPCLAIM_SettledDate>
                 <UQ_LDHP_HPCLAIM_ClaimAmount><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtClaimAmount']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtClaimAmount']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_HPCLAIM_ClaimAmount>
                 <UQ_LDHP_HPCLAIM_ClaimDesc><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtClaimDesc']/outcome:value)" /></UQ_LDHP_HPCLAIM_ClaimDesc>
<UQ_LDHP_HPCLAIM_ClaimStatus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimStatus']/outcome:value)" /></UQ_LDHP_HPCLAIM_ClaimStatus>
<UQ_LDHP_HPCLAIM_ClaimDetail><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimDetail']/outcome:value)" /></UQ_LDHP_HPCLAIM_ClaimDetail>
<UQ_LDHP_HPCLAIM_ClaimType><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboClaimType']/outcome:value)" /></UQ_LDHP_HPCLAIM_ClaimType>
                 <UQ_LDHP_HPCLAIM_IncidentDate><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskIncidentDate']/outcome:value,2)" /></UQ_LDHP_HPCLAIM_IncidentDate>
                 <UQ_LDHP_HPCLAIM_ClaimRef><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtClaimRef']/outcome:value)" /></UQ_LDHP_HPCLAIM_ClaimRef>
<UQ_LDHP_HPCLAIM_AppSection><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboAppSection']/outcome:value)" /></UQ_LDHP_HPCLAIM_AppSection>
             </UO_LDHP_HPCLAIM>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmXSANDNCD.tst" match="frmXSANDNCD.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmXSANDNCD.tst' and @parentID=$pParentID]">
             <UO_LDHP_XSANDNCD xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_LDHP_XSANDNCD_NCD><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboNCD']/outcome:value)" /></UQ_LDHP_XSANDNCD_NCD>
<UQ_LDHP_XSANDNCD_Excess><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboExcess']/outcome:value)" /></UQ_LDHP_XSANDNCD_Excess>
             </UO_LDHP_XSANDNCD>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmPropDet.tst" match="frmPropDet.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmPropDet.tst' and @parentID=$pParentID]">
             <UO_LDHP_PropDet xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_LDHP_PropDet_Relationship><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboRelationship']/outcome:value)" /></UQ_LDHP_PropDet_Relationship>
<UQ_LDHP_PropDet_Marital><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboMarital']/outcome:value)" /></UQ_LDHP_PropDet_Marital>
                 <UQ_LDHP_PropDet_AGe><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtAGe']/outcome:value)" /></UQ_LDHP_PropDet_AGe>
                 <UQ_LDHP_PropDet_DOB><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskDOB']/outcome:value,2)" /></UQ_LDHP_PropDet_DOB>
                 <UQ_LDHP_PropDet_Surname><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSurname']/outcome:value)" /></UQ_LDHP_PropDet_Surname>
                 <UQ_LDHP_PropDet_Initials><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtInitials']/outcome:value)" /></UQ_LDHP_PropDet_Initials>
                 <UQ_LDHP_PropDet_Forename><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtForename']/outcome:value)" /></UQ_LDHP_PropDet_Forename>
<UQ_LDHP_PropDet_Title><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTitle']/outcome:value)" /></UQ_LDHP_PropDet_Title>
                 <UQ_LDHP_PropDet_Female><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optFemale']/outcome:value='1' or outcome:controls/outcome:control[@name='optFemale']/outcome:value='true')" /></UQ_LDHP_PropDet_Female>
                 <UQ_LDHP_PropDet_Male><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='optMale']/outcome:value='1' or outcome:controls/outcome:control[@name='optMale']/outcome:value='true')" /></UQ_LDHP_PropDet_Male>

                 <xsl:call-template name="frmOCCUPAT.tst">
                     <xsl:with-param name="pParentID" select="@iD" />
                 </xsl:call-template>
             </UO_LDHP_PropDet>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmOCCUPAT.tst" match="frmOCCUPAT.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmOCCUPAT.tst' and @parentID=$pParentID]">
             <UO_LDHP_OCCUPAT xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_LDHP_OCCUPAT_PartTime><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkPartTime']/outcome:value='1' or outcome:controls/outcome:control[@name='chkPartTime']/outcome:value='true')" /></UQ_LDHP_OCCUPAT_PartTime>
<UQ_LDHP_OCCUPAT_EmpStatus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpStatus']/outcome:value)" /></UQ_LDHP_OCCUPAT_EmpStatus>
<UQ_LDHP_OCCUPAT_EmployersBus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmployersBus']/outcome:value)" /></UQ_LDHP_OCCUPAT_EmployersBus>
<UQ_LDHP_OCCUPAT_Occupation><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboOccupation']/outcome:value)" /></UQ_LDHP_OCCUPAT_Occupation>
             </UO_LDHP_OCCUPAT>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmEXTCOV.tst" match="frmEXTCOV.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmEXTCOV.tst' and @parentID=$pParentID]">
             <UO_LDHP_EXTCOV xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_LDHP_EXTCOV_PropOnCover><xsl:choose><xsl:when test="outcome:controls/outcome:control[@name='txtPropOnCover']/outcome:value=''">0</xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtPropOnCover']/outcome:value)" /></xsl:otherwise></xsl:choose></UQ_LDHP_EXTCOV_PropOnCover>
<UQ_LDHP_EXTCOV_RSADiscoun><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboRSADiscoun']/outcome:value)" /></UQ_LDHP_EXTCOV_RSADiscoun>
             </UO_LDHP_EXTCOV>
         </xsl:for-each>
     </xsl:template>

</xsl:stylesheet>
