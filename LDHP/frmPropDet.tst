<?xml version="1.0" encoding="utf-8"?>
<screen name="frmPropDet.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Proposer Details</caption>
    <description>Proposer Details</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>392</height>
    <parentID />
    <text>Proposer Details</text>
    <width>414</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="grpOccupationGroup" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>115</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>19</tabIndex>
            <text>Occupation</text>
            <top>205</top>
            <width>382</width>
            <controls>
                <control name="cmdOccuRemove" type="WisButton">
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>OccuRemove</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>304</left>
                    <listView>lvwOccList</listView>
                    <top>86</top>
                    <tabIndex>3</tabIndex>
                    <type>5</type>
                    <text>    &amp;Remove</text>
                    <width>75</width>
                </control>
                <control name="cmdOccuEdit" type="WisButton">
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>OccuEdit</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>223</left>
                    <listView>lvwOccList</listView>
                    <top>86</top>
                    <tabIndex>2</tabIndex>
                    <type>4</type>
                    <text>  &amp;Edit</text>
                    <width>75</width>
                </control>
                <control name="cmdoCCUaDD" type="WisButton">
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>OccuAdd</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>142</left>
                    <listView>lvwOccList</listView>
                    <top>86</top>
                    <tabIndex>1</tabIndex>
                    <type>3</type>
                    <text>  &amp;Add</text>
                    <width>75</width>
                </control>
                <control name="lvwOccList" type="WisListView">
                    <columns>
                        <column>
                            <text>Occupation</text>
                            <Width>150</Width>
                            <bindControl>cboOccupation</bindControl>
                        </column>
                        <column>
                            <text>Employers Business</text>
                            <Width>150</Width>
                            <bindControl>cboEmployersBus</bindControl>
                        </column>
                        <column>
                            <text>Status</text>
                            <Width>70</Width>
                            <bindControl>cboEmpStatus</bindControl>
                        </column>
                    </columns>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <boundScreen>LDHP\frmOCCUPAT.tst</boundScreen>
                    <description>Occupation List</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>60</height>
                    <left>6</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text />
                    <top>20</top>
                    <width>370</width>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="cboRelationship" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>RELATIONSHIP_ID</columnName>
            <description>Relationship</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_RELATIONSHIP</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>18</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>176</top>
            <width>267</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblRelationship" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>17</tabIndex>
            <text>Relationship</text>
            <textAlign>1</textAlign>
            <top>179</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboMarital" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>MARITAL_STATUS_ID</columnName>
            <description>Marital</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_MARITAL_STATUS</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>16</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>149</top>
            <width>267</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblMarital" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>15</tabIndex>
            <text>Marital Status</text>
            <textAlign>1</textAlign>
            <top>152</top>
            <width>95</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtAGe" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>AGE</columnName>
            <description>Age</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>243</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>10</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>11</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>122</top>
            <width>54</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <startYear>frmPropDet.DOB</startYear>
            <endYear>System.Date</endYear>
            <controls />
        </control>
        <control name="lblAge" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>19</height>
            <left>207</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>Age</text>
            <textAlign>1</textAlign>
            <top>124</top>
            <width>40</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="mskDOB" type="WisMaskedBox">
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <startYear>0</startYear>
            <endYear>0</endYear>
            <reverseList>False</reverseList>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>DOB</columnName>
            <description>DOB</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <mask>##/##/####</mask>
            <maxLength>32767</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <text>__/__/____</text>
            <textAlign>0</textAlign>
            <top>122</top>
            <width>75</width>
            <boundObject>0</boundObject>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblDateOfBirth" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text>DOB</text>
            <textAlign>1</textAlign>
            <top>124</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="lblSurname" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>7</tabIndex>
            <text>Surname</text>
            <textAlign>1</textAlign>
            <top>97</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtSurname" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>SURNAME</columnName>
            <description>Surname</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>6</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>95</top>
            <width>171</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="txtInitials" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>INITIALS</columnName>
            <description>Initials</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>5</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>68</top>
            <width>171</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblInitials" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text>Initials</text>
            <textAlign>1</textAlign>
            <top>70</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtForename" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>FORENAME</columnName>
            <description>Forename</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>41</top>
            <width>171</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblForename" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Forename</text>
            <textAlign>1</textAlign>
            <top>44</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="lblTitle" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description />
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <text>Title</text>
            <textAlign>1</textAlign>
            <top>17</top>
            <width>48</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboTitle" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>TITLE_ID</columnName>
            <description>Title</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>126</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_TITLE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>14</top>
            <width>121</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="grpSexGroup" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>79</height>
            <left>303</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <text>Sex</text>
            <top>10</top>
            <width>90</width>
            <controls>
                <control name="optFemale" type="WisRadioButton">
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>FEMALE</columnName>
                    <description>Female</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>18</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>13</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>Female</text>
                    <textAlign>16</textAlign>
                    <top>49</top>
                    <width>70</width>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="optMale" type="WisRadioButton">
                    <enableControls />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>MALE</columnName>
                    <description>Male</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>18</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>12</tabIndex>
                    <checkAlign>16</checkAlign>
                    <text>Male</text>
                    <textAlign>16</textAlign>
                    <top>20</top>
                    <width>67</width>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
            </controls>
        </control>
    </controls>
    <generalSummary>
        <columns>
            <column>
                <text>Title</text>
                <bindControl>cboTitle</bindControl>
            </column>
            <column>
                <text>Forename</text>
                <bindControl>txtForename</bindControl>
            </column>
            <column>
                <text>Surname</text>
                <bindControl>txtSurname</bindControl>
            </column>
        </columns>
    </generalSummary>
    <formValidation />
    <previousRiskAtAQ />
    <hideClaim />
</screen>