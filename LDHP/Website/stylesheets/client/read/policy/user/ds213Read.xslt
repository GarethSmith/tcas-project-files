<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas" xmlns:ds="http://tempuri.org/dsCustomer.xsd">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmGENQUEST.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmHHDETAIL.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmHPCLAIM.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmXSANDNCD.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPropDet.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmEXTCOV.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmGENQUEST.tst" match="ds:USER_LDHP_GENQUEST">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_GENQUEST[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGENQUEST.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_GENQUEST_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DETAILS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkExtraPre</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:EXTRAPRE='1' or ds:EXTRAPRE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkCriminalOffence</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:CRIMINALOFFENCE='1' or ds:CRIMINALOFFENCE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefOrCancel</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REFORCANCEL='1' or ds:REFORCANCEL='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmHHDETAIL.tst" match="ds:USER_LDHP_HHDETAIL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_HHDETAIL[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmHHDETAIL.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_HHDETAIL_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtYearBuilt</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:YEARBUILT)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:YEARBUILT" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtContentValue</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CONTENTVALUE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADPets</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:ADPETS='1' or ds:ADPETS='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkADCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:ADCOVER='1' or ds:ADCOVER='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtBuildingValue</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:BUILDINGVALUE)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:BUILDINGVALUE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboBusUse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:BUSUSE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:BUSUSE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboUnoccupancy</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:UNOCCUPANCY_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:UNOCCUPANCY_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkGSRepair</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:GSREPAIR='1' or ds:GSREPAIR='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkWaterSwitch</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:WATERSWITCH='1' or ds:WATERSWITCH='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkSCEntrance</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:SCENTRANCE='1' or ds:SCENTRANCE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkWindowLocks</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:WINDOWLOCKS='1' or ds:WINDOWLOCKS='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkDoorsLocked</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:DOORSLOCKED='1' or ds:DOORSLOCKED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtNumofBedrooms</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:NUMOFBEDROOMS)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:NUMOFBEDROOMS" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboWallConst</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:WALLCONST_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:WALLCONST_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboRoofConst</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ROOFCONST_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:ROOFCONST_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboFlatOrFelt</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:FLATORFELT_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:FLATORFELT_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboListStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:LISTSTATUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:LISTSTATUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTypeOfProperty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TYPEOFPROPERTY_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:TYPEOFPROPERTY_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCountry</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:COUNTRY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCounty</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:COUNTY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtCity</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CITY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLocality</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:LOCALITY" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtStreet</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:STREET" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtHouse</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:HOUSE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtPostcode</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:POSTCODE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSleeps</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:SLEEPS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkCNTCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:CNTCOVER='1' or ds:CNTCOVER='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkBLDCover</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:BLDCOVER='1' or ds:BLDCOVER='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkBldAD</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:BLDAD='1' or ds:BLDAD='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboHiring</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:HIRING_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:HIRING_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboAlarm</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:ALARM_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:ALARM_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkSmokeDetect</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:SMOKEDETECT='1' or ds:SMOKEDETECT='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmHPCLAIM.tst" match="ds:USER_LDHP_HPCLAIM">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_HPCLAIM[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmHPCLAIM.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_HPCLAIM_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkIncOnPol</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:INCONPOL='1' or ds:INCONPOL='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkNCDPrejudiced</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:NCDPREJUDICED='1' or ds:NCDPREJUDICED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtLegalRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:LEGALREF" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSettledAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:SETTLEDAMOUNT)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:SETTLEDAMOUNT" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskSettledDate</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:SETTLEDDATE)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:SETTLEDDATE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimAmount</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:CLAIMAMOUNT)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:CLAIMAMOUNT" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimDesc</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMDESC" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMSTATUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMSTATUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimDetail</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMDETAIL_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMDETAIL_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboClaimType</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMTYPE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:CLAIMTYPE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskIncidentDate</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:INCIDENTDATE)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:INCIDENTDATE" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtClaimRef</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:CLAIMREF" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboAppSection</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:APPSECTION_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:APPSECTION_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmXSANDNCD.tst" match="ds:USER_LDHP_XSANDNCD">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_XSANDNCD[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmXSANDNCD.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_XSANDNCD_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboNCD</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:NCD_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:NCD_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboExcess</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EXCESS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EXCESS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPropDet.tst" match="ds:USER_LDHP_PROPDET">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_PROPDET[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPropDet.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_PROPDET_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">cboRelationship</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:RELATIONSHIP_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:RELATIONSHIP_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboMarital</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:MARITAL_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:MARITAL_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtAGe</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:AGE" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:DOB)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:DOB" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:SURNAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitials</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:INITIALS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:FORENAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TITLE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:TITLE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optFemale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:FEMALE='1' or ds:FEMALE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">optMale</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:MALE='1' or ds:MALE='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>

         <xsl:call-template name="frmOCCUPAT.tst">
             <xsl:with-param name="pParentID" select="ds:LDHP_PROPDET_ID" />
         </xsl:call-template>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmOCCUPAT.tst" match="ds:USER_LDHP_OCCUPAT">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_OCCUPAT[ds:LDHP_PROPDET_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmOCCUPAT.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_OCCUPAT_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:LDHP_PROPDET_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkPartTime</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:PARTTIME='1' or ds:PARTTIME='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpStatus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EMPSTATUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EMPSTATUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmployersBus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EMPLOYERSBUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EMPLOYERSBUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboOccupation</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:OCCUPATION_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:OCCUPATION_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmEXTCOV.tst" match="ds:USER_LDHP_EXTCOV">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_LDHP_EXTCOV[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmEXTCOV.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:LDHP_EXTCOV_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtPropOnCover</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:PROPONCOVER)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="ds:PROPONCOVER" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">n</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboRSADiscoun</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:RSADISCOUN_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:RSADISCOUN_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
