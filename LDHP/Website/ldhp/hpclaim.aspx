<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Claim Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Claim Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_HPCLAIM__CLAIMREF__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMREF__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMREF__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMREF__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMREF__0">ClaimRef</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HPCLAIM__CLAIMREF__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMREF__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMREF__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimRef" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__INCIDENTDATE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__INCIDENTDATE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__INCIDENTDATE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__INCIDENTDATE__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__INCIDENTDATE__0">IncidentDate</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_LDHP_HPCLAIM__INCIDENTDATE__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__INCIDENTDATE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__INCIDENTDATE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="IncidentDate" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMTYPE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMTYPE_ID__0">ClaimType</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" runat="server" ListViewName="hh_claim_type_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMTYPE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimType" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0">ClaimDetail</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" runat="server" ListViewName="hh_claim_detail_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMDETAIL_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimDetail" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0">ClaimStatus</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" runat="server" ListViewName="hh_claim_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMSTATUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimStatus" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__CLAIMDESC__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMDESC__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMDESC__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMDESC__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMDESC__0">ClaimDesc</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HPCLAIM__CLAIMDESC__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="8000" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMDESC__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMDESC__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimDesc" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__CLAIMAMOUNT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__CLAIMAMOUNT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__CLAIMAMOUNT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__CLAIMAMOUNT__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__CLAIMAMOUNT__0">ClaimAmount</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HPCLAIM__CLAIMAMOUNT__0" runat="server" MaxLength="15"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__CLAIMAMOUNT__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__CLAIMAMOUNT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ClaimAmount" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__SETTLEDDATE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__SETTLEDDATE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__SETTLEDDATE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__SETTLEDDATE__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__SETTLEDDATE__0">SettledDate</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_LDHP_HPCLAIM__SETTLEDDATE__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__SETTLEDDATE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__SETTLEDDATE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="SettledDate" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__SETTLEDAMOUNT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__SETTLEDAMOUNT__0">SettledAmount</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" runat="server" MaxLength="200"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__SETTLEDAMOUNT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="SettledAmount" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__LEGALREF__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__LEGALREF__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__LEGALREF__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__LEGALREF__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__LEGALREF__0">LegalRef</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HPCLAIM__LEGALREF__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__LEGALREF__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__LEGALREF__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="LegalRef" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__NCDPREJUDICED__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__NCDPREJUDICED__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__NCDPREJUDICED__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__NCDPREJUDICED__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__NCDPREJUDICED__0">NCDPrejudiced</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HPCLAIM__NCDPREJUDICED__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__INCONPOL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__INCONPOL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__INCONPOL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__INCONPOL__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__INCONPOL__0">IncOnPol</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HPCLAIM__INCONPOL__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HPCLAIM__APPSECTION_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HPCLAIM__APPSECTION_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HPCLAIM__APPSECTION_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HPCLAIM__APPSECTION_ID__0" runat="server" AssociatedControlID="USER_LDHP_HPCLAIM__APPSECTION_ID__0">cboAppSection</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HPCLAIM__APPSECTION_ID__0" runat="server" ListViewName="hh_claim_section_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HPCLAIM__APPSECTION_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HPCLAIM__APPSECTION_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboAppSection" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
