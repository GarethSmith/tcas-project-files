<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Excess and NCD Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Excess and NCD Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_XSANDNCD__EXCESS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_XSANDNCD__EXCESS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_XSANDNCD__EXCESS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_XSANDNCD__EXCESS_ID__0" runat="server" AssociatedControlID="USER_LDHP_XSANDNCD__EXCESS_ID__0">Excess</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_XSANDNCD__EXCESS_ID__0" runat="server" ListViewName="hh_excess_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_XSANDNCD__EXCESS_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_XSANDNCD__EXCESS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Excess" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_XSANDNCD__NCD_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_XSANDNCD__NCD_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_XSANDNCD__NCD_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_XSANDNCD__NCD_ID__0" runat="server" AssociatedControlID="USER_LDHP_XSANDNCD__NCD_ID__0">NCD</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_XSANDNCD__NCD_ID__0" runat="server" ListViewName="hh_ncd_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_XSANDNCD__NCD_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_XSANDNCD__NCD_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="NCD" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
