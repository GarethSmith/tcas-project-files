<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Proposer Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Proposer Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_PROPDET__TITLE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__TITLE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__TITLE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__TITLE_ID__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__TITLE_ID__0">Title</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_PROPDET__TITLE_ID__0" runat="server" ListViewName="title_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__TITLE_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__TITLE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Title" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__FORENAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__FORENAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__FORENAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__FORENAME__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__FORENAME__0">Forename</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_PROPDET__FORENAME__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__FORENAME__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__FORENAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Forename" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__INITIALS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__INITIALS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__INITIALS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__INITIALS__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__INITIALS__0">Initials</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_PROPDET__INITIALS__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__INITIALS__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__INITIALS__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Initials" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__SURNAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__SURNAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__SURNAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__SURNAME__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__SURNAME__0">Surname</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_PROPDET__SURNAME__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__SURNAME__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__SURNAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Surname" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__DOB__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__DOB__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__DOB__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__DOB__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__DOB__0">DOB</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_LDHP_PROPDET__DOB__0" runat="server" YearDiffStartYear="USER_LDHP_PROPDET__DOB__0_cboYear" YearDiffEndYear="System.Date" YearDiffTarget="USER_LDHP_PROPDET__AGE__0_ctl" ></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__DOB__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__DOB__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="DOB" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__AGE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__AGE__0" Style="display: none;" >
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__AGE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__AGE__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__AGE__0">Age</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_PROPDET__AGE__0" runat="server" MaxLength="10" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__AGE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__AGE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Age" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__SEXGROUP__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__SEXGROUP__0"">
                 <span class="required">&nbsp;</span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__SEXGROUP__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__SEXGROUP__0">Male</tes_webcontrols:TGSLLabel>
                 <span class="control"><span class="radiocontrol" ID="USER_LDHP_PROPDET__SEXGROUP__0" runat="server"><span><asp:RadioButton ID="USER_LDHP_PROPDET__MALE__0" runat="server" Text="Male" GroupName="grpSexGroup" /></span><span><asp:RadioButton ID="USER_LDHP_PROPDET__FEMALE__0" runat="server" Text="Female" GroupName="grpSexGroup" /></span></span></span></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__MARITAL_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__MARITAL_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__MARITAL_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__MARITAL_ID__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__MARITAL_ID__0">Marital</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_PROPDET__MARITAL_ID__0" runat="server" ListViewName="marital_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__MARITAL_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__MARITAL_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Marital" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_PROPDET__RELATIONSHIP_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_PROPDET__RELATIONSHIP_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_PROPDET__RELATIONSHIP_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_PROPDET__RELATIONSHIP_ID__0" runat="server" AssociatedControlID="USER_LDHP_PROPDET__RELATIONSHIP_ID__0">Relationship</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_PROPDET__RELATIONSHIP_ID__0" runat="server" ListViewName="relationship_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_PROPDET__RELATIONSHIP_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_PROPDET__RELATIONSHIP_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Relationship" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
