<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Holiday Home Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Holiday Home Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_HHDETAIL__POSTCODE__0" runat="server" style="display:none;"></li>
             </ol>
             <uc1:PostcodeSearch ID="cmdPostSearch" runat="server" PostcodeLabel="Postcode" PostcodeID="USER_LDHP_HHDETAIL__POSTCODE__0" PostcodeRequired="true" HouseLabel="House" HouseID="USER_LDHP_HHDETAIL__HOUSE__0" HouseRequired="true" StreetLabel="Street" StreetID="USER_LDHP_HHDETAIL__STREET__0" StreetRequired="true" LocalityLabel="Locality" LocalityID="USER_LDHP_HHDETAIL__LOCALITY__0" LocalityRequired="true" CityLabel="City" CityID="USER_LDHP_HHDETAIL__CITY__0" CityRequired="true" CountyLabel="County" CountyID="USER_LDHP_HHDETAIL__COUNTY__0" CountyRequired="true" CountryLabel="Country" CountryID="USER_LDHP_HHDETAIL__COUNTRY__0" CountryRequired="true" ShowHelpText="false" />
             <ol class="formlayout">
             <li id="subUSER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0">TypeOfProperty</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" runat="server" ListViewName="hh_type_property_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__TYPEOFPROPERTY_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="TypeOfProperty" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__LISTSTATUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__LISTSTATUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__LISTSTATUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__LISTSTATUS_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__LISTSTATUS_ID__0">ListStatus</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__LISTSTATUS_ID__0" runat="server" ListViewName="hh_list_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__LISTSTATUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__LISTSTATUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ListStatus" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__FLATORFELT_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__FLATORFELT_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__FLATORFELT_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__FLATORFELT_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__FLATORFELT_ID__0">FlatOrFelt</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__FLATORFELT_ID__0" runat="server" ListViewName="parkhome_rooftype_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__FLATORFELT_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__FLATORFELT_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="FlatOrFelt" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__ROOFCONST_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__ROOFCONST_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__ROOFCONST_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__ROOFCONST_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__ROOFCONST_ID__0">RoofConst</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__ROOFCONST_ID__0" runat="server" ListViewName="hh_roof_construct_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__ROOFCONST_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__ROOFCONST_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="RoofConst" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__WALLCONST_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__WALLCONST_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__WALLCONST_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__WALLCONST_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__WALLCONST_ID__0">WallConst</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__WALLCONST_ID__0" runat="server" ListViewName="hh_wall_construct_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__WALLCONST_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__WALLCONST_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="WallConst" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__NUMOFBEDROOMS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__NUMOFBEDROOMS__0">NumofBedrooms</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" runat="server" MaxLength="10"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__NUMOFBEDROOMS__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="NumofBedrooms" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__DOORSLOCKED__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__DOORSLOCKED__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__DOORSLOCKED__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__DOORSLOCKED__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__DOORSLOCKED__0">DoorsLocked</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__DOORSLOCKED__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__WINDOWLOCKS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__WINDOWLOCKS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__WINDOWLOCKS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__WINDOWLOCKS__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__WINDOWLOCKS__0">WindowLocks</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__WINDOWLOCKS__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__WATERSWITCH__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__WATERSWITCH__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__WATERSWITCH__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__WATERSWITCH__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__WATERSWITCH__0">WaterSwitch</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__WATERSWITCH__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__SCENTRANCE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__SCENTRANCE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__SCENTRANCE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__SCENTRANCE__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__SCENTRANCE__0">SCEntrance</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__SCENTRANCE__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__GSREPAIR__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__GSREPAIR__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__GSREPAIR__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__GSREPAIR__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__GSREPAIR__0">GSRepair</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__GSREPAIR__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0">Unoccupancy</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" runat="server" ListViewName="hh_unoccupancy_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__UNOCCUPANCY_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Unoccupancy" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__BUSUSE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__BUSUSE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__BUSUSE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__BUSUSE_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__BUSUSE_ID__0">BusUse</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__BUSUSE_ID__0" runat="server" ListViewName="hh_business_use_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__BUSUSE_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__BUSUSE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="BusUse" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__ALARM_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__ALARM_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__ALARM_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__ALARM_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__ALARM_ID__0">cboAlarm</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__ALARM_ID__0" runat="server" ListViewName="hh_alarm_fitted_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__ALARM_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__ALARM_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboAlarm" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__SMOKEDETECT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__SMOKEDETECT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__SMOKEDETECT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__SMOKEDETECT__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__SMOKEDETECT__0">chkSmokeDetect</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__SMOKEDETECT__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__HIRING_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__HIRING_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__HIRING_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__HIRING_ID__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__HIRING_ID__0">cboHiring</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_HHDETAIL__HIRING_ID__0" runat="server" ListViewName="hh_mortgage_hirin_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__HIRING_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__HIRING_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboHiring" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__BUILDINGVALUE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__BUILDINGVALUE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__BUILDINGVALUE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__BUILDINGVALUE__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__BUILDINGVALUE__0">BuildingValue</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HHDETAIL__BUILDINGVALUE__0" runat="server" isDecimalValue="True"  MaxLength="50"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__BUILDINGVALUE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__BUILDINGVALUE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="BuildingValue" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__CONTENTVALUE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__CONTENTVALUE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__CONTENTVALUE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__CONTENTVALUE__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__CONTENTVALUE__0">ContentValue</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HHDETAIL__CONTENTVALUE__0" runat="server" MaxLength="50" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__CONTENTVALUE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__CONTENTVALUE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ContentValue" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__ADCOVER__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__ADCOVER__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__ADCOVER__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__ADCOVER__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__ADCOVER__0">ADCover</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__ADCOVER__0" runat="server"><relatedControl ctl="USER_LDHP_HHDETAIL__ADPETS__0_ctl" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="lblUSER_LDHP_HHDETAIL__ADPETS__0" type="enable" checkedItem="True" page="hhdetail.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__ADPETS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__ADPETS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__ADPETS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__ADPETS__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__ADPETS__0">ADPets</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__ADPETS__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__BLDAD__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__BLDAD__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__BLDAD__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__BLDAD__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__BLDAD__0">chkBldAD</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__BLDAD__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__BLDCOVER__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__BLDCOVER__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__BLDCOVER__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__BLDCOVER__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__BLDCOVER__0">chkBLDCover</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__BLDCOVER__0" runat="server"><relatedControl ctl="USER_LDHP_HHDETAIL__BLDAD__0_ctl" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="lblUSER_LDHP_HHDETAIL__BLDAD__0" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="USER_LDHP_HHDETAIL__BUILDINGVALUE__0_ctl" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="lblUSER_LDHP_HHDETAIL__BUILDINGVALUE__0" type="enable" checkedItem="True" page="hhdetail.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__CNTCOVER__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__CNTCOVER__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__CNTCOVER__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__CNTCOVER__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__CNTCOVER__0">chkCNTCover</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_HHDETAIL__CNTCOVER__0" runat="server"><relatedControl ctl="USER_LDHP_HHDETAIL__ADCOVER__0_ctl" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="lblUSER_LDHP_HHDETAIL__ADCOVER__0" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="USER_LDHP_HHDETAIL__CONTENTVALUE__0_ctl" type="enable" checkedItem="True" page="hhdetail.aspx" /><relatedControl ctl="lblUSER_LDHP_HHDETAIL__CONTENTVALUE__0" type="enable" checkedItem="True" page="hhdetail.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__YEARBUILT__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__YEARBUILT__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__YEARBUILT__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__YEARBUILT__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__YEARBUILT__0">YearBuilt</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HHDETAIL__YEARBUILT__0" runat="server" MaxLength="4"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__YEARBUILT__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__YEARBUILT__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="YearBuilt" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_HHDETAIL__SLEEPS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_HHDETAIL__SLEEPS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_HHDETAIL__SLEEPS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_HHDETAIL__SLEEPS__0" runat="server" AssociatedControlID="USER_LDHP_HHDETAIL__SLEEPS__0">txtSleeps</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_HHDETAIL__SLEEPS__0" runat="server" MaxLength="2" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_HHDETAIL__SLEEPS__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_HHDETAIL__SLEEPS__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="txtSleeps" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
