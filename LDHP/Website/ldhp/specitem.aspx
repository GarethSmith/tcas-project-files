<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Specified Item</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Specified Item</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_SPECITEM__ITEMDESC__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_SPECITEM__ITEMDESC__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_SPECITEM__ITEMDESC__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_SPECITEM__ITEMDESC__0" runat="server" AssociatedControlID="USER_LDHP_SPECITEM__ITEMDESC__0">ItemDesc</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_SPECITEM__ITEMDESC__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_SPECITEM__ITEMDESC__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_SPECITEM__ITEMDESC__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="ItemDesc" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_SPECITEM__VALUE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_SPECITEM__VALUE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_SPECITEM__VALUE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_SPECITEM__VALUE__0" runat="server" AssociatedControlID="USER_LDHP_SPECITEM__VALUE__0">Value</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_SPECITEM__VALUE__0" runat="server" MaxLength="200"  Numeric="True" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_SPECITEM__VALUE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_SPECITEM__VALUE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Value" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_SPECITEM__MAKE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_SPECITEM__MAKE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_SPECITEM__MAKE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_SPECITEM__MAKE__0" runat="server" AssociatedControlID="USER_LDHP_SPECITEM__MAKE__0">Make</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_SPECITEM__MAKE__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_SPECITEM__MAKE__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_SPECITEM__MAKE__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Make" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_SPECITEM__MODEL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_SPECITEM__MODEL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_SPECITEM__MODEL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_SPECITEM__MODEL__0" runat="server" AssociatedControlID="USER_LDHP_SPECITEM__MODEL__0">Model</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_SPECITEM__MODEL__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_SPECITEM__MODEL__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_SPECITEM__MODEL__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Model" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_SPECITEM__SERIALNUMBER__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_SPECITEM__SERIALNUMBER__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_SPECITEM__SERIALNUMBER__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_SPECITEM__SERIALNUMBER__0" runat="server" AssociatedControlID="USER_LDHP_SPECITEM__SERIALNUMBER__0">SerialNumber</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_SPECITEM__SERIALNUMBER__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_SPECITEM__SERIALNUMBER__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_SPECITEM__SERIALNUMBER__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="SerialNumber" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
