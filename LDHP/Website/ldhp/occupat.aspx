<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Occupation Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LOBParent" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Proposer Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" runat="server" style="display:none;"></li>
             <li>
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" runat="server" AssociatedControlID="USER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0">Proposer Details</tes_webcontrols:TGSLLabel>
                 <span class="control"><asp:DropDownList id="USER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" runat="server"></asp:DropDownList>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_OCCUPAT__LDHP_PROPDET_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Proposer Details" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Occupation Details</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_OCCUPAT__OCCUPATION_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_OCCUPAT__OCCUPATION_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_OCCUPAT__OCCUPATION_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_OCCUPAT__OCCUPATION_ID__0" runat="server" AssociatedControlID="USER_LDHP_OCCUPAT__OCCUPATION_ID__0">Occupation</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_OCCUPAT__OCCUPATION_ID__0" runat="server" ListViewName="occupation_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_OCCUPAT__OCCUPATION_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_OCCUPAT__OCCUPATION_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Occupation" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" runat="server" AssociatedControlID="USER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0">EmployersBusiness</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" runat="server" ListViewName="employers_bus_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_OCCUPAT__EMPLOYERSBUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="EmployersBusiness" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_OCCUPAT__EMPSTATUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_OCCUPAT__EMPSTATUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_OCCUPAT__EMPSTATUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_OCCUPAT__EMPSTATUS_ID__0" runat="server" AssociatedControlID="USER_LDHP_OCCUPAT__EMPSTATUS_ID__0">EmpStatus</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_LDHP_OCCUPAT__EMPSTATUS_ID__0" runat="server" ListViewName="employment_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_OCCUPAT__EMPSTATUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_OCCUPAT__EMPSTATUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="EmpStatus" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_LDHP_OCCUPAT__PARTTIME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_OCCUPAT__PARTTIME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_OCCUPAT__PARTTIME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_OCCUPAT__PARTTIME__0" runat="server" AssociatedControlID="USER_LDHP_OCCUPAT__PARTTIME__0">PartTime</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_OCCUPAT__PARTTIME__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
