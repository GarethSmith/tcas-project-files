<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">General Questions</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>General Questions</legend>
         <ol class="formlayout">
             <li id="subUSER_LDHP_GENQUEST__REFORCANCEL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_GENQUEST__REFORCANCEL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_GENQUEST__REFORCANCEL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_GENQUEST__REFORCANCEL__0" runat="server" AssociatedControlID="USER_LDHP_GENQUEST__REFORCANCEL__0">RefOrCancel</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_GENQUEST__REFORCANCEL__0" runat="server"><relatedControl ctl="USER_LDHP_GENQUEST__DETAILS__0_ctl" type="enable" checkedItem="True" page="genquest.aspx" /><relatedControl ctl="lblUSER_LDHP_GENQUEST__DETAILS__0" type="enable" checkedItem="True" page="genquest.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_GENQUEST__CRIMINALOFFENCE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_GENQUEST__CRIMINALOFFENCE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_GENQUEST__CRIMINALOFFENCE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_GENQUEST__CRIMINALOFFENCE__0" runat="server" AssociatedControlID="USER_LDHP_GENQUEST__CRIMINALOFFENCE__0">CriminalOffence</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_GENQUEST__CRIMINALOFFENCE__0" runat="server"><relatedControl ctl="USER_LDHP_GENQUEST__DETAILS__0_ctl" type="enable" checkedItem="True" page="genquest.aspx" /><relatedControl ctl="lblUSER_LDHP_GENQUEST__DETAILS__0" type="enable" checkedItem="True" page="genquest.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_GENQUEST__EXTRAPRE__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_GENQUEST__EXTRAPRE__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_GENQUEST__EXTRAPRE__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_GENQUEST__EXTRAPRE__0" runat="server" AssociatedControlID="USER_LDHP_GENQUEST__EXTRAPRE__0">ExtraPre</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_LDHP_GENQUEST__EXTRAPRE__0" runat="server"><relatedControl ctl="USER_LDHP_GENQUEST__DETAILS__0_ctl" type="enable" checkedItem="True" page="genquest.aspx" /><relatedControl ctl="lblUSER_LDHP_GENQUEST__DETAILS__0" type="enable" checkedItem="True" page="genquest.aspx" /></tes_webcontrols:TGSLCheckbox></span>
             </li>
             <li id="subUSER_LDHP_GENQUEST__DETAILS__0" runat="server" style="display:none;"></li>
             <li id="pUSER_LDHP_GENQUEST__DETAILS__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_LDHP_GENQUEST__DETAILS__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_LDHP_GENQUEST__DETAILS__0" runat="server" AssociatedControlID="USER_LDHP_GENQUEST__DETAILS__0">Details</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_LDHP_GENQUEST__DETAILS__0" runat="server" TextMode="MultiLine" Rows="4"  MaxLength="800" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_LDHP_GENQUEST__DETAILS__0" runat="server" Branded="False" ControlToValidate="USER_LDHP_GENQUEST__DETAILS__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="Details" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
