<?xml version="1.0" encoding="utf-8"?>
<screen name="frmHHDETAIL.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Holiday Home Details</caption>
    <description>Holiday Home Details</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>518</height>
    <parentID />
    <text>Holiday Home Details</text>
    <width>787</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="lblSleepsUpTo" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblSleepsUpTo</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>28</tabIndex>
            <text>Sleeps Up To</text>
            <textAlign>1</textAlign>
            <top>425</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtYearBuilt" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>YEARBUILT</columnName>
            <description>YearBuilt</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <maximumTotal>9999</maximumTotal>
            <minimumTotal>0</minimumTotal>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>4</maxLength>
            <multiLine>False</multiLine>
            <numeric>True</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>27</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>261</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="txtSleeps" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>SLEEPS</columnName>
            <description>txtSleeps</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>2</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>27</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>423</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="grpSIDets" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>195</height>
            <left>331</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>26</tabIndex>
            <text>Sum Insured</text>
            <top>251</top>
            <width>437</width>
            <controls>
                <control name="chkCNTCover" type="WisCheckBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>LDHP\frmHHDETAIL.tst</boundScreen>
                            <control>chkADCover</control>
                            <enable>True</enable>
                        </enableControl>
                        <enableControl>
                            <boundScreen>LDHP\frmHHDETAIL.tst</boundScreen>
                            <control>txtContentValue</control>
                            <enable>True</enable>
                        </enableControl>
                    </enableControls>
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>CNTCOVER</columnName>
                    <description>chkCNTCover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>22</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>34</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Contents Cover</text>
                    <textAlign>16</textAlign>
                    <top>98</top>
                    <width>184</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkBLDCover" type="WisCheckBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>LDHP\frmHHDETAIL.tst</boundScreen>
                            <control>chkBldAD</control>
                            <enable>True</enable>
                        </enableControl>
                        <enableControl>
                            <boundScreen>LDHP\frmHHDETAIL.tst</boundScreen>
                            <control>txtBuildingValue</control>
                            <enable>True</enable>
                        </enableControl>
                    </enableControls>
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>BLDCOVER</columnName>
                    <description>chkBLDCover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>22</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>33</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Buildings Cover</text>
                    <textAlign>16</textAlign>
                    <top>18</top>
                    <width>184</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkBldAD" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>BLDAD</columnName>
                    <description>chkBldAD</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>22</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>32</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Building AD Cover</text>
                    <textAlign>16</textAlign>
                    <top>73</top>
                    <width>184</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="WisLabel6" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>388</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>28</tabIndex>
                    <text>(GBP)</text>
                    <textAlign>1</textAlign>
                    <top>130</top>
                    <width>38</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtContentValue" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>CONTENTVALUE</columnName>
                    <description>ContentValue</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>196</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>50</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>28</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>128</top>
                    <width>186</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel5" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>180</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>31</tabIndex>
                    <text>£</text>
                    <textAlign>1</textAlign>
                    <top>130</top>
                    <width>10</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblContVal" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>ContVal</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>30</tabIndex>
                    <text>Contents Value</text>
                    <textAlign>1</textAlign>
                    <top>130</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="chkADPets" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>ADPETS</columnName>
                    <description>ADPets</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>294</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>30</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>AD By Pets</text>
                    <textAlign>16</textAlign>
                    <top>156</top>
                    <width>88</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkADCover" type="WisCheckBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>LDHP\frmHHDETAIL.tst</boundScreen>
                            <control>chkADPets</control>
                            <enable>True</enable>
                        </enableControl>
                    </enableControls>
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>ADCOVER</columnName>
                    <description>ADCover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>22</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>29</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Contents AD Cover</text>
                    <textAlign>16</textAlign>
                    <top>156</top>
                    <width>184</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="WisLabel4" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>388</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>19</tabIndex>
                    <text>(GBP)</text>
                    <textAlign>1</textAlign>
                    <top>50</top>
                    <width>38</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="WisLabel3" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>180</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>18</tabIndex>
                    <text>£</text>
                    <textAlign>1</textAlign>
                    <top>50</top>
                    <width>10</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="WisLabel2" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>17</tabIndex>
                    <text>Building Value</text>
                    <textAlign>1</textAlign>
                    <top>50</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtBuildingValue" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>BUILDINGVALUE</columnName>
                    <description>BuildingValue</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>True</useDecimalPlaces>
                    <minimumTotal />
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>196</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>50</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>True</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>27</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>48</top>
                    <width>186</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="grpSecDet" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>235</height>
            <left>331</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>16</tabIndex>
            <text>Security and Extras</text>
            <top>10</top>
            <width>437</width>
            <controls>
                <control name="WisLabel8" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>27</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>33</tabIndex>
                    <text>Mortgage &amp;&amp; Hiring</text>
                    <textAlign>1</textAlign>
                    <top>152</top>
                    <width>118</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboHiring" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HH_MORTGAGE_HIRIN_ID</columnName>
                    <description>cboHiring</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>198</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HH_MORTGAGE_HIRIN</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>32</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>149</top>
                    <width>228</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="chkSmokeDetect" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>SMOKEDETECT</columnName>
                    <description>chkSmokeDetect</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>24</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>31</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Smoke Detectors</text>
                    <textAlign>16</textAlign>
                    <top>95</top>
                    <width>185</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="WisLabel7" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>27</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>30</tabIndex>
                    <text>Alarm Fitted</text>
                    <textAlign>1</textAlign>
                    <top>73</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboAlarm" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HH_ALARM_FITTED_ID</columnName>
                    <description>cboAlarm</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>198</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HH_ALARM_FITTED</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>29</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>68</top>
                    <width>228</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="WisLabel1" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>27</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>26</tabIndex>
                    <text>Other Business Use</text>
                    <textAlign>1</textAlign>
                    <top>208</top>
                    <width>119</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblUnoccu" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblUnoccu</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>27</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>25</tabIndex>
                    <text>Length of Unoccupancy</text>
                    <textAlign>1</textAlign>
                    <top>181</top>
                    <width>152</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboBusUse" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HH_BUSINESS_USE_ID</columnName>
                    <description>BusUse</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>198</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HH_BUSINESS_USE</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>25</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>205</top>
                    <width>228</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="cboUnoccupancy" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HH_UNOCCUPANCY_ID</columnName>
                    <description>Unoccupancy</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>198</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HH_UNOCCUPANCY</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>24</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>178</top>
                    <width>228</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="chkGSRepair" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>GSREPAIR</columnName>
                    <description>GSRepair</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>22</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Good State of Repair?</text>
                    <textAlign>16</textAlign>
                    <top>125</top>
                    <width>185</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkWaterSwitch" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>WATERSWITCH</columnName>
                    <description>WaterSwitch</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>224</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>20</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Electronic Water Switch Fitted?</text>
                    <textAlign>16</textAlign>
                    <top>98</top>
                    <width>202</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkSCEntrance" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>SCENTRANCE</columnName>
                    <description>SCEntrance</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>21</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Self Contained Entrance</text>
                    <textAlign>16</textAlign>
                    <top>20</top>
                    <width>185</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkWindowLocks" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>WINDOWLOCKS</columnName>
                    <description>WindowLocks</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>24</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>19</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>Key/Push locks on windows?</text>
                    <textAlign>16</textAlign>
                    <top>44</top>
                    <width>185</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
                <control name="chkDoorsLocked" type="WisCheckBox">
                    <enableControls />
                    <displayPages />
                    <appearance>0</appearance>
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <columnName>DOORSLOCKED</columnName>
                    <description>DoorsLocked</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>18</height>
                    <left>241</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>17</tabIndex>
                    <checkAlign>64</checkAlign>
                    <text>All external doors locked?</text>
                    <textAlign>16</textAlign>
                    <top>20</top>
                    <width>185</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <displayAsYesNo>False</displayAsYesNo>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="txtNumofBedrooms" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>NUMOFBEDROOMS</columnName>
            <description>NumofBedrooms</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <maxLength>10</maxLength>
            <multiLine>False</multiLine>
            <numeric>True</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>15</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>396</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblNumBeds" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblNumBeds</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <text>Num Of Bedrooms</text>
            <textAlign>1</textAlign>
            <top>398</top>
            <width>111</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboWallConst" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>HH_WALL_CONSTRUCT_ID</columnName>
            <description>WallConst</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_HH_WALL_CONSTRUCT</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>14</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>315</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblWallConst" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblWallConst</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>12</tabIndex>
            <text>Wall Construction</text>
            <textAlign>1</textAlign>
            <top>318</top>
            <width>107</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboRoofConst" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>HH_ROOF_CONSTRUCT_ID</columnName>
            <description>RoofConst</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_HH_ROOF_CONSTRUCT</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>13</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>342</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblRoofConst" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblRoofConst</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>Roof Construction</text>
            <textAlign>1</textAlign>
            <top>345</top>
            <width>107</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="lblFlatOrFelt" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblFlatOrFelt</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text>Flat or Felt Roof?</text>
            <textAlign>1</textAlign>
            <top>372</top>
            <width>104</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboFlatOrFelt" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>PARKHOME_ROOFTYPE_ID</columnName>
            <description>FlatOrFelt</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_PARKHOME_ROOFTYPE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>12</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>369</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblListStatus" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblListStatus</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>6</tabIndex>
            <text>List Status</text>
            <textAlign>1</textAlign>
            <top>291</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboListStatus" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>HH_STATUS_ID</columnName>
            <description>ListStatus</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_HH_LIST_STATUS</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>11</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>288</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblYearBuilt" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblYearBuilt</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text>Year Built</text>
            <textAlign>1</textAlign>
            <top>264</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="lblTypeOfProperty" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblTypeOfProperty</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>15</height>
            <left>17</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Type of Property</text>
            <textAlign>1</textAlign>
            <top>237</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboTypeOfProperty" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>HH_TYPE_PROPERTY_ID</columnName>
            <description>TypeOfProperty</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>123</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <listTable>LIST_HH_TYPE_PROPERTY</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>234</top>
            <width>184</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="grpPropAdd" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>218</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Property Address</text>
            <top>10</top>
            <width>315</width>
            <controls>
                <control name="lblCountry" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblCountry</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>14</tabIndex>
                    <text>Country</text>
                    <textAlign>1</textAlign>
                    <top>189</top>
                    <width>50</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblCounty" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblCounty</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>13</tabIndex>
                    <text>County</text>
                    <textAlign>1</textAlign>
                    <top>162</top>
                    <width>50</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblCity" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblCity</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>12</tabIndex>
                    <text>City</text>
                    <textAlign>1</textAlign>
                    <top>135</top>
                    <width>50</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblLocality" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblLocality</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>11</tabIndex>
                    <text>Locality</text>
                    <textAlign>1</textAlign>
                    <top>108</top>
                    <width>50</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblStreet" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblStreet</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>10</tabIndex>
                    <text>Street</text>
                    <textAlign>1</textAlign>
                    <top>81</top>
                    <width>51</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblHouse" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblHouse</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>9</tabIndex>
                    <text>House</text>
                    <textAlign>1</textAlign>
                    <top>54</top>
                    <width>44</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="lblPostcocde" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>LblPostcode</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>15</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>8</tabIndex>
                    <text>Postcode</text>
                    <textAlign>1</textAlign>
                    <top>26</top>
                    <width>63</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cmdPostSearch" type="WisButton">
                    <searchProperties>
                        <Postcode>txtPostcode</Postcode>
                        <House>txtHouse</House>
                        <Street>txtStreet</Street>
                        <Locality>txtLocality</Locality>
                        <City>txtCity</City>
                        <County>txtCounty</County>
                        <Country>txtCountry</Country>
                    </searchProperties>
                    <pluginId>29843A42AE0740DDAD98E8A15206F0C8</pluginId>
                    <searchType>TES Postcode Search</searchType>
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <writableProperties />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>PostSearch</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>265</left>
                    <top>22</top>
                    <tabIndex>2</tabIndex>
                    <type>6</type>
                    <text />
                    <width>32</width>
                </control>
                <control name="txtCountry" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>COUNTRY</columnName>
                    <description>Country</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>8</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>187</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtCounty" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>COUNTY</columnName>
                    <description>County</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>7</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>160</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtCity" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>CITY</columnName>
                    <description>City</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>6</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>133</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtLocality" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>LOCALITY</columnName>
                    <description>Locality</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>106</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtStreet" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>STREET</columnName>
                    <description>Street</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>79</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtHouse" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>HOUSE</columnName>
                    <description>House</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>52</top>
                    <width>221</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtPostcode" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>POSTCODE</columnName>
                    <description>Postcode</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>76</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>24</top>
                    <width>183</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
            </controls>
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
    <formValidation />
    <previousRiskAtAQ />
    <hideClaim />
</screen>