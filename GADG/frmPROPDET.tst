<?xml version="1.0" encoding="utf-8"?>
<screen name="frmPROPDET.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Proposer Details</caption>
    <description>Proposer Details</description>
    <screenType>3</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>320</height>
    <parentID />
    <text>Proposer Details</text>
    <width>366</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="grpEmployment" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>100</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>10</tabIndex>
            <text>Employment Details</text>
            <top>150</top>
            <width>337</width>
            <controls>
                <control name="cboEmpStat" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>EMPLOYMENT_STATUS_ID</columnName>
                    <description>cboEmpStat</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>134</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_EMPLOYMENT_STATUS</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>68</top>
                    <width>191</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblEmploymentSta" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblEmploymentSta</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>7</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text>Employment Status</text>
                    <textAlign>1</textAlign>
                    <top>71</top>
                    <width>125</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboEmpBus" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>EMPLOYERS_BUS_ID</columnName>
                    <description>cboEmpBus</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>134</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_EMPLOYERS_BUS</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>41</top>
                    <width>191</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblEmpBus" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblEmpBus</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <text>Employers Business</text>
                    <textAlign>1</textAlign>
                    <top>44</top>
                    <width>126</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboOccupation" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>OCCUPATION_ID</columnName>
                    <description>cboOccupation</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>134</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <linkedData />
                    <listTable>LIST_OCCUPATION</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>14</top>
                    <width>191</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblOccup" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblOccup</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text>Occupation</text>
                    <textAlign>1</textAlign>
                    <top>17</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="mskDOB" type="WisMaskedBox">
            <dateRestriction />
            <cultureInfo>en-GB</cultureInfo>
            <displayFormat>dd/MM/yyyy</displayFormat>
            <startYear>0</startYear>
            <endYear>0</endYear>
            <reverseList>False</reverseList>
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>DOB</columnName>
            <description>mskDOB</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>144</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData>dtDOB</linkedData>
            <mask>##/##/####</mask>
            <maxLength>32767</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>9</tabIndex>
            <text>__/__/____</text>
            <textAlign>0</textAlign>
            <top>122</top>
            <width>100</width>
            <boundObject>0</boundObject>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblDOB" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblDOB</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>8</tabIndex>
            <text>Date Of Birth</text>
            <textAlign>1</textAlign>
            <top>124</top>
            <width>81</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtSurname" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>SURNAME</columnName>
            <description>txtSurname</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>144</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData>sSurname</linkedData>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>7</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>95</top>
            <width>191</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblSurname" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblSurname</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>6</tabIndex>
            <text>Surname</text>
            <textAlign>1</textAlign>
            <top>97</top>
            <width>100</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtInitial" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>INITIAL</columnName>
            <description>txtInitial</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>144</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData>sInitials</linkedData>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>5</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>68</top>
            <width>77</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblInitial" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblInitial</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>4</tabIndex>
            <text>Initial</text>
            <textAlign>1</textAlign>
            <top>70</top>
            <width>75</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="txtForename" type="WisTextBox">
            <enableControls />
            <sumTotalControls />
            <acceptsTab>False</acceptsTab>
            <acceptsReturn>False</acceptsReturn>
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <borderStyle>1</borderStyle>
            <characterCasing>0</characterCasing>
            <columnName>FORENAME</columnName>
            <description>txtForename</description>
            <decimalPlaces>0</decimalPlaces>
            <useDecimalPlaces>False</useDecimalPlaces>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>144</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData>sForename</linkedData>
            <maxLength>200</maxLength>
            <multiLine>False</multiLine>
            <numeric>False</numeric>
            <autoIncrement>False</autoIncrement>
            <maxAutoValue>0</maxAutoValue>
            <properCase>False</properCase>
            <rightToLeft>0</rightToLeft>
            <tabIndex>3</tabIndex>
            <text />
            <textAlign>0</textAlign>
            <top>43</top>
            <width>191</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblForeName" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblForeName</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>25</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>2</tabIndex>
            <text>Forename</text>
            <textAlign>1</textAlign>
            <top>45</top>
            <width>65</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
        <control name="cboTitle" type="WisComboBox">
            <enableControls />
            <backColor>
                <r>255</r>
                <g>255</g>
                <b>255</b>
            </backColor>
            <columnName>TITLE_ID</columnName>
            <description>cboTitle</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>21</height>
            <left>144</left>
            <font>
                <bold>False</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <linkedData>objTitle</linkedData>
            <listTable>LIST_TITLE</listTable>
            <maxLength>0</maxLength>
            <numeric>False</numeric>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <userFilled>False</userFilled>
            <text />
            <top>16</top>
            <width>77</width>
            <objectID>0</objectID>
            <longPropertyID>0</longPropertyID>
            <defaultValue />
            <showHelpText>False</showHelpText>
            <helpText />
            <required>True</required>
            <displayOnWebpage>True</displayOnWebpage>
            <alwaysVisible>True</alwaysVisible>
            <controls />
        </control>
        <control name="lblTitle" type="WisLabel">
            <sumTotalControls />
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <borderStyle>0</borderStyle>
            <description>lblTitle</description>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>23</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Title</text>
            <textAlign>1</textAlign>
            <top>20</top>
            <width>65</width>
            <isDateDiff>0</isDateDiff>
            <controls />
        </control>
    </controls>
    <generalSummary>
        <columns>
            <column>
                <text>Title</text>
                <bindControl>cboTitle</bindControl>
            </column>
            <column>
                <text>Forename</text>
                <bindControl>txtForename</bindControl>
            </column>
            <column>
                <text>Surname</text>
                <bindControl>txtSurname</bindControl>
            </column>
        </columns>
    </generalSummary>
    <formValidation />
    <previousRiskAtAQ />
    <hideClaim />
</screen>