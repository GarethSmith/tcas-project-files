<%@ Page Language="VB" MasterPageFile="~/ScreenLOBSingle.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Gadget and Leisure Cover Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Gadget and Leisure Cover Details</legend>
         <ol class="formlayout">
             <li id="subUSER_GADG_GDLE__PRODUCT_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_GDLE__PRODUCT_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_GDLE__PRODUCT_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_GDLE__PRODUCT_ID__0" runat="server" AssociatedControlID="USER_GADG_GDLE__PRODUCT_ID__0">cboProduct</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_GDLE__PRODUCT_ID__0" runat="server" ListViewName="gadg_product_type_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_GDLE__PRODUCT_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_GDLE__PRODUCT_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboProduct" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_GDLE__LEISURE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_GDLE__LEISURE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_GDLE__LEISURE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_GDLE__LEISURE_ID__0" runat="server" AssociatedControlID="USER_GADG_GDLE__LEISURE_ID__0">cboLeisure</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_GDLE__LEISURE_ID__0" runat="server" ListViewName="gadg_lequip_si_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_GDLE__LEISURE_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_GDLE__LEISURE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboLeisure" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_GDLE__GADGET_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_GDLE__GADGET_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_GDLE__GADGET_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_GDLE__GADGET_ID__0" runat="server" AssociatedControlID="USER_GADG_GDLE__GADGET_ID__0">cboGadget</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_GDLE__GADGET_ID__0" runat="server" ListViewName="gadg_gadg_si_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_GDLE__GADGET_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_GDLE__GADGET_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboGadget" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_GDLE__VISITINGFAMILY_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_GDLE__VISITINGFAMILY_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_GDLE__VISITINGFAMILY_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_GDLE__VISITINGFAMILY_ID__0" runat="server" AssociatedControlID="USER_GADG_GDLE__VISITINGFAMILY_ID__0">cboVisitingFamily</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_GDLE__VISITINGFAMILY_ID__0" runat="server" ListViewName="gadg_visit_si_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_GDLE__VISITINGFAMILY_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_GDLE__VISITINGFAMILY_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboVisitingFamily" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_GDLE__EURO__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_GDLE__EURO__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_GDLE__EURO__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_GDLE__EURO__0" runat="server" AssociatedControlID="USER_GADG_GDLE__EURO__0">chkEuro</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLCheckbox ID="USER_GADG_GDLE__EURO__0" runat="server"></tes_webcontrols:TGSLCheckbox></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
