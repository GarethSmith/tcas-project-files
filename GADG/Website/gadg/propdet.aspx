<%@ Page Language="VB" MasterPageFile="~/ScreenLOBMultiple.master" AutoEventWireup="false" Inherits="clsCorePage" %>
<%@ Register Src="../PostcodeSearch.ascx" TagName="PostcodeSearch" TagPrefix="uc1" %>
<%@ Register Src="../vehiclesearch.ascx" TagName="VehicleSearch" TagPrefix="uc2" %>
<%@ Register TagPrefix="tes_webcontrols" Namespace="TES_WebControls" Assembly="TES_WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LOBPageName" Runat="Server">
 <h1 id="pageHeading" runat="server">Proposer Details</h1>
 <div id="pageDescription" runat="server" visible="false"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LOBData" Runat="Server">
 <div class="datacapture">
     <fieldset>
         <legend>Proposer Details</legend>
         <ol class="formlayout">
             <li id="subUSER_GADG_PROPDET__TITLE_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__TITLE_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__TITLE_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__TITLE_ID__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__TITLE_ID__0">cboTitle</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_PROPDET__TITLE_ID__0" runat="server" ListViewName="title_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__TITLE_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__TITLE_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboTitle" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__FORENAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__FORENAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__FORENAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__FORENAME__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__FORENAME__0">txtForename</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_GADG_PROPDET__FORENAME__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__FORENAME__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__FORENAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="txtForename" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__INITIAL__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__INITIAL__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__INITIAL__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__INITIAL__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__INITIAL__0">txtInitial</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_GADG_PROPDET__INITIAL__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__INITIAL__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__INITIAL__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="txtInitial" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__SURNAME__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__SURNAME__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__SURNAME__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__SURNAME__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__SURNAME__0">txtSurname</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLTextBox id="USER_GADG_PROPDET__SURNAME__0" runat="server" MaxLength="200" ></tes_webcontrols:TGSLTextBox>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__SURNAME__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__SURNAME__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="txtSurname" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__DOB__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__DOB__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__DOB__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__DOB__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__DOB__0">mskDOB</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLDateSelect id="USER_GADG_PROPDET__DOB__0" runat="server"></tes_webcontrols:TGSLDateSelect>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__DOB__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__DOB__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="mskDOB" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__OCCUPATION_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__OCCUPATION_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__OCCUPATION_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__OCCUPATION_ID__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__OCCUPATION_ID__0">cboOccupation</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_PROPDET__OCCUPATION_ID__0" runat="server" ListViewName="occupation_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__OCCUPATION_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__OCCUPATION_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboOccupation" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__EMPBUS_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__EMPBUS_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__EMPBUS_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__EMPBUS_ID__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__EMPBUS_ID__0">cboEmpBus</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_PROPDET__EMPBUS_ID__0" runat="server" ListViewName="employers_bus_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__EMPBUS_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__EMPBUS_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboEmpBus" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
             <li id="subUSER_GADG_PROPDET__EMPSTAT_ID__0" runat="server" style="display:none;"></li>
             <li id="pUSER_GADG_PROPDET__EMPSTAT_ID__0">
                 <span class="required"><tes_webcontrols:tgslimage id="imgUSER_GADG_PROPDET__EMPSTAT_ID__0" runat="server" UseBranding="True" ImageFileName="required.gif" ImageUrl="~/images/required.gif"></tes_webcontrols:tgslimage></span>
                 <tes_webcontrols:TGSLLabel ID="lblUSER_GADG_PROPDET__EMPSTAT_ID__0" runat="server" AssociatedControlID="USER_GADG_PROPDET__EMPSTAT_ID__0">cboEmpStat</tes_webcontrols:TGSLLabel>
                 <span class="control"><tes_webcontrols:TGSLGetListDropDown id="USER_GADG_PROPDET__EMPSTAT_ID__0" runat="server" ListViewName="employment_status_view"></tes_webcontrols:TGSLGetListDropDown>&nbsp;<tes_webcontrols:TGSLRequiredValidator ID="reqUSER_GADG_PROPDET__EMPSTAT_ID__0" runat="server" Branded="False" ControlToValidate="USER_GADG_PROPDET__EMPSTAT_ID__0" Display="Dynamic" ShowMandatory="False" EnableClientScript="False" ErrorMessage="cboEmpStat" Override="True" OverrideErrorMessage="*Required"></tes_webcontrols:TGSLRequiredValidator></span>
             </li>
         </ol>
     </fieldset>
 </div>
</asp:Content>
