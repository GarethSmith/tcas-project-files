<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:outcome="http://www.wisl.co.uk/schemas" xmlns:func="http://www.wisl.co.uk/core/functions">

	<xsl:import href="outcomeClient.xslt" />
	<xsl:import href="outcomePolicy.xslt" />
	<xsl:import href="outcomeAddon.xslt" />
	<xsl:import href="outcomeclaims.xslt" />
	<xsl:import href="../core/format.xslt" />

	<xsl:template name="outcome212" match="/outcome:client">

		<xsl:param name="QuoteStage"/>
		<xsl:param name="AgentName"/>
		<xsl:variable name="vPolicyDetailsID" select="outcome:policies/outcome:policy/@iD" />

		<System xmlns="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.wisl.co.uk/schemas http://www.wisl-test.co.uk/schemas/core/outcome.xsd">
			<Client>
				<xsl:call-template name="outcomeClient">
					<xsl:with-param name="AgentName" select="$AgentName" />
				</xsl:call-template>
				<Policy_Details>
					<xsl:call-template name="outcomePolicy">
						<xsl:with-param name="QuoteStage" select="$QuoteStage" />
					</xsl:call-template>
				    <xsl:call-template name="outcomeAddon" />
				    <xsl:call-template name="outcomeClaims" />
                     <xsl:call-template name="frmGDLE.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmPROPDET.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmGeneral.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
				</Policy_Details>
			</Client>
		</System>

	</xsl:template>

     <xsl:template name="frmGDLE.tst" match="frmGDLE.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmGDLE.tst' and @parentID=$pParentID]">
             <UO_GADG_GDLE xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_GADG_GDLE_Euro><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkEuro']/outcome:value='1' or outcome:controls/outcome:control[@name='chkEuro']/outcome:value='true')" /></UQ_GADG_GDLE_Euro>
<UQ_GADG_GDLE_VisitingFamily><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboVisitingFamily']/outcome:value)" /></UQ_GADG_GDLE_VisitingFamily>
<UQ_GADG_GDLE_Gadget><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboGadget']/outcome:value)" /></UQ_GADG_GDLE_Gadget>
<UQ_GADG_GDLE_Leisure><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboLeisure']/outcome:value)" /></UQ_GADG_GDLE_Leisure>
<UQ_GADG_GDLE_Product><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboProduct']/outcome:value)" /></UQ_GADG_GDLE_Product>
             </UO_GADG_GDLE>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmPROPDET.tst" match="frmPROPDET.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmPROPDET.tst' and @parentID=$pParentID]">
             <UO_GADG_PROPDET xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_GADG_PROPDET_EmpStat><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpStat']/outcome:value)" /></UQ_GADG_PROPDET_EmpStat>
<UQ_GADG_PROPDET_EmpBus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpBus']/outcome:value)" /></UQ_GADG_PROPDET_EmpBus>
<UQ_GADG_PROPDET_Occupation><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboOccupation']/outcome:value)" /></UQ_GADG_PROPDET_Occupation>
                 <UQ_GADG_PROPDET_DOB><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskDOB']/outcome:value,2)" /></UQ_GADG_PROPDET_DOB>
                 <UQ_GADG_PROPDET_Surname><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSurname']/outcome:value)" /></UQ_GADG_PROPDET_Surname>
                 <UQ_GADG_PROPDET_Initial><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtInitial']/outcome:value)" /></UQ_GADG_PROPDET_Initial>
                 <UQ_GADG_PROPDET_Forename><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtForename']/outcome:value)" /></UQ_GADG_PROPDET_Forename>
<UQ_GADG_PROPDET_Title><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTitle']/outcome:value)" /></UQ_GADG_PROPDET_Title>
             </UO_GADG_PROPDET>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmGeneral.tst" match="frmGeneral.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmGeneral.tst' and @parentID=$pParentID]">
             <UO_GADG_General xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_GADG_General_Details><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDetails']/outcome:value)" /></UQ_GADG_General_Details>
                 <UQ_GADG_General_Requested><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRequested']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRequested']/outcome:value='true')" /></UQ_GADG_General_Requested>
                 <UQ_GADG_General_Convicted><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='1' or outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='true')" /></UQ_GADG_General_Convicted>
                 <UQ_GADG_General_Refused><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRefused']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRefused']/outcome:value='true')" /></UQ_GADG_General_Refused>
             </UO_GADG_General>
         </xsl:for-each>
     </xsl:template>

</xsl:stylesheet>
