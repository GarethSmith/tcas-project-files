<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmGDLE.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPROPDET.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmGeneral.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmGDLE.tst" match="frmGDLE">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmGDLE[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGDLE.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="gadg_gdle_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkEuro</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="euro" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboVisitingFamily</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="visitingfamily_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="visitingfamily_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboGadget</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="gadget_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="gadget_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboLeisure</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="leisure_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="leisure_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboProduct</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="product_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="product_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPROPDET.tst" match="frmPROPDET">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmPROPDET[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPROPDET.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="gadg_propdet_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:choose>
             <xsl:when test="count(link_id) &gt; 0">
                 <xsl:attribute name="linkID"><xsl:value-of select="link_id" /></xsl:attribute>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:attribute name="linkID">0</xsl:attribute>
             </xsl:otherwise>
         </xsl:choose>
         <controls>
             <control>
                 <xsl:attribute name="name">cboEmpStat</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="empstat_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="empstat_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpBus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="empbus_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="empbus_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboOccupation</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="occupation_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="occupation_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="dob" /></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="surname" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitial</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="initial" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="forename" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="title_id" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="title_debug" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmGeneral.tst" match="frmGeneral">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//frmGeneral[policy_details_id=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGeneral.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="gadg_general_id" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="policy_details_id" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="details" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRequested</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="requested" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkConvicted</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="convicted" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefused</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="refused" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
