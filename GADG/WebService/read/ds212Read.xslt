<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.wisl.co.uk/schemas" xmlns:ds="http://tempuri.org/dsCustomer.xsd">

 <xsl:param name="pPolicy_Details_ID" select="''"></xsl:param>
 <xsl:template name="screens" match="/">
     <xsl:element name="screens">
         <xsl:call-template name="frmGDLE.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmPROPDET.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
         <xsl:call-template name="frmGeneral.tst">
             <xsl:with-param name="pParentID" select="$pPolicy_Details_ID" />
         </xsl:call-template>
     </xsl:element>
 </xsl:template>

<xsl:template name="frmGDLE.tst" match="ds:USER_GADG_GDLE">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_GADG_GDLE[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGDLE.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:GADG_GDLE_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">chkEuro</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:EURO='1' or ds:EURO='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboVisitingFamily</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:VISITINGFAMILY_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:VISITINGFAMILY_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboGadget</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:GADGET_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:GADGET_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboLeisure</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:LEISURE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:LEISURE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboProduct</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:PRODUCT_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:PRODUCT_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmPROPDET.tst" match="ds:USER_GADG_PROPDET">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_GADG_PROPDET[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmPROPDET.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:GADG_PROPDET_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:choose>
             <xsl:when test="count(//ds:USER_GADG_PROPDET_LINK/ds:INSURED_PARTY_ID) &gt; 0">
                 <xsl:attribute name="linkID"><xsl:value-of select="//ds:USER_GADG_PROPDET_LINK/ds:INSURED_PARTY_ID" /></xsl:attribute>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:attribute name="linkID">0</xsl:attribute>
             </xsl:otherwise>
         </xsl:choose>
         <controls>
             <control>
                 <xsl:attribute name="name">cboEmpStat</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EMPSTAT_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EMPSTAT_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboEmpBus</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:EMPBUS_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:EMPBUS_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboOccupation</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:OCCUPATION_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:OCCUPATION_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">mskDOB</xsl:attribute>
                 <xsl:element name="value"><xsl:choose><xsl:when test="normalize-space(ds:DOB)=''">1899-12-30T00:00:00</xsl:when><xsl:otherwise><xsl:value-of select="ds:DOB" /></xsl:otherwise></xsl:choose></xsl:element>
                 <xsl:element name="dataType">d</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtSurname</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:SURNAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtInitial</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:INITIAL" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">txtForename</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:FORENAME" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">cboTitle</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:TITLE_ID" /></xsl:element>
                 <xsl:element name="comboDesc"><xsl:value-of select="ds:TITLE_DEBUG" /></xsl:element>
                 <xsl:element name="dataType">l</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

<xsl:template name="frmGeneral.tst" match="ds:USER_GADG_GENERAL">
<xsl:param name="pParentID" select="''"></xsl:param>
 <xsl:for-each select="//ds:USER_GADG_GENERAL[ds:POLICY_DETAILS_ID=$pParentID]">
     <screen>
         <xsl:attribute name="name">frmGeneral.tst</xsl:attribute>
         <xsl:attribute name="index"><xsl:value-of select="position()" /></xsl:attribute>
         <xsl:attribute name="iD"><xsl:value-of select="ds:GADG_GENERAL_ID" /></xsl:attribute>
         <xsl:attribute name="parentID"><xsl:value-of select="ds:POLICY_DETAILS_ID" /></xsl:attribute>
         <xsl:attribute name="linkID">0</xsl:attribute>
         <controls>
             <control>
                 <xsl:attribute name="name">txtDetails</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="ds:DETAILS" /></xsl:element>
                 <xsl:element name="dataType">t</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRequested</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REQUESTED='1' or ds:REQUESTED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkConvicted</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:CONVICTED='1' or ds:CONVICTED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
             <control>
                 <xsl:attribute name="name">chkRefused</xsl:attribute>
                 <xsl:element name="value"><xsl:value-of select="number(boolean(ds:REFUSED='1' or ds:REFUSED='true'))" /></xsl:element>
                 <xsl:element name="dataType">b</xsl:element>
             </control>
         </controls>
     </screen>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
