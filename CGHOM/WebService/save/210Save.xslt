<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tes="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:func="http://www.wisl.co.uk/functions">

<xsl:import href="../../../../core/functions.xslt" />

 <xsl:param name="pPolicyDetailsID" select="''" />
 <xsl:param name="pHistoryID" select="''" />

 <xsl:template name="screens" match="/">

    <ArrayOfStcStoredProcedure>

        <stcStoredProcedure/>

         <stcStoredProcedure>
             <strName>SP_BO_SAVE_POLICY_LINK</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@cipPolicyLinkID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="func:getguid()"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:client/@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cipInsuredPartyHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="tes:client/@hID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdPolicyDetailsID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pPolicyDetailsID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@cpdHistoryID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

        <xsl:call-template name="frmHED.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmPROPDET.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

        <xsl:call-template name="frmGeneral.tst">
	        <xsl:with-param name="pParentID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
	        <xsl:with-param name="pHistoryID" select="$pHistoryID" />
        </xsl:call-template>

    </ArrayOfStcStoredProcedure>
</xsl:template>

 <xsl:template name="frmHED.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmHED.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGHOM_HED</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGHOM_HED_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CITY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCity']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@LOCALITY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtLocality']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@STREET</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtStreet']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HOUSE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtHouse']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POSTCODE</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtPostcode']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MH_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboMH']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TCARA_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboTCara']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HC_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboHC']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@MR_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboMR']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HPCOVER_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboHPCover']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@COUNTY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCounty']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POSTSECT</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtPostSect']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@COUNTRY</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtCountry']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmPROPDET.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmPROPDET.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGHOM_PROPDET</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGHOM_PROPDET_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EMPSTAT_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboEmpStat']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@OCCUPATION_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboOccupation']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DOB</strName>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value!=''">
                         <objValue xsi:type="xsd:dateTime"><xsl:value-of select="tes:controls/tes:control[@name='mskDOB']/tes:value"/></objValue>
                     </xsl:if>
                     <xsl:if test="tes:controls/tes:control[@name='mskDOB']/tes:value=''">
                         <objValue></objValue>
                     </xsl:if>
                 </stcParameter>
                 <stcParameter>
                     <strName>@SURNAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtSurname']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@INITIAL</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtInitial']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@FORENAME</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtForename']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@TITLE_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboTitle']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@EMPBUS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='cboEmpBus']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

         <xsl:call-template name="frmPROPDET.tstlink">
             <xsl:with-param name="pParentID" select="$pParentID" />
             <xsl:with-param name="pPolicyDetailsID" select="$pPolicyDetailsID" />
             <xsl:with-param name="pHistoryID" select="$pHistoryID" />
             <xsl:with-param name="pInsuredPartyHistoryID" select="//tes:client/@hID" />
         </xsl:call-template>

     </xsl:for-each>
 </xsl:template>

 <xsl:template name="frmGeneral.tst">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:for-each select="//tes:screen[@name='frmGeneral.tst' and @parentID=$pParentID]">
         <stcStoredProcedure>
             <strName>USER_SAVE_CGHOM_GENERAL</strName>
             <udtParameters>
                 <stcParameter />
                 <stcParameter>
                     <strName>@CGHOM_GENERAL_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@HISTORY_ID</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@POLICY_DETAILS_ID</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="$pParentID"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@DETAILS</strName>
                     <objValue xsi:type="xsd:string"><xsl:value-of select="tes:controls/tes:control[@name='txtDetails']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REQUESTED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRequested']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@CONVICTED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkConvicted']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@REFUSED</strName>
                     <objValue xsi:type="xsd:boolean"><xsl:value-of select="tes:controls/tes:control[@name='chkRefused']/tes:value"/></objValue>
                 </stcParameter>
                 <stcParameter>
                     <strName>@USERINSTANCE</strName>
                     <objValue xsi:type="xsd:int"><xsl:value-of select="@index"/></objValue>
                 </stcParameter>
             </udtParameters>
         </stcStoredProcedure>

     </xsl:for-each>
 </xsl:template>

<xsl:template name="frmPROPDET.tstlink">
     <xsl:param name="pParentID" select="''" />
     <xsl:param name="pPolicyDetailsID" select="''" />
     <xsl:param name="pHistoryID" select="''" />
     <xsl:param name="pInsuredPartyHistoryID" select="''" />
         <xsl:if test="@linkID != '0'">
			<stcStoredProcedure>
				<strName>USER_SAVE_CGHOM_PROPDET_LINK</strName>
				<udtParameters>
					<stcParameter />
					<stcParameter>
						<strName>@CGHOM_PROPDET_LINK_ID</strName>
						<objValue xsi:type="xsd:string"><xsl:value-of select="func:getguid()"/></objValue>
					</stcParameter>
					<stcParameter>
						<strName>@HISTORY_ID</strName>
						<objValue xsi:type="xsd:int"><xsl:value-of select="$pHistoryID"/></objValue>
					</stcParameter>
					<stcParameter>
						<strName>@POLICY_DETAILS_ID</strName>
						<objValue xsi:type="xsd:string"><xsl:value-of select="$pPolicyDetailsID"/></objValue>
					</stcParameter>
					<stcParameter>
						<strName>@CGHOM_PROPDET_ID</strName>
						<objValue xsi:type="xsd:string"><xsl:value-of select="@iD"/></objValue>
					</stcParameter>
					<stcParameter>
						<strName>@INSURED_PARTY_ID</strName>
						<objValue xsi:type="xsd:string"><xsl:value-of select="@linkID"/></objValue>
					</stcParameter>
					<stcParameter>
						<strName>@INSURED_PARTY_HISTORY_ID</strName>
						<objValue xsi:type="xsd:int"><xsl:value-of select="$pInsuredPartyHistoryID"/></objValue>
					</stcParameter>
				</udtParameters>
			</stcStoredProcedure>
         </xsl:if>
 </xsl:template>

</xsl:stylesheet>
