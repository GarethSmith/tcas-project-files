<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:outcome="http://www.wisl.co.uk/schemas" xmlns:func="http://www.wisl.co.uk/core/functions">

	<xsl:import href="outcomeClient.xslt" />
	<xsl:import href="outcomePolicy.xslt" />
	<xsl:import href="outcomeAddon.xslt" />
	<xsl:import href="outcomeclaims.xslt" />
	<xsl:import href="../core/format.xslt" />

	<xsl:template name="outcome210" match="/outcome:client">

		<xsl:param name="QuoteStage"/>
		<xsl:param name="AgentName"/>
		<xsl:variable name="vPolicyDetailsID" select="outcome:policies/outcome:policy/@iD" />

		<System xmlns="http://www.wisl.co.uk/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.wisl.co.uk/schemas http://www.wisl-test.co.uk/schemas/core/outcome.xsd">
			<Client>
				<xsl:call-template name="outcomeClient">
					<xsl:with-param name="AgentName" select="$AgentName" />
				</xsl:call-template>
				<Policy_Details>
					<xsl:call-template name="outcomePolicy">
						<xsl:with-param name="QuoteStage" select="$QuoteStage" />
					</xsl:call-template>
				    <xsl:call-template name="outcomeAddon" />
				    <xsl:call-template name="outcomeClaims" />
                     <xsl:call-template name="frmHED.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmPROPDET.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
                     <xsl:call-template name="frmGeneral.tst">
                         <xsl:with-param name="pParentID" select="$vPolicyDetailsID" />
                     </xsl:call-template>
				</Policy_Details>
			</Client>
		</System>

	</xsl:template>

     <xsl:template name="frmHED.tst" match="frmHED.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmHED.tst' and @parentID=$pParentID]">
             <UO_CGHOM_HED xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGHOM_HED_City><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCity']/outcome:value)" /></UQ_CGHOM_HED_City>
                 <UQ_CGHOM_HED_Locality><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtLocality']/outcome:value)" /></UQ_CGHOM_HED_Locality>
                 <UQ_CGHOM_HED_Street><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtStreet']/outcome:value)" /></UQ_CGHOM_HED_Street>
                 <UQ_CGHOM_HED_House><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtHouse']/outcome:value)" /></UQ_CGHOM_HED_House>
                 <UQ_CGHOM_HED_Postcode_Town><xsl:value-of select="func:PostCodeTown(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_CGHOM_HED_Postcode_Town>
                 <UQ_CGHOM_HED_Postcode_Sector><xsl:value-of select="func:PostCodeSector(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_CGHOM_HED_Postcode_Sector>
                 <UQ_CGHOM_HED_Postcode_TownAndSector><xsl:value-of select="func:PostCodeTown(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /><xsl:value-of select="func:PostCodeSector(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_CGHOM_HED_Postcode_TownAndSector>
                 <UQ_CGHOM_HED_Postcode_TownOnly><xsl:value-of select="func:PostCodeTownOnly(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_CGHOM_HED_Postcode_TownOnly>
                 <UQ_CGHOM_HED_Postcode><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtPostcode']/outcome:value)" /></UQ_CGHOM_HED_Postcode>
<UQ_CGHOM_HED_MH><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboMH']/outcome:value)" /></UQ_CGHOM_HED_MH>
<UQ_CGHOM_HED_TCara><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTCara']/outcome:value)" /></UQ_CGHOM_HED_TCara>
<UQ_CGHOM_HED_HC><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboHC']/outcome:value)" /></UQ_CGHOM_HED_HC>
<UQ_CGHOM_HED_MR><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboMR']/outcome:value)" /></UQ_CGHOM_HED_MR>
<UQ_CGHOM_HED_HPCover><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboHPCover']/outcome:value)" /></UQ_CGHOM_HED_HPCover>
                 <UQ_CGHOM_HED_County><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCounty']/outcome:value)" /></UQ_CGHOM_HED_County>
                 <UQ_CGHOM_HED_PostSect><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtPostSect']/outcome:value)" /></UQ_CGHOM_HED_PostSect>
                 <UQ_CGHOM_HED_Country><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtCountry']/outcome:value)" /></UQ_CGHOM_HED_Country>
             </UO_CGHOM_HED>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmPROPDET.tst" match="frmPROPDET.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmPROPDET.tst' and @parentID=$pParentID]">
             <UO_CGHOM_PROPDET xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
<UQ_CGHOM_PROPDET_EmpStat><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpStat']/outcome:value)" /></UQ_CGHOM_PROPDET_EmpStat>
<UQ_CGHOM_PROPDET_Occupation><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboOccupation']/outcome:value)" /></UQ_CGHOM_PROPDET_Occupation>
                 <UQ_CGHOM_PROPDET_DOB><xsl:value-of select="func:MyDateFormat(outcome:controls/outcome:control[@name='mskDOB']/outcome:value,2)" /></UQ_CGHOM_PROPDET_DOB>
                 <UQ_CGHOM_PROPDET_Surname><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtSurname']/outcome:value)" /></UQ_CGHOM_PROPDET_Surname>
                 <UQ_CGHOM_PROPDET_Initial><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtInitial']/outcome:value)" /></UQ_CGHOM_PROPDET_Initial>
                 <UQ_CGHOM_PROPDET_Forename><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtForename']/outcome:value)" /></UQ_CGHOM_PROPDET_Forename>
<UQ_CGHOM_PROPDET_Title><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboTitle']/outcome:value)" /></UQ_CGHOM_PROPDET_Title>
<UQ_CGHOM_PROPDET_EmpBus><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='cboEmpBus']/outcome:value)" /></UQ_CGHOM_PROPDET_EmpBus>
             </UO_CGHOM_PROPDET>
         </xsl:for-each>
     </xsl:template>

     <xsl:template name="frmGeneral.tst" match="frmGeneral.tst">
     <xsl:param name="pParentID" select="''"></xsl:param>
         <xsl:for-each select="//outcome:screen[@name='frmGeneral.tst' and @parentID=$pParentID]">
             <UO_CGHOM_General xmlns="http://www.wisl.co.uk/schemas" >
                 <xsl:attribute name="instance"><xsl:value-of select="position()" /></xsl:attribute>
                 <UQ_CGHOM_General_Details><xsl:value-of select="normalize-space(outcome:controls/outcome:control[@name='txtDetails']/outcome:value)" /></UQ_CGHOM_General_Details>
                 <UQ_CGHOM_General_Requested><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRequested']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRequested']/outcome:value='true')" /></UQ_CGHOM_General_Requested>
                 <UQ_CGHOM_General_Convicted><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='1' or outcome:controls/outcome:control[@name='chkConvicted']/outcome:value='true')" /></UQ_CGHOM_General_Convicted>
                 <UQ_CGHOM_General_Refused><xsl:value-of select="boolean(outcome:controls/outcome:control[@name='chkRefused']/outcome:value='1' or outcome:controls/outcome:control[@name='chkRefused']/outcome:value='true')" /></UQ_CGHOM_General_Refused>
             </UO_CGHOM_General>
         </xsl:for-each>
     </xsl:template>

</xsl:stylesheet>
