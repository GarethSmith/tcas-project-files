<?xml version="1.0" encoding="utf-8"?>
<screen name="frmHED.tst" type="NextPrevious">
    <backColor>
        <r>212</r>
        <g>208</g>
        <b>200</b>
    </backColor>
    <boundScreen />
    <caption>Home Emergency Details</caption>
    <description>Home Emergency Details</description>
    <screenType>0</screenType>
    <projectType>1</projectType>
    <postQuoteNavigationDuplicated>False</postQuoteNavigationDuplicated>
    <foreColor>
        <r>0</r>
        <g>0</g>
        <b>0</b>
    </foreColor>
    <height>249</height>
    <parentID />
    <text>Home Emergency Details</text>
    <width>829</width>
    <defaultFont>
        <bold>False</bold>
        <fontName>Tahoma</fontName>
        <formLoadCode />
        <formValidateCode />
        <italic>False</italic>
        <size>8.25</size>
        <strikeout>False</strikeout>
        <underline>False</underline>
    </defaultFont>
    <controls>
        <control name="grpRiskAdd" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>165</height>
            <left>358</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>1</tabIndex>
            <text>Risk Address</text>
            <top>10</top>
            <width>452</width>
            <controls>
                <control name="txtPostSect" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>POSTSECT</columnName>
                    <description>txtPostSect</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>90</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>14</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>223</top>
                    <width>0</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtCountry" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>COUNTRY</columnName>
                    <description>txtCountry</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>90</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>13</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>193</top>
                    <width>0</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="txtCounty" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>COUNTY</columnName>
                    <description>txtCounty</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>11</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>161</top>
                    <width>0</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="cmdPostSearch" type="WisButton">
                    <searchProperties>
                        <Postcode>txtPostcode</Postcode>
                        <House>txtHouse</House>
                        <Street>txtStreet</Street>
                        <Locality>txtLocality</Locality>
                        <City>txtCity</City>
                        <County>txtCounty</County>
                        <Country>txtCountry</Country>
                        <PostcodeSector>txtPostSect</PostcodeSector>
                    </searchProperties>
                    <pluginId>29843A42AE0740DDAD98E8A15206F0C8</pluginId>
                    <searchType>TES Postcode Search</searchType>
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <writableProperties />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <description>cmdPostSearch</description>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>368</left>
                    <top>23</top>
                    <tabIndex>10</tabIndex>
                    <type>6</type>
                    <text>  &amp;Search</text>
                    <width>75</width>
                </control>
                <control name="txtCity" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>CITY</columnName>
                    <description>txtCity</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>9</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>134</top>
                    <width>357</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblCity" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblCity</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>8</tabIndex>
                    <text>City</text>
                    <textAlign>1</textAlign>
                    <top>136</top>
                    <width>73</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtLocality" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>LOCALITY</columnName>
                    <description>txtLocality</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>7</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>107</top>
                    <width>357</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblLocality" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblLocality</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>6</tabIndex>
                    <text>Locailty</text>
                    <textAlign>1</textAlign>
                    <top>109</top>
                    <width>69</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtStreet" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>STREET</columnName>
                    <description>txtStreet</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>80</top>
                    <width>357</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblStreet" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblStreet</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text>Street</text>
                    <textAlign>1</textAlign>
                    <top>82</top>
                    <width>73</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtHouse" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>HOUSE</columnName>
                    <description>txtHouse</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>52</top>
                    <width>357</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblHouse" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblHouse</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <text>House</text>
                    <textAlign>1</textAlign>
                    <top>54</top>
                    <width>77</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="txtPostcode" type="WisTextBox">
                    <enableControls />
                    <sumTotalControls />
                    <acceptsTab>False</acceptsTab>
                    <acceptsReturn>False</acceptsReturn>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <borderStyle>1</borderStyle>
                    <characterCasing>0</characterCasing>
                    <columnName>POSTCODE</columnName>
                    <description>txtPostcode</description>
                    <decimalPlaces>0</decimalPlaces>
                    <useDecimalPlaces>False</useDecimalPlaces>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>87</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <maxLength>200</maxLength>
                    <multiLine>False</multiLine>
                    <numeric>False</numeric>
                    <autoIncrement>False</autoIncrement>
                    <maxAutoValue>0</maxAutoValue>
                    <properCase>False</properCase>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <text />
                    <textAlign>0</textAlign>
                    <top>26</top>
                    <width>267</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblPost" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblPost</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text>Postcode</text>
                    <textAlign>1</textAlign>
                    <top>28</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
            </controls>
        </control>
        <control name="grpCoverReq" type="GroupBox">
            <backColor>
                <r>212</r>
                <g>208</g>
                <b>200</b>
            </backColor>
            <enabled>True</enabled>
            <foreColor>
                <r>0</r>
                <g>0</g>
                <b>0</b>
            </foreColor>
            <height>165</height>
            <left>10</left>
            <font>
                <bold>True</bold>
                <fontName>Tahoma</fontName>
                <italic>False</italic>
                <size>8.25</size>
                <strikeout>False</strikeout>
                <underline>False</underline>
            </font>
            <rightToLeft>0</rightToLeft>
            <tabIndex>0</tabIndex>
            <text>Cover Required</text>
            <top>10</top>
            <width>342</width>
            <controls>
                <control name="cboMH" type="WisComboBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHC</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4H1E4</listValue>
                                <listValue>3RV4H1H7</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHPCover</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4H1E4</listValue>
                                <listValue>3RV4H1H7</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboTCara</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4H1E4</listValue>
                                <listValue>3RV4H1H7</listValue>
                            </listValues>
                        </enableControl>
                    </enableControls>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HEM_MH_COVER_ID</columnName>
                    <description>cboMH</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>112</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HEM_MH_COVER</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>9</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>134</top>
                    <width>222</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblMH" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblMH</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>8</tabIndex>
                    <text>Motorhome</text>
                    <textAlign>1</textAlign>
                    <top>137</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboTCara" type="WisComboBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboMH</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43QF4</listValue>
                                <listValue>3RV43QR6</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHC</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43QF4</listValue>
                                <listValue>3RV43QR6</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHPCover</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43QF4</listValue>
                                <listValue>3RV43QR6</listValue>
                            </listValues>
                        </enableControl>
                    </enableControls>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HEM_TC_MH_ID</columnName>
                    <description>cboTCara</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>112</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HEM_TC_MH</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>7</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>106</top>
                    <width>222</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblTCara" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblTCara</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>6</tabIndex>
                    <text>Touring Caravan</text>
                    <textAlign>1</textAlign>
                    <top>109</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboHPCover" type="WisComboBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHC</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4GQU4</listValue>
                                <listValue>3RV4GR38</listValue>
                                <listValue>3RV4GR84</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboTCara</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4GQU4</listValue>
                                <listValue>3RV4GR38</listValue>
                                <listValue>3RV4GR84</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboMH</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV4GQU4</listValue>
                                <listValue>3RV4GR38</listValue>
                                <listValue>3RV4GR84</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtCity</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EH56</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtHouse</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EH56</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtLocality</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EH56</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtPostcode</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EH56</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtStreet</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EH56</listValue>
                            </listValues>
                        </enableControl>
                    </enableControls>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HEM_HP_COVER_ID</columnName>
                    <description>cboHPCover</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>112</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HEM_HP_COVER</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>5</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>79</top>
                    <width>222</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblHP" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblHP</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>4</tabIndex>
                    <text>Holiday Property</text>
                    <textAlign>1</textAlign>
                    <top>82</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboHC" type="WisComboBox">
                    <enableControls>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboHPCover</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43E39</listValue>
                                <listValue>3RV43EF1</listValue>
                                <listValue>3RV43EM5</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboTCara</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43E39</listValue>
                                <listValue>3RV43EF1</listValue>
                                <listValue>3RV43EM5</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>cboMH</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV43E39</listValue>
                                <listValue>3RV43EF1</listValue>
                                <listValue>3RV43EM5</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtCity</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EF42</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtHouse</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EF42</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtLocality</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EF42</listValue>
                                <listValue>3RV7EF42</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtPostcode</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EF42</listValue>
                            </listValues>
                        </enableControl>
                        <enableControl>
                            <boundScreen>CGHOM\frmHED.tst</boundScreen>
                            <control>txtStreet</control>
                            <enable>False</enable>
                            <listValues>
                                <listValue>3RV7EF42</listValue>
                            </listValues>
                        </enableControl>
                    </enableControls>
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HEM_HH_HP_PH_ID</columnName>
                    <description>cboHC</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>112</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HEM_HH_HP_PH</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>3</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>52</top>
                    <width>223</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="lblHC" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>lblHC</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>2</tabIndex>
                    <text>Holiday Caravan</text>
                    <textAlign>1</textAlign>
                    <top>55</top>
                    <width>100</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
                <control name="cboMR" type="WisComboBox">
                    <enableControls />
                    <backColor>
                        <r>255</r>
                        <g>255</g>
                        <b>255</b>
                    </backColor>
                    <columnName>HEM_MAIN_COVER_ID</columnName>
                    <description>cboMR</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>21</height>
                    <left>112</left>
                    <font>
                        <bold>False</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <listTable>LIST_HEM_MAIN_COVER</listTable>
                    <maxLength>0</maxLength>
                    <numeric>False</numeric>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>1</tabIndex>
                    <userFilled>False</userFilled>
                    <text />
                    <top>25</top>
                    <width>223</width>
                    <objectID>0</objectID>
                    <longPropertyID>0</longPropertyID>
                    <defaultValue />
                    <showHelpText>False</showHelpText>
                    <helpText />
                    <required>True</required>
                    <displayOnWebpage>True</displayOnWebpage>
                    <alwaysVisible>True</alwaysVisible>
                    <controls />
                </control>
                <control name="LBLMR" type="WisLabel">
                    <sumTotalControls />
                    <backColor>
                        <r>212</r>
                        <g>208</g>
                        <b>200</b>
                    </backColor>
                    <borderStyle>0</borderStyle>
                    <description>LBLMR</description>
                    <enabled>True</enabled>
                    <foreColor>
                        <r>0</r>
                        <g>0</g>
                        <b>0</b>
                    </foreColor>
                    <height>23</height>
                    <left>6</left>
                    <font>
                        <bold>True</bold>
                        <fontName>Tahoma</fontName>
                        <italic>False</italic>
                        <size>8.25</size>
                        <strikeout>False</strikeout>
                        <underline>False</underline>
                    </font>
                    <rightToLeft>0</rightToLeft>
                    <tabIndex>0</tabIndex>
                    <text>Main Residence</text>
                    <textAlign>1</textAlign>
                    <top>28</top>
                    <width>96</width>
                    <isDateDiff>0</isDateDiff>
                    <controls />
                </control>
            </controls>
        </control>
    </controls>
    <generalSummary>
        <columns />
    </generalSummary>
    <formValidation />
    <previousRiskAtAQ />
    <hideClaim />
</screen>